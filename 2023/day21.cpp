#include <cstdlib>
#include <fstream>
#include <iostream>
#include <optional>
#include <ostream>
#include <queue>
#include <set>
#include <string>
#include <tuple>
#include <vector>

// #define FILENAME "bin/2023/tests/day21-part1.txt"
#define FILENAME "bin/2023/tests/day21-example1.txt"

namespace aoc {

using pair = std::tuple<int64_t, int64_t>;

class map {
  std::vector<std::string> data_;
  size_t y_max_;
  size_t x_max_;

public:
  explicit map(const std::vector<std::string> &data)
      : data_(data), y_max_(data.size()) {
    if (y_max_ > 0) {
      x_max_ = data[0].length();
    } else {
      x_max_ = 0;
    }
  }

  [[nodiscard]] auto grid() const -> const std::vector<std::string> & {
    return data_;
  }

  [[nodiscard]] auto get(size_t x_index, size_t y_index) const -> char {
    return data_[y_index][x_index];
  }

  [[nodiscard]] auto max() const -> pair { return {x_max_, y_max_}; }
};

class visited {
  std::set<pair> visited_;
  size_t x_max_;
  size_t y_max_;

public:
  visited(size_t x_max, size_t y_max) : x_max_(x_max), y_max_(y_max) {}
  auto vert_unvisited(size_t x_index, size_t y_index) {
    if (y_index >= 0 && y_index < y_max_) {
      return unvisited(x_index, y_index);
    }
    return false;
  }
  auto hz_unvisited(size_t x_index, size_t y_index) {
    if (x_index >= 0 && x_index <= x_max_) {
      return unvisited(x_index, y_index);
    }
    return false;
  }
  auto unvisited(size_t x_index, size_t y_index) -> bool {
    auto [unused_, inserted] = visited_.insert({x_index, y_index});
    return inserted;
  }
  auto reset() { visited_.clear(); }
};

auto brute_force(const map &map, pair start, visited visited, size_t steps)
    -> size_t {
  std::queue<aoc::pair> positions;
  positions.push(start);

  for (auto step = 0; step < steps; step++) {
    visited.reset();
    auto pos_sz = positions.size();
    for (auto pos_i = 0; pos_i < pos_sz; pos_i++) {
      auto [x_index, y_index] = positions.front();
      positions.pop();

      // check north
      auto north = y_index - 1;
      if (visited.vert_unvisited(x_index, north) &&
          map.grid()[north][x_index] != '#') {
        positions.emplace(x_index, north);
      }
      // check south
      auto south = y_index + 1;
      if (visited.vert_unvisited(x_index, south) &&
          map.grid()[south][x_index] != '#') {
        positions.emplace(x_index, south);
      }
      // check west
      auto west = x_index - 1;
      if (visited.hz_unvisited(west, y_index) &&
          map.grid()[y_index][west] != '#') {
        positions.emplace(west, y_index);
      }
      // check east
      auto east = x_index + 1;
      if (visited.hz_unvisited(east, y_index) &&
          map.grid()[y_index][east] != '#') {
        positions.emplace(east, y_index);
      }
    }
  }
  return positions.size();
}

enum class entered_on : bool { even, odd };

class predict_key {

  pair start_;
  entered_on on_;

public:
  predict_key(pair start, entered_on is_on) : start_(start), on_(is_on) {}
};

class predict_value {
  uint64_t plots_;
  std::vector<predict_key> edges_entered_;
  int64_t steps_taken_;

public:
  predict_value(uint64_t plots, std::vector<predict_key> &edges, int64_t steps)
      : plots_(plots), edges_entered_(edges), steps_taken_(steps) {}

  [[nodiscard]] auto plot() const -> uint64_t { return plots_; }
  [[nodiscard]] auto edges_entered() const -> std::vector<predict_key> {
    return edges_entered_;
  }
  [[nodiscard]] auto steps_taken() const -> size_t { return steps_taken_; }
};

auto make_prediction(const map &map, pair start, int64_t steps)
    -> predict_value {
  auto [x_max, y_max] = map.max();
  uint64_t plots = 0;
  std::set<pair> result_cache;
  std::queue<pair> positions;
  positions.push(start);

  std::vector<predict_key> edges;

  // if steps is even then 0, if steps is odd then 1. Imagine we have
  // 67 steps left, we need to move 2 spaces to get back to our same position
  // so 65 means on 67 we get back to our position
  int64_t prime_offset = steps % 2;
  int64_t steps_taken = 0;
  for (; !positions.empty() && steps_taken <= steps; steps_taken++) {
    auto pos_sz = positions.size();
    for (auto pos_i = 0; pos_i < pos_sz; pos_i++) {
      auto pos = positions.front();
      positions.pop();

      // a position on the map is only ever evaluated once, after
      // we get through evaluating every single position we simply
      // return the results as a `predict_value`
      auto [unused_, inserted] = result_cache.insert(pos);
      if (!inserted) {
        continue;
      }

      auto [x_index, y_index] = pos;

      // we will end up on this tile if the rest of steps is even. For example,
      // if 2 steps left, we move off of this step, then back on in the next
      // step. If odd is left, we don't end up on this space
      bool is_even = (steps_taken + prime_offset) % 2 == 0;
      if (is_even) {
        std::cout << x_index << ", " << y_index << '\n';
        plots += 1;
      }

      auto is_on = static_cast<entered_on>(is_even);

      // check north
      auto north = y_index - 1;
      std::cout << north << " nxt north-\n";
      if (north < 0 && map.get(x_index, y_max - 1) != '#') {
        // we enter to the next map through the north side
        pair nxt = {x_index, y_max - 1};
        edges.emplace_back(nxt, is_on);
      } else if (map.get(x_index, north) != '#') {
        std::cout << "nxt north\n";
        positions.emplace(x_index, north);
      }

      // check south
      auto south = y_index + 1;
      std::cout << "nxt south-\n";
      if (south >= y_max - 1 && map.get(x_index, 0) != '#') {
        // we enter to the next map through the south side
        pair nxt = {x_index, 0};
        edges.emplace_back(nxt, is_on);
      } else if (map.get(x_index, south) != '#') {
        std::cout << "nxt south\n";
        positions.emplace(x_index, south);
      }

      // check west
      auto west = x_index - 1;
      std::cout << "nxt west-\n";
      if (west < 0 && map.get(x_max - 1, y_index) != '#') {
        // we enter to the next map through the west side
        pair nxt = {x_max - 1, y_index};
        edges.emplace_back(nxt, is_on);
      } else if (map.get(west, y_index) != '#') {
        std::cout << "nxt west\n";
        positions.emplace(west, y_index);
      }

      // check east
      auto east = x_index + 1;
      std::cout << "nxt east-\n";
      if (east >= x_max - 1 && map.get(0, y_index) != '#') {
        // we enter to the next map through the east side
        pair nxt = {0, y_index};
        edges.emplace_back(nxt, is_on);
      } else if (map.get(east, y_index) != '#') {
        std::cout << "nxt east\n";
        positions.emplace(east, y_index);
      }
    }
  }

  return {plots, edges, steps - steps_taken};
}

auto predict_steps(const map &map, pair start, int64_t steps) -> uint64_t {
  auto prediction = make_prediction(map, start, steps);
  steps -= prediction.steps_taken();
  std::cout << "steps left " << steps << '\n';
  return prediction.plot();
}

} // namespace aoc

// constexpr int64_t PART2_STEPS = 26501365;
constexpr int64_t PART2_STEPS = 5000;
// constexpr int64_t PART1_STEPS = 64;
constexpr int64_t PART1_STEPS = 10;

auto main() -> int {
  std::string line;
  std::fstream infile(FILENAME);
  std::vector<std::string> grid;
  std::optional<aoc::pair> start;

  size_t x_max = 0;

  size_t y_max = 0;
  while (std::getline(infile, line)) {
    grid.push_back(line);
    auto pos = line.find('S');
    if (pos != std::string::npos) {
      auto x_index = static_cast<size_t>(pos);
      start = {x_index, y_max};
      x_max = line.length();
    }
    y_max++;
  }

  if (!start.has_value()) {
    std::cout << "No starting pos\n";
    return EXIT_FAILURE;
  }

  aoc::map map(grid);
  aoc::visited visited(x_max, y_max);
  auto result1 = aoc::brute_force(map, start.value(), visited, PART1_STEPS);
  auto result2 = aoc::predict_steps(map, start.value(), PART1_STEPS);
  std::cout << "Part 1 : " << result1 << '\n' << "Part 2 : " << result2 << '\n';

  return EXIT_SUCCESS;
}
