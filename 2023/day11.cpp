
/*
 * Day 11 - I got frustrated with too many of the same pointer in a container
 *          by the time I realized, I already switched to C++. I then also
 * realized that C++ will generally copy by default so it's actually a good
 * idea anyways.
 */
#include <cstdlib>
#include <glib.h>
#include <glib/gstdio.h>
#include <iostream>
#include <ostream>
#include <stdbool.h>
#include <stdlib.h>
#include <string>
#include <tuple>
#include <unordered_set>
#include <vector>

#define FILENAME "bin/2023/tests/day11-part1.txt"
// #define FILENAME "bin/2023/tests/day11-example1.txt"

// part1
// #define INSERTS 1
// part2
// #define INSERTS 10
// #define INSERTS 100
#define INSERTS 1000000

void
universe_print (std::vector<std::string> universe)
{
  for (auto str : universe)
    {
      std::cout << str << std::endl;
    }
}

typedef struct Universe
{
  std::vector<std::string> map;
  std::unordered_set<guint64> x_expands;
  std::unordered_set<guint64> y_expands;
} universe_t;

universe_t
universe_expand (gchar **lines)
{
  guint lsz = g_strv_length (lines);
  // keep track of our grid in both X and Y orientations
  std::vector<std::string> x_axis;
  std::vector<std::string> y_axis;

  universe_t universe;

  // guint64 x_expands = 0;
  // guint64 y_expands = 0;

  for (guint64 y = 0; y < lsz; y++)
    {
      std::string line (lines[y]);
      if (y_axis.size () < line.length ())
        {
          y_axis.resize (line.length ());
        }

      if (line.length () < y_axis.size ())
        {
          continue;
        }

      bool has_galaxy = false;

      for (guint64 x = 0; x < line.length (); x++)
        {
          gchar c = line[x];
          if (c == '#')
            {
              has_galaxy = true;
            }

          y_axis[x].insert (y, 1, c);
        }

      // since we're rebuilding the original, we also re-insert here
      x_axis.push_back (line);

      // Simple brute force, for part 2 we can simply add the `INSERTS`
      // when we cross a galaxy
      // if this row doesn't have a galaxy then we need to insert a
      // new row at this point later, can't do it now since we're already
      // looping
      // if (!has_galaxy)
      //  {
      //    x_axis.push_back (line);
      //  }
      if (!has_galaxy)
        {
          universe.x_expands.insert (y);
        }
    }

  // we want to operate off of our X, not Y. Therefore, we're just
  // going to pad the X stuff
  // gsize y_len = y_axis->len;
  for (guint64 x = 0; x < y_axis.size (); x++)
    {
      std::string column = y_axis[x];
      bool has_galaxy = false;
      for (guint64 y = 0; y < column.length (); y++)
        {
          gchar c = column[y];
          if (c == '#')
            {
              has_galaxy = true;
            }
        }

      if (!has_galaxy)
        {
          // Simple brute force but we can also simply append `INSERTS`
          // when crossing this index
          // for (guint64 y = 0; y < x_axis.size (); y++)
          //  {
          //    x_axis[y].insert (x + y_expands, INSERTS, '.');
          //  }
          // y_expands++;
          universe.y_expands.insert (x);
        }
    }
  universe.map = x_axis;
  return universe;
}

std::vector<std::tuple<guint64, guint64> >
galaxies_find (universe_t universe)
{
  std::vector<std::tuple<guint64, guint64> > ret;

  guint64 vert_offset = 0;
  for (auto y = 0; y < universe.map.size (); y++)
    {
      if (universe.x_expands.find (y) != universe.x_expands.end ())
        {
          printf ("ADDING %d\n", y);
          vert_offset += INSERTS - 1;
        }
      auto row = universe.map[y];
      guint64 hz_offset = 0;
      for (auto x = 0; x < row.length (); x++)
        {
          if (universe.y_expands.find (x) != universe.y_expands.end ())
            {
              hz_offset += INSERTS - 1;
            }
          auto c = row[x];
          if (c == '#')
            {
              ret.push_back ({ x + hz_offset, y + vert_offset });
            }
        }
    }
  return ret;
}

int
main ()
{
  gchar *contents = NULL;
  GError *err = NULL;
  g_file_get_contents (FILENAME, &contents, NULL, &err);
  if (err != NULL)
    {
      g_printerr ("could not open %s : but got %s\n", FILENAME, err->message);
      g_assert (contents == NULL);
    }

  gchar **lines = g_strsplit (contents, "\n", 0);
  gint64 result1 = 0; //, result2 = 0;

  universe_t universe = universe_expand (lines);
  universe_print (universe.map);

  std::cout << "X inserts at : ";
  for (auto x : universe.x_expands)
    {
      std::cout << x << ' ';
    }
  std::cout << std::endl;

  std::cout << "Y inserts at : ";
  for (auto y : universe.y_expands)
    {
      std::cout << y << ' ';
    }
  std::cout << std::endl;

  std::vector<std::tuple<guint64, guint64> > galaxies
      = galaxies_find (universe);

  for (auto [x, y] : galaxies)
    {
      std::cout << "{" << x << ", " << y << "}" << std::endl;
    }

  for (auto i = 0; i < galaxies.size (); i++)
    {
      auto [ax, ay] = galaxies[i];
      for (auto j = i + 1; j < galaxies.size (); j++)
        {
          auto [bx, by] = galaxies[j];

          long long x_diff = abs ((long long)(ax - bx));
          long long y_diff = abs ((long long)(ay - by));

          auto diff = x_diff + y_diff;
          g_printf ("diff between %d and %d is %lld\n", i, j, diff);
          result1 += diff;
        }
    }

  g_print ("Part 1 : %ld\n", result1);

  return EXIT_SUCCESS;
}
