#include <cstdint>
#include <fstream>
#include <iostream>
#include <optional>
#include <ostream>
#include <string>
#include <sys/types.h>
#include <vector>

// #define FILENAME "bin/2023/tests/day13-example1.txt"
#define FILENAME "bin/2023/tests/day13-part1.txt"
#define nl "\n"

using std::cout;
// using std::plus;

std::optional<uint64_t> mirrored(std::vector<std::string> &mirror,
                                 int64_t lbounds, int64_t ubounds, bool fix) {
  // cout << mirror[lbounds] << " == " << mirror[ubounds] << "  | ";
  bool did_fix = false;
  uint64_t ret = 0;

  while (lbounds >= 0) {
    ret += 1;
    if (ubounds < mirror.size()) {
      auto lstr = mirror[lbounds];
      auto ustr = mirror[ubounds];
      if (lstr.length() != ustr.length()) {
        // error
        cout << "ERROR: encountered a weird state where these strings aren't "
                "the "
             << "same length " << lstr.length() << " : " << ustr.length() << nl;
        return ret;
      }

      for (auto index = 0; index < lstr.length(); index++) {
        if (lstr[index] != ustr[index]) {
          if (fix && !did_fix) {
            did_fix = true;
          } else {
            return std::nullopt;
          }
        }
      }
    }

    lbounds--;
    ubounds++;
  }
  // puts(" : yes\n");
  if (fix && !did_fix) {
    return std::nullopt;
  }
  return ret;
}

class mirror_length {
public:
  size_t index;
  uint64_t result;

  mirror_length(uint64_t index, uint64_t result)
      : index(index), result(result) {}
};

std::optional<mirror_length> mirror_middle(std::vector<std::string> &mirror,
                                           bool fix,
                                           std::optional<size_t> prev) {
  // in a grid of length 7, the middle is either index
  // 2 and 3 or 3 and 4. So we only need to iterate the
  // middle + 2
  for (auto sindex = 0; sindex < mirror.size() - 1; sindex++) {
    if (prev.has_value() && prev.value() == sindex) {
      // continue; // we matched on this previously
    }
    auto lbound = sindex;
    auto ubound = sindex + 1;

    // cout << "trying " << lbound << " : " << ubound << nl;

    auto len = mirrored(mirror, static_cast<int64_t>(lbound),
                        static_cast<int64_t>(ubound), fix);
    if (len.has_value()) {
      mirror_length result(sindex, len.value());
      return result;
    }
  }
  return std::nullopt;
}

std::vector<std::string> rotate(std::vector<std::string> &vec) {
  // all of the items should be the same size
  auto len = vec[0].length();
  std::vector<std::string> ret(len);
  for (auto yindex = 0; yindex < vec.size(); yindex++) {
    auto str = vec[yindex];
    for (auto xindex = 0; xindex < len; xindex++) {
      ret[xindex].push_back(str[xindex]);
    }
  }
  return ret;
}

constexpr auto MULTI = 100;
mirror_length mirror_length(std::vector<std::string> mirror, bool fix,
                            std::optional<size_t> prev) {
  // cout << "hz \n" << nl;
  // Phase 1, find the middle, horizontally
  // cout << "vert \n" << nl;
  auto middle = mirror_middle(mirror, fix, prev);
  if (middle.has_value()) {
    // we're vertically split!
    // printf("h: %ld\n", middle.value());
    middle.value().result *= MULTI;
    return middle.value();
  }

  //  Phase 2. Rotate the map and check middle, vertically
  auto rotated = rotate(mirror);
  middle = mirror_middle(rotated, fix, prev);
  if (middle.has_value()) {
    // printf("v: %ld\n", middle.value());
    return middle.value();
  }

  return {0, 0};
}

int main() {
  // std::vector<std::future<uint64_t>> results;
  std::vector<std::string> mirror;

  std::string line;
  std::fstream infile(FILENAME);
  uint64_t result1 = 0;
  uint64_t result2 = 0;
  uint64_t cnter = 0;
  while (std::getline(infile, line)) {
    if (line.empty()) {
      // results.push_back(std::async(std::launch::async, mirror_length,
      // mirror));
      //  above call copies mirror so we can clear the current one
      auto len1 = mirror_length(mirror, false, std::nullopt);
      auto len2 = mirror_length(mirror, true, len1.index);
      cout << cnter << " " << len1.result << " -> " << len2.result << nl;
      cnter++;
      mirror.clear();
      result1 += len1.result;
      result2 += len2.result;
    } else {
      mirror.push_back(line);
    }
  }
  if (!mirror.empty()) {
    auto len1 = mirror_length(mirror, false, std::nullopt);
    auto len2 = mirror_length(mirror, true, len1.index);
    cout << cnter << " " << len1.result << " -> " << len2.result << nl;
    result1 += len1.result;
    result2 += len2.result;
    // results.push_back(std::async(std::launch::async, mirror_length, mirror));
  }

  /*
  uint64_t result1 = std::accumulate(
      results.begin(), results.end(), 0,
      [&](const auto &acc, auto &fut) mutable { return acc + fut.get(); });
  */

  cout << "part1 = " << result1 << nl << "part2 = " << result2 << nl;
}
