
#include <algorithm>
#include <boost/fiber/future/future.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/breadth_first_search.hpp>
#include <boost/graph/depth_first_search.hpp>
#include <boost/graph/detail/adjacency_list.hpp>
#include <boost/graph/graph_selectors.hpp>
#include <boost/graph/named_function_params.hpp>
#include <boost/leaf.hpp>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <memory>
#include <optional>
#include <string>
#include <tuple>
#include <unordered_set>

namespace aoc {

using Graph =
    boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS>;
using Vertex = Graph::vertex_descriptor;

namespace grid {

using Paths = std::shared_ptr<std::vector<std::vector<Vertex>>>;
class Visitor {
  Paths paths_;
  Vertex end_;
  std::vector<Vertex> path_;
  std::unordered_set<Vertex> visited_;
  std::shared_ptr<int64_t> longest_;

 public:
  Visitor(Paths &paths, Vertex end, std::shared_ptr<int64_t> &longest)
      : paths_(paths), end_(end), longest_(longest) {}

  void discover_vertex(Vertex vertex, const Graph &graph) {
    path_.push_back(vertex);
    std::cout << vertex << " == " << end_ << "\n";

    if (vertex == end_) {
      std::cout << "end\n";
      // Found the end vertex, record the current path and its length
      paths_->push_back(path_);
    }
  }

  void finish_vertex(Vertex &vertex, const Graph &graph) {
    std::cout << vertex << " done\n";
    path_.pop_back();  // Backtrack to the previous vertex
  }
};

class DFSVisitor : public boost::default_dfs_visitor {
  Visitor visitor_;

 public:
  explicit DFSVisitor(Visitor &visitor) : visitor_(visitor) {}

  void discover_vertex(Vertex vertex, const Graph &graph) {
    visitor_.discover_vertex(vertex, graph);
  }

  void finish_vertex(Vertex vertex, const Graph &graph) {
    visitor_.finish_vertex(vertex, graph);
  }
};

class BFSVisitor : public boost::default_bfs_visitor {
  Visitor visitor_;

 public:
  explicit BFSVisitor(Visitor &visitor) : visitor_(visitor) {}

  void discover_vertex(Vertex vertex, const Graph &graph) {
    visitor_.discover_vertex(vertex, graph);
  }

  void finish_vertex(Vertex vertex, const Graph &graph) {
    visitor_.finish_vertex(vertex, graph);
  }
};

using Pos = std::optional<Graph::vertex_descriptor>;
using Row = std::vector<Pos>;

class RecursiveDFS {
  Graph &graph_;
  std::unordered_set<Vertex> visited_;
  int64_t longest = 0;

  [[nodiscard]] auto run(std::optional<Vertex> from, Vertex to, Vertex end)
      -> int64_t {
    //-> std::vector<Vertex> {
    // std::cout << "tick1 " << from.value_or(0) << ", " << to << "\n";
    if (to == end) {
      auto ret = static_cast<int64_t>(visited_.size());
      if (ret > longest) {
        longest = ret;
        std::cout << ret << '\n';
      }
      return ret;
    }

    visited_.insert(to);

    auto [first, last] = boost::adjacent_vertices(to, graph_);

    int64_t ret = 0;
    // #pragma omp parallel for
    for (auto i = first; i != last; i++) {
      Vertex next = boost::vertex(*i, graph_);
      // std::cout << "from " << from.value_or(0) << " to " << to << " then "
      //           << next << '\n';
      // if (from.has_value() && next == from.value()) {
      // std::cout << "skip\n";
      //  continue;
      //}
      // auto [unused_, inserted] = this_visited.insert(next);
      // if (!inserted) {
      //  continue;
      //}

      if (visited_.find(next) != visited_.end()) {
        continue;
      }
      auto result = run(to, next, end);
      if (result > ret) {
        ret = result;
      }
    }
    visited_.erase(to);
    if (ret == 0) {
      return 0;
      // ret.clear();
    }
    // else {
    // ret += rest + 1;
    // ret.insert(ret.end(), rest.begin(), rest.end());
    //}
    return ret;
  }

 public:
  explicit RecursiveDFS(Graph &graph) : graph_(graph) {}

  /*
  [[nodiscard]] auto longest_path(Vertex vector, Vertex end) const
      -> std::vector<Vertex> {
    return longest_path(std::nullopt, vector, end);
  }
  [[nodiscard]] auto longest_path(std::optional<Vertex> from, Vertex vector,
                                  Vertex end) const -> std::vector<Vertex> {
    std::unordered_set<Vertex> visited;
    return run(from, vector, end, visited);
  }
  */

  [[nodiscard]] auto longest_len(Vertex vector, Vertex end) -> int64_t {
    return run(std::nullopt, vector, end);
  }
};

class Builder {
  Graph paths_;
  std::vector<Row> grid_;
  std::optional<Vertex> end_;
  std::optional<Vertex> start_;

  auto add_edges(size_t x_index, size_t y_index) -> boost::leaf::result<void> {
    auto vertex_opt = grid_[y_index][x_index];
    if (!vertex_opt.has_value()) {
      return boost::leaf::new_error(
          "expected X/Y to have a valid vertex in grid_");
    }
    auto vertex = vertex_opt.value();

    // look up/left for edges to connect to
    // we don't need to look down or right since we add edges incrementally top
    // to bottom and left to right

    if (y_index > 0) {
      auto north = grid_[y_index - 1][x_index];
      if (north.has_value()) {
        boost::add_edge(vertex, north.value(), paths_);
      }
    }

    if (x_index > 0) {
      auto west = grid_[y_index][x_index - 1];
      if (west.has_value()) {
        boost::add_edge(vertex, west.value(), paths_);
      }
    }

    return {};
  }

 public:
  auto add_line(std::string &line) -> boost::leaf::result<void> {
    Row row(line.size());
    for (auto x_index = 0; x_index < line.size(); x_index++) {
      auto tile = line[x_index];
      std::optional<Graph::vertex_descriptor> vertex;
      if (tile != '#') {
        vertex = boost::add_vertex(paths_);
        if (!vertex.has_value()) {
          return boost::leaf::new_error("could not add vertex");
        }
        if (boost::num_vertices(paths_) == 1) {
          start_ = vertex.value();
        }
        end_ = vertex.value();
      }
      row[x_index] = vertex;
    }
    grid_.push_back(row);
    auto y_index = grid_.size() - 1;

    for (auto x_index = 0; x_index < row.size(); x_index++) {
      if (row[x_index].has_value()) {
        auto err = add_edges(x_index, y_index);
        if (!err) {
          return boost::leaf::new_error("could not add edges", err.error());
        }
      }
    }
    return {};
  }

  auto minimize(Vertex vertex) {
    auto prev = vertex;

    while (prev != end_) {
      auto [first, last] = boost::adjacent_vertices(prev, paths_);
      auto len = last - first;
      if (len >= 3) {
        // junction point
        break;
      }
      for (auto index=first; index != last; index++) {
        auto next = boost::vertex(*index, paths_);
        if (next == vertex) {
          continue;
        }
        prev = next;
      }
    }
  }

  auto minimize()-> boost::leaf::result<void> {
    if (!start_.has_value()) {
      return boost::leaf::new_error("expected a starting point");
    }

    minimize(start_.value());
    return {};
  }

  [[nodiscard]] auto size() const -> std::tuple<size_t, size_t> {
    return {boost::num_vertices(paths_), boost::num_edges(paths_)};
  }

  auto find_paths() -> boost::leaf::result<int64_t> {
    if (!start_.has_value() || !end_.has_value()) {
      return boost::leaf::new_error("start and/or end not initialized");
    }

    auto start = start_.value();
    auto end = end_.value();

    // Paths paths = std::make_shared<std::vector<std::vector<Vertex>>>();
    RecursiveDFS rdfs(paths_);
    return rdfs.longest_len(start, end);

    // paths->push_back(recursive_dfs(std::nullopt, start, end, {}));

    // std::shared_ptr<int64_t> longest = std::make_shared<int64_t>(0);
    // Visitor base_visitor(paths, end, longest);

    // BFSVisitor bfs_vis(base_visitor);
    // std::vector<int> bfs_path(boost::num_vertices(paths_));
    // auto bfs_vis = boost::make_bfs_visitor(
    //    boost::record_predecessors(bfs_path.data(), boost::on_tree_edge()));
    // auto bgl_bfs_vis = boost::visitor(bfs_vis);
    // for (auto index : bfs_path) {
    //  std::cout << index << " -> ";
    //}
    // std::cout << '\n';
    // boost::breadth_first_search(paths_, start, bgl_bfs_vis);

    // DFSVisitor dfs_vis(base_visitor);
    // auto bgl_dfs_vis = boost::visitor(dfs_vis);
    // auto dfs_root_vertex = bgl_dfs_vis.root_vertex(start);

    // boost::depth_first_search(paths_, bgl_dfs_vis);

    // return paths;
  }

  void print(const Paths &paths) const {
    for (const auto &path : *paths) {
      std::cout << "===================================\n";
      std::cout << path.size() << " steps\n";
      for (const auto &point : path) {
        std::cout << point << " -> ";
      }
      std::cout << "\n===================================\n";
      for (const auto &row : grid_) {
        for (const auto &point : row) {
          if (point.has_value()) {
            if (std::find(path.begin(), path.end(), point.value()) !=
                path.end()) {
              std::cout << "<" << std::setw(3) << point.value() << ">";
            } else {
              std::cout << "." << std::setw(3) << point.value() << ".";
            }
          } else {
            std::cout << "|###|";
          }
        }
        std::cout << '\n';
      }
      std::cout << "===================================\n";
    }
  }
};

} // namespace grid

} // namespace aoc

// #define FILENAME "bin/2023/tests/day23-example1.txt"
#define FILENAME "bin/2023/tests/day23-part1.txt"

auto main() -> int {
  aoc::grid::Builder builder;
  std::string line;
  std::fstream infile(FILENAME);
  while (std::getline(infile, line)) {
    auto err = builder.add_line(line);
    if (!err) {
      std::cout << "ERROR could not add line got : " << err.error() << '\n';
      return EXIT_FAILURE;
    }
  }

  auto [verts, edgs] = builder.size();
  std::cout << "Verticies : " << verts << '\n' << "Edges : " << edgs << '\n';

  auto paths_res = builder.find_paths();
  if (!paths_res) {
    std::cout << "ERROR: could not find paths got : " << paths_res.error()
              << '\n';
    return EXIT_FAILURE;
  }
  auto paths = paths_res.value();
  std::cout << "Got " << paths << " steps\n";

  // builder.print(paths);

  return EXIT_SUCCESS;
}
