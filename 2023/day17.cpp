/*
 * Day 17 : WIP could not complete, not even part 1
 */
#include <sys/types.h>

#include <algorithm>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <cassert>
#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <fstream>
#include <functional>
#include <iostream>
#include <optional>
#include <queue>
#include <set>
#include <string>
#include <tuple>
#include <vector>

#define FILENAME "bin/2023/tests/day17-example1.txt"

using std::cout;
constexpr char endl = '\n';

namespace aoc {

using position = std::tuple<size_t, size_t>;
using grid_data = std::vector<std::vector<uint32_t>>;

enum class direction { east, south, west, north };
constexpr auto endd = static_cast<int>(direction::north);

auto to_char(aoc::direction dir) -> char {
  switch (dir) {
    case direction::east:
      return '>';
    case direction::south:
      return 'v';
    case direction::west:
      return '<';
    case direction::north:
      return '^';
  }
}

auto direction_remove(aoc::direction dir, std::vector<aoc::direction> &dirs)
    -> std::optional<aoc::direction> {
  auto dir_it = std::find(dirs.begin(), dirs.end(), dir);
  if (dir_it != dirs.end()) {
    auto ret = *dir_it;
    dirs.erase(dir_it);
    return ret;
  }
  return std::nullopt;
}

constexpr uint32_t MOVE_BOUNDS = 3;
class crucible {
  aoc::position pos = {0, 0};
  uint32_t moves_left = MOVE_BOUNDS - 1;
  uint64_t heat_lost = 0;
  aoc::direction dir;
  std::set<std::tuple<aoc::position, aoc::direction>> visited_;

 public:
  explicit crucible(aoc::position pos, aoc::direction dir)
      : pos(pos), dir(dir) {}
  explicit crucible(const aoc::crucible &orig, aoc::direction dir)
      : pos(orig.pos),
        heat_lost(orig.heat_lost),
        dir(dir),
        visited_(orig.visited_) {
    cout << "CLONED CRUC " << this->heat_loss() << endl;
  }

  [[nodiscard]] auto heat_loss() const -> uint64_t { return heat_lost; }
  [[nodiscard]] auto position() const -> aoc::position { return pos; }

  friend auto operator>(const aoc::crucible &left, const aoc::crucible &right)
      -> bool {
    auto [lxx, lyy] = left.pos;
    auto [rxx, ryy] = right.pos;
    if (left.heat_lost == right.heat_lost) {
      if (lxx > rxx) {
        return false;
      }
      if (lyy > ryy) {
        return false;
      }
    }
    return left.heat_lost > right.heat_lost;
  }

  auto direction_on(aoc::position pos) -> std::optional<aoc::direction> {
    for (auto index = 0; index <= aoc::endd; index++) {
      auto dir = static_cast<aoc::direction>(index);
      std::tuple<aoc::position, aoc::direction> key = {pos, dir};
      if (visited_.contains(key)) {
        return dir;
      }
    }
    return std::nullopt;
  }

  auto move(const aoc::grid_data &grid_data) -> std::vector<aoc::crucible> {
    auto [unused_, inserted] =
        visited_.insert({this->pos, aoc::direction::east});
    if (!inserted) {
      // we've been here so just end now
      cout << "ALREADY BEEN HERE" << endl;
      this->heat_lost = UINT64_MAX;
      return {};
    }

    auto [grid_x, grid_y] = this->pos;

    cout << "(" << grid_x << ", " << grid_y << ") < " << grid_data.size();
    //  phase 2: calculate heat loss
    const auto &x_axis = grid_data[grid_y];
    cout << ", " << x_axis.size() << endl;
    // assert(grid_x < x_axis.size());
    this->heat_lost += x_axis[grid_x];

    std::vector<aoc::direction> dirs(2);

    switch (this->dir) {
      case aoc::direction::north:
      case aoc::direction::south:
        // cout << "adding ns\n";
        dirs[0] = aoc::direction::west;
        dirs[1] = aoc::direction::east;
        break;
      case aoc::direction::east:
      case aoc::direction::west:
        // cout << "adding ew\n";
        dirs[0] = aoc::direction::north;
        dirs[1] = aoc::direction::south;
        break;
    }

    this->moves_left--;
    if (this->moves_left > 0) {
      dirs.push_back(this->dir);
      // cout << "keeping\n";
    } else {
      this->moves_left = MOVE_BOUNDS;
    }

    // Phase 3.5 - remove any directions that will cause us to move off
    // the grid
    if (grid_x == 0) {
      // don't move west or we'll be moving off the grid... etc etc
      direction_remove(aoc::direction::west, dirs);
    } else if (grid_x + 1 == x_axis.size()) {
      direction_remove(aoc::direction::east, dirs);
    }

    if (grid_y == 0) {
      direction_remove(aoc::direction::north, dirs);
    } else if (grid_y + 1 == grid_data.size()) {
      // south is usually towards the end so
      // we prefer going in this direction
      direction_remove(aoc::direction::south, dirs);
    }

    this->dir = *(dirs.end() - 1);
    dirs.pop_back();

    // phase 1: move
    switch (this->dir) {
      case direction::north:
        grid_y--;
        break;
      case direction::south:
        grid_y++;
        break;
      case direction::east:
        grid_x++;
        break;
      case direction::west:
        grid_x--;
        break;
    }
    this->pos = {grid_x, grid_y};

    // phase 3: determine which other valid paths exist,
    // and calculate our new direction
    std::vector<aoc::crucible> new_crucs;

    // let's start by seeing if we're at the end, if we are then
    // we can end early since we're done
    // if (grid_x + 1 == x_axis.size() && grid_y + 1 == grid_data.size()) {
    //  return new_crucs;
    //}

    for (auto dir : dirs) {
      aoc::crucible new_cruc(*this, dir);
      new_crucs.push_back(new_cruc);
    }

    return new_crucs;
  }
};

using crucible_queue =
    std::priority_queue<aoc::crucible, std::vector<aoc::crucible>,
                        std::greater<>>;
class grid {
  aoc::grid_data data;

 public:
  grid() = default;

  void print() const {
    aoc::crucible empty{{0, 0}, aoc::direction::east};
    print(empty);
  }
  void print(aoc::crucible &cruc) const {
    cout << "================================== " << cruc.heat_loss() << endl;
    for (auto y_index = 0; y_index < this->data.size(); y_index++) {
      const auto &tiles = this->data[y_index];
      for (auto x_index = 0; x_index < tiles.size(); x_index++) {
        const aoc::position tile_pos = {x_index, y_index};
        /*
        auto crucibles =
            std::accumulate(queue.begin(), queue.end(), 0,
                            [&](uint64_t acc, const auto &cruc) mutable {
                              if (cruc.position() == tile_pos) {
                                if (cruc.heat_loss() > acc) {
                                  return cruc.heat_loss();
                                }
                                return acc;
                              }
                              return acc;
                            });
        */
        auto dir = cruc.direction_on(tile_pos);
        if (dir.has_value()) {
          cout << aoc::to_char(dir.value());
        } else {
          cout << tiles[x_index];
        }
        // cout << "| " << crucibles << " |";
      }
      cout << endl;
    }
    cout << "==================================" << endl;
  }

  void add_line(const std::string &line) {
    const auto len = line.length();
    std::vector<uint32_t> ints(len);
    for (auto index = 0; index < len; index++) {
      auto ascii = line[index] - '0';
      auto my_uchar = static_cast<u_char>(ascii);
      auto my_int = static_cast<uint32_t>(my_uchar);
      ints[index] = my_int;
    }
    data.push_back(ints);
  }

  auto path_least_heat_loss() -> uint64_t {
    assert(!data.empty());

    aoc::crucible_queue crucs;
    aoc::position pos = {1, 0};
    crucs.emplace(pos, aoc::direction::east);
    //  crucs.emplace(aoc::direction::south);

    aoc::position destination = {data[0].size() - 1, data.size() - 1};

    auto i = 100;
    for (; !crucs.empty(); crucs.pop()) {
      // i--;
      auto cruc = crucs.top();
      print(cruc);
      auto new_crucs = cruc.move(this->data);
      if (cruc.position() == destination) {
        return cruc.heat_loss();
      }
      if (cruc.heat_loss() != UINT64_MAX && i >= 0) {
        // LATER: should replace UINT64_MAX with INFINITY
        crucs.push(cruc);
        for (auto &next_cruc : new_crucs) {
          crucs.push(next_cruc);
        }
      }
    }
    return UINT64_MAX;
  }
};

}  // namespace aoc

auto main() -> int {
  // mutable grid so we can build it line by line
  aoc::grid grid;
  std::string line;
  std::fstream infile(FILENAME);
  while (std::getline(infile, line)) {
    grid.add_line(line);
  }

  // grid.print();
  auto result1 = grid.path_least_heat_loss();
  cout << "Part 1 : " << result1 << endl;
}
