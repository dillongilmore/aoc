#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

// TODO: make this a CLI argument
#define FILENAME "tests/day1-part2.txt"

// TODO: static defs like these should be in a header
typedef struct stat stat_t;

typedef struct number {
  char *number;
  int digit;
  size_t length;
} number_t;

#define NUMBERS 9
const number_t NONUM = {.digit = 0, .number = "", .length = 0};
const number_t NUMS[NUMBERS] = {{.digit = 1, .number = "one", .length = 3},
                                {.digit = 2, .number = "two", .length = 3},
                                {.digit = 3, .number = "three", .length = 5},
                                {.digit = 4, .number = "four", .length = 4},
                                {.digit = 5, .number = "five", .length = 4},
                                {.digit = 6, .number = "six", .length = 3},
                                {.digit = 7, .number = "seven", .length = 5},
                                {.digit = 8, .number = "eight", .length = 5},
                                {.digit = 9, .number = "nine", .length = 4}};

inline bool is_nonum(number_t num) {
  return num.number == NONUM.number && num.length == NONUM.length;
}

number_t number_from_startswith(char *line) {
  for (size_t i = 0; i < NUMBERS; i++) {
    number_t num = NUMS[i];
    if (strncmp(num.number, line, num.length) == 0) {
      return num;
    }
  }
  return NONUM;
}

int main(void) {
  FILE *file = fopen(FILENAME, "r");
  if (file == NULL) {
    fprintf(stderr, "ERROR: could not open %s : got %d\n", FILENAME, errno);
    perror("fopen");
    return EXIT_FAILURE;
  }

  int fd = fileno(file);

  stat_t statb;
  int statr = fstat(fd, &statb);
  if (statr != 0) {
    fprintf(stderr, "ERROR: could not read %s informations : got %d\n",
            FILENAME, errno);
    perror("fstat");
    return EXIT_FAILURE;
  }

  size_t bufsize = (size_t)statb.st_size;
  char *linebuffer = (char *)malloc(bufsize);
  if (linebuffer == NULL) {
    fprintf(stderr, "ERROR: could not alloc mem to read %s : got %d\n",
            FILENAME, errno);
    perror("malloc");
    return EXIT_FAILURE;
  }

  // loop here
  int result = 0;
  while (getline(&linebuffer, &bufsize, file) != -1) {
    if (errno != 0) {
      fprintf(stderr, "ERROR: attempted to read a line from %s but got : %d\n",
              FILENAME, errno);
      perror("getline");
      continue;
    }
    int first = -1, last = -1;
    for (size_t i = 0; linebuffer[i]; i++) {
      int num = (int)(linebuffer[i] - '0');
      // TODO: make this a CLI argument on whether to check string digits
      char *numb = &linebuffer[i];
      number_t number = number_from_startswith(numb);
      if (!is_nonum(number)) {
        num = number.digit;
      }

      if (num >= 0 && num <= 9) {
        // we are a number indeed
        if (first == -1) {
          first = num;
        }
        last = num;
      }
    }
    if (first == -1 || last == -1) {
      fprintf(stderr, "WARN: expected a first and last int but got : %d : %d\n",
              first, last);
      continue;
    }

    int num = (first * 10) + last;
    printf("%s : %d : %d == %d (%d)\n", linebuffer, first, last, num, result);
    result += num;
  }

  printf("%d\n", result);

  if (linebuffer != NULL) {
    free(linebuffer);
  }
  fclose(file);
  return EXIT_SUCCESS;
}
