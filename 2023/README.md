# 2023

2023 Advent of Code was my first year taking part in the puzzles. It was
quite refreshing on how much I didn't know and also was impressed on
how well I did overall.

## Summary

I wanted to reflect on where I did strong, where I was weak
and what I can do to do better next year.

## Times

As of this commit here are my times:

```
      --------Part 1---------   --------Part 2--------
Day       Time    Rank  Score       Time   Rank  Score
 25       >24h   10143      0          -      -      -
 24   15:45:11   10223      0          -      -      -
 23   02:31:03    4060      0       >24h  13095      0
 22   23:21:41   11279      0          -      -      -
 21   01:46:26    5626      0          -      -      -
 19   02:00:00    5963      0   03:43:41   4105      0
 18   02:58:00    6727      0   03:14:24   3841      0
 16   02:41:42    6867      0   03:02:41   6634      0
 15   00:26:59    6334      0   02:34:31   8786      0
 14   00:50:30    6310      0   02:59:28   6409      0
 13   04:51:50   12498      0       >24h  27389      0
 12   10:16:52   18968      0          -      -      -
 11   02:22:54   11103      0   03:31:14  12208      0
 10   13:41:56   33210      0          -      -      -
  9   01:02:19    8961      0   01:03:26   8208      0
  8   01:36:36   13984      0   02:26:23  10018      0
  7   02:55:27   15159      0   03:53:58  14068      0
  6   01:14:36   13735      0   01:36:08  14333      0
  5   09:59:56   39983      0   12:01:55  23296      0
  4   01:25:06   16756      0   02:13:38  15510      0
  3   02:01:52   11982      0   02:34:19  10704      0
  2   01:11:46   12621      0   01:47:51  14296      0
  1   18:58:10  135919      0   19:13:06  95609      0

```

## Strong

### Logic puzzles

I was very fast on logic. I attribute this to 99% of what I do for work.
The days that were purely logic-based rather than algorithm or performance
focused, I was under the top 10k to finish.

## Needs Improvement

### Graph Theory

Given that I've never put the words *graph* and *theory* together before. I
am rather impressed on how close I got with some of these questions. I
*almost* re-invented *Dijkstra* without having heard it before.

Understanding how Dijkstra's works was something I got quite fast. Ultimately,
I did find it challenging to actually implement these algorithms in a performant
manner so while my understanding of graph theory is already quite strong, my
implementations need massive improvement.

## Weak

### Algebra

I'm surprised by this but I break this down into 3 parts.

1. Knowing when to use an algebraic formula
2. Knowing what that formula is supposed to be
3. Translating that formula into code

Normally, all 3 of these should be trivial for me but it turns out that when you
combine all 3 together, as I needed to do, that I wasn't able to do it. When looking
up Youtube videos on it, most people deferred to algebraic libraries that
automatically solved their equations. Translating the Python to C/C++ became
rather difficult in those situations. The top rated libraries _seemed_ to be
Intel's oneAPI MKL, which looks extremely impressive and I need to devout
quite some time on really understanding how to use it.

## What I learned?

This was my first serious usage of C and C++. I really wanted to stick to vanilla
C code, but it became quite clear, quite fast that wasn't going to be a good idea.
I also wanted to build a small library of internal symbols for re-use and package
it all nicely together with Autotools, but I simply didn't have the time to devout
to either of these tasks.

The first library I turned to was `glib`. I really wanted hash tables and real strings.
Glib offers both of these, but are quite difficult to use and you generally lose your
typing unless you keep track of hash tables as they are passed around, or simply
not pass them around.

At a certain point, I realized it was going to be better to defer to C++ so that I
can have the compiler help me through some of the more challenging problems. I
feel quite confident with my understanding of what's available in the standard
library, when and how to use them. I also really appreciated being able to have real
containers.

It was also very eye opening on how verbose and powerful, yet succinct C++ can be,
and at the same time it was easier to write poor performant code than in Python.
In a lot of cases most people were able to write higher performing solutions
than my C++ ones. I think if I spent a bit of time optimizing that my solutions could
easily outperform the Python ones, but it's clear that given the same amount of time
to optimize that a Python solution may perform better.

I think the most valuable lesson I learned is:

"Pre-mature optimizations are the root of all evils"

### Looking forward to next year's AoC

