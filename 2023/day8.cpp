#include <cstdint>
#include <cstdlib>
#include <errno.h>
#include <functional>
#include <iostream>
#include <numeric>
#include <ostream>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <sys/stat.h>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#define FILENAME "bin/2023/tests/day8-part1.txt"
// #define FILENAME "bin/2023/tests/day8-example2.txt"

typedef struct stat stat_t;

struct Map
{
  std::string id;
  std::string left;
  std::string right;

  Map () {}
  Map (std::string id, std::string left, std::string right)
      : id (id), left (left), right (right)
  {
  }

  explicit
  operator bool const ()
  {
    return !id.empty () && !left.empty () && !right.empty ();
  }

  friend inline bool
  operator!= (const Map &a, const Map &b)
  {
    return a.id != b.id;
  }
};

/*
constexpr auto
lcm (auto x, auto... xs)
{
  return ((x = std::lcm (x, xs)), ...);
}
*/

auto
steps_to_end (std::unordered_map<std::string, Map> maps, Map at,
              const std::string instructions,
              std::function<bool (const Map &)> done) -> uint64_t
{
  uint64_t results1 = 0;
  uint64_t inst_pos = 0;
  while (!done (at))
    {
      char inst = instructions[inst_pos];
      switch (inst)
        {
        case 'L':
          {
            Map nxt = maps[at.left];
            at = nxt;
            results1 += 1;
            break;
          }
        case 'R':
          {
            Map nxt = maps[at.right];
            at = nxt;
            results1 += 1;
            break;
          }
        case '\0':
          {
            break;
          }
        default:
          {
            fprintf (stderr, "ERROR: expected L or R got : %c from %s\n", inst,
                     instructions.c_str ());
          }
        }

      inst_pos += 1;
      if (inst_pos >= instructions.size ())
        {
          inst_pos = 0;
        }
    }
  return results1;
}

int
main ()
{
  FILE *file = fopen (FILENAME, "r");
  if (file == NULL)
    {
      fprintf (stderr, "ERROR: could not open %s : got %d\n", FILENAME, errno);
      perror ("fopen");
      return EXIT_FAILURE;
    }

  int fd = fileno (file);

  stat_t statb;
  int statr = fstat (fd, &statb);
  if (statr != 0)
    {
      fprintf (stderr, "ERROR: could not read %s informations : got %d\n",
               FILENAME, errno);
      perror ("fstat");
      return EXIT_FAILURE;
    }

  size_t bufsize = (size_t)statb.st_size;
  char *linebuffer = (char *)malloc (bufsize);
  if (linebuffer == NULL)
    {
      fprintf (stderr, "ERROR: could not alloc mem to read %s : got %d\n",
               FILENAME, errno);
      perror ("malloc");
      return EXIT_FAILURE;
    }

  uint64_t line = 0;
  long linesz_u = 0;
  size_t linesz = 0;

  std::string instructions;
  std::unordered_map<std::string, Map> maps;
  std::unordered_set<std::string> dupes;

  const std::string id_del = " = ";
  const std::string inst_del = " ";

  Map start;
  std::vector<Map> starts;

  while ((linesz_u = getline (&linebuffer, &bufsize, file)) != -1)
    {
      line++;
      if (errno != 0)
        {
          fprintf (
              stderr,
              "ERROR: (%ld) attempted to read a line from %s but got : %d\n",
              line, FILENAME, errno);
          perror ("getline");
          continue;
        }
      linesz = (size_t)linesz_u;
      linebuffer[linesz - 1] = '\0';
      fprintf (stderr, "INFO: Starting to process line %ld :: \n", line);

      std::string str (linebuffer, linesz);

      if (str.empty () || str.size () <= 1)
        {
          continue;
        }

      if (instructions.empty ())
        {
          if (str[0] != 'R' && str[0] != 'L')
            {
              continue;
            }
          instructions = str;
          printf ("%ld insts\n", instructions.size ());
          continue;
        }

      std::string id = str.substr (0, str.find (id_del));
      auto inserted = dupes.insert (id);
      if (!inserted.second)
        {
          puts ("DUPE!");
          return EXIT_FAILURE;
        }

      size_t lbegin = id.size () + id_del.size () + 1;
      std::string left = str.substr (lbegin, str.find (inst_del));
      size_t rbegin = lbegin + left.size () + id_del.size () - 1;
      // assuming right is the same size as left
      std::string right = str.substr (rbegin, left.size ());

      Map map (id, left, right);
      maps[id] = map;
      if (id[2] == 'A')
        {
          starts.push_back (map);
        }
      if (id == "AAA")
        {
          start = map;
        }
    }

  printf ("%ld maps\n", maps.size ());
  size_t inst_pos = 0;
  uint64_t results1
      = steps_to_end (maps, start, instructions,
                      [] (const Map &cur) { return cur.id == "ZZZ"; });

  size_t len = starts.size ();
  uint64_t results2 = 0;
  for (size_t i = 0; i < len; i++)
    {
      Map map = starts[i];
      uint64_t results
          = steps_to_end (maps, map, instructions,
                          [] (const Map &cur) { return cur.id[2] == 'Z'; });
      if (results2)
        {
          results2 = std::lcm (results2, results);
        }
      else
        {
          results2 = results;
        }
    }

  std::cout << "Part 1 : " << results1 << std::endl
            << "Part 2 : " << results2 << std::endl;
}
