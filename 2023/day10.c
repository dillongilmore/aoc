/*
 * Day 10 - ran into serious issues so refactoring onto `glib`
 *
 * WIP: does not work on part2
 */
#include <glib.h>
#include <stdbool.h>
#include <stdlib.h>

// #define FILENAME "bin/2023/tests/day10-part1.txt"
#define FILENAME "bin/2023/tests/day10-example1.txt"
// #define FILENAME "bin/2023/tests/day10-example2.txt"
// #define FILENAME "bin/2023/tests/day10-example3.txt"
// #define FILENAME "bin/2023/tests/day10-example4.txt"
// #define FILENAME "bin/2023/tests/day10-example5.txt"
// #define FILENAME "bin/2023/tests/day10-example6.txt"

typedef enum ConnectedOn { Input, Output } connected_on_t;

typedef enum direction {
  none,
  north,
  south,
  east,
  west,
} direction_t;
#define DIRS 5

gchar *dir_as_str(direction_t dir) {
  switch (dir) {
    case north:
      return "north";
    case south:
      return "south";
    case east:
      return "east";
    case west:
      return "west";
    case none:
      return "none";
  }
}

#define PIPE_ALIGN 16

typedef struct Pipe {
  direction_t i;
  direction_t o;
  gchar sym;
} __attribute__((aligned(PIPE_ALIGN))) pipe_t;

#define COORD_ALIGN 64

typedef struct Coordinates {
  pipe_t *pipe;
  guint64 x;
  guint64 y;
  bool loops;

  // unsure if this should go here
  struct Coordinates *next;
  struct Coordinates *prev;

  // hijacking this to avoid page faulting
  guint64 visits;
} __attribute__((aligned(COORD_ALIGN))) coord_t;

void grid_visit_reset(GArray *grid) {
  for (gsize y_index = 0; y_index < grid->len; y_index++) {
    GArray *x_axis = g_array_index(grid, GArray *, y_index);

    guint x_len = x_axis->len;
#pragma unroll 1
    for (gsize x_index = 0; x_index < x_len; x_index++) {
      coord_t *coord = g_array_index(x_axis, coord_t *, x_index);
      coord->visits = 0;
    }
  }
}

pipe_t *pipe_new(const gchar sym, const direction_t input,
                 const direction_t output) {
  pipe_t *ret = g_malloc(sizeof(pipe_t));
  ret->i = input;
  ret->o = output;
  ret->sym = sym;
  return ret;
}

void pipe_print(pipe_t *pipe) {
  if (pipe == NULL) {
    g_printerr("ERROR: didn't expect a null pipe to print\n");
    return;
  }

  g_print("Pipe<input=%d,output=%d,sym=%c>\n", pipe->i, pipe->o, pipe->sym);
}

void pipe_free(pipe_t *pipe) { g_free(pipe); }

// NOLINTBEGIN
G_DEFINE_AUTOPTR_CLEANUP_FUNC(pipe_t, pipe_free)
// NOLINTEND

void coord_free(coord_t *coord) {
  pipe_free(coord->pipe);
  g_free(coord);
}

// NOLINTBEGIN
G_DEFINE_AUTOPTR_CLEANUP_FUNC(coord_t, coord_free);
// NOLINTEND

GHashTable *pipe_lookup_table() {
  GHashTable *lookup_table = g_hash_table_new(g_direct_hash, g_direct_equal);

  gchar aul = 'L';
  g_hash_table_insert(lookup_table, GINT_TO_POINTER(aul),
                      pipe_new(aul, north, east));
  gchar avb = '|';
  g_hash_table_insert(lookup_table, GINT_TO_POINTER(avb),
                      pipe_new(avb, north, south));
  gchar ahh = '-';
  g_hash_table_insert(lookup_table, GINT_TO_POINTER(ahh),
                      pipe_new(ahh, east, west));
  gchar auj = 'J';
  g_hash_table_insert(lookup_table, GINT_TO_POINTER(auj),
                      pipe_new(auj, north, west));
  gchar ans = '7';
  g_hash_table_insert(lookup_table, GINT_TO_POINTER(ans),
                      pipe_new(ans, south, west));
  gchar auf = 'F';
  g_hash_table_insert(lookup_table, GINT_TO_POINTER(auf),
                      pipe_new(auf, south, east));
  gchar aus = 'S';
  g_hash_table_insert(lookup_table, GINT_TO_POINTER(aus),
                      pipe_new(aus, none, none));
  gchar add = '.';
  g_hash_table_insert(lookup_table, GINT_TO_POINTER(add),
                      pipe_new(add, none, none));

  return lookup_table;
}

const GHashTable *pipe_maybe_lookup_table(GHashTable *lookup_table) {
  if (lookup_table != NULL) {
    return lookup_table;
  }
  return pipe_lookup_table();
}

void pipe_lookup_table_clear(GHashTable *lookup_table) {
  // in case there are things like reference counts
  // GHashTable *lookup_table = pipe_lookup_table();
  g_hash_table_remove_all(lookup_table);
  g_hash_table_destroy(lookup_table);
}

pipe_t *pipe_from(gchar tile, GError **err) {
  GHashTable *pipe_map = pipe_lookup_table();
  gint64 pipe_key = (gint64)tile;
  pipe_t *pipe_ref = g_hash_table_lookup(pipe_map, GINT_TO_POINTER(pipe_key));
  if (pipe_ref == NULL) {
    const char *msg = "unexpected [%c]";
    g_printerr(msg, tile);
    *err = g_error_new(1, 1, msg, tile);
    // return an empty pipe just so we can track the symbol
    return NULL;
  }

  return pipe_new(tile, pipe_ref->i, pipe_ref->o);
}

bool pipe_is_routable(pipe_t *pipe) {
  return pipe->i != none && pipe->o != none;
}

coord_t *coord_new(pipe_t *pipe, guint64 x_idx, guint64 y_idx) {
  coord_t *coord = g_new0(coord_t, 1);
  g_assert(pipe != NULL);
  coord->visits = 0;
  coord->pipe = pipe;
  coord->x = x_idx;
  coord->y = y_idx;
  coord->loops = false;
  coord->next = NULL;
  coord->prev = NULL;
  return coord;
}

bool pipes_connected(const direction_t dir, const direction_t input,
                     const direction_t output, connected_on_t *conn) {
  g_assert(input != none && output != none);
  switch (dir) {
    case none: {
      g_printerr("ERROR: was not meant to be here\n");
      g_assert(dir != none);
      return false;
    }
    case north: {
      if (input == south) {
        *conn = Output;
        return true;
      }
      if (output == south) {
        *conn = Input;
        return true;
      }
    }
    case south: {
      if (input == north) {
        *conn = Output;
        return true;
      }
      if (output == north) {
        *conn = Input;
        return true;
      }
    }
    case east: {
      if (input == west) {
        *conn = Output;
        return true;
      }
      if (output == west) {
        *conn = Input;
        return true;
      }
    }
    case west: {
      if (input == east) {
        *conn = Output;
        return true;
      }
      if (output == east) {
        *conn = Input;
        return true;
      }
    }
  }
  return false;
}

void grid_set(GArray *grid, guint x_idx, guint y_idx, coord_t *new) {
  GArray *x_axis = g_array_index(grid, GArray *, y_idx);
  g_assert(x_axis != NULL);
  g_assert(x_axis->len > x_idx);
  g_array_insert_val(x_axis, x_idx, new);
  g_array_remove_index(x_axis - 1, x_idx);
}

coord_t *grid_lookup(GArray *grid, guint x_idx, guint y_idx) {
  GArray *x_axis = g_array_index(grid, GArray *, y_idx);
  g_assert(x_axis != NULL);
  coord_t *nxt = g_array_index(x_axis, coord_t *, x_idx);
  g_assert(nxt != NULL);
  return nxt;
}

void grid_print(GArray *grid) {
  for (int y_idx = 0; y_idx < grid->len; y_idx++) {
    GArray *x_axis = g_array_index(grid, GArray *, y_idx);
    for (int x_idx = 0; x_idx < x_axis->len; x_idx++) {
      coord_t *tile = grid_lookup(grid, x_idx, y_idx);
      g_print("%c", tile->pipe->sym);
    }
    g_print("\n");
  }
}

guint64 loop_build(GArray *grid, coord_t *start_pos, GArray **coords) {
  // gchar start = start_pos->pipe->sym;
  guint64 idx = start_pos->x;
  guint64 idy = start_pos->y;

  guint64 results = 0;
  guint64 max_y = grid->len;

  coord_t *prev = start_pos;
  if (!pipe_is_routable(prev->pipe)) {
    // expected: unbuildable loop - can just end here
    // g_printerr ("ERROR: %c is an unbuildable loop\n", prev->pipe->sym);
    return 0;
  }

  guint64 x_idx = 0;
  guint64 y_idx = 0;

  connected_on_t conn = Output;
  bool done = false;

  while (!done) {
    GArray *x_axis = g_array_index(grid, GArray *, idy);
    // gchar *x_axis = grid[y];
    guint64 max_x = x_axis->len - 1;

    direction_t prev_head = none;
    if (conn == Output) {
      prev_head = prev->pipe->o;
    } else {
      prev_head = prev->pipe->i;
    }

    // start by checking where this current pipe is able to connect to
    switch (prev_head) {
      case none: {
        g_printerr("ERROR: wasn't supposed to get none here\n");
        g_assert(prev_head != none);
        goto err;
      }
      case north: {
        x_idx = idx;

        if (idy <= 0) {
          // we're already at the top of the grid
          goto err;
        }
        y_idx = idy - 1;  // move up the grid
        break;
      }
      case south: {
        x_idx = idx;

        if (idy >= max_y) {
          // we're already at the bottom of the grid
          goto err;
        }
        y_idx = idy + 1;  // move down the grid
        break;
      }
      case east: {
        y_idx = idy;

        if (idx >= max_x) {
          // we're already at the right side of the grid
          goto err;
        }
        x_idx = idx + 1;  // move to the rigth of the grid
        break;
      }
      case west: {
        y_idx = idy;

        if (idx <= 0) {
          // we're already at the right side of the grid
          goto err;
        }
        x_idx = idx - 1;  // move to the left of the grid
        break;
      }
    }

    coord_t *coord = grid_lookup(grid, x_idx, y_idx);
    pipe_t *nxt = coord->pipe;
    gchar cur = nxt->sym;

    if (cur == 'S') {
      done = true;
      coord->next = start_pos->next;
      // this means we've finished our loop
    } else {
      // pipe_t *nxt = g_hash_table_lookup (lookup_table, GINT_TO_POINTER
      // (cur)); g_print ("%ld:%ld %c\n", nxt-> nxt->sym);
      if (!pipe_is_routable(nxt)) {
        // this means we've ran into a dead end
        goto err;
      }

      // check to see if this next pipe connects to the previous pipe
      if (!pipes_connected(prev_head, nxt->i, nxt->o, &conn)) {
        goto err;
      }
    }

    prev->next = coord;
    coord->prev = prev;

    results += 1;
    g_array_append_val(*coords, coord);
    prev = coord;
    idx = x_idx;
    idy = y_idx;
  }
  return results;
err:;
  return 0;
}

#define RESULTS_ALIGN 32
typedef struct Results {
  GArray *grid;
  GArray *actual_grid;
  coord_t *start;
  guint64 loop_length;
} __attribute__((aligned(RESULTS_ALIGN))) results_t;

pipe_t *pipe_copy(pipe_t *old) {
  pipe_t *new = pipe_new(old->sym, old->i, old->o);
  return new;
}

coord_t *coord_copy(coord_t *old) {
  pipe_t *pipe = pipe_copy(old->pipe);
  coord_t *new = coord_new(pipe, old->x, old->y);
  return new;
}

void loop_build_from_start(gchar sym, pipe_t *pipe, results_t *results) {
  coord_t *start = coord_copy(results->start);
  GHashTable *lookup_table = pipe_lookup_table();
  start->pipe = g_hash_table_lookup(lookup_table, GINT_TO_POINTER(sym));

  g_autoptr(GArray) coords = g_array_new(true, true, sizeof(coord_t *));
  // GArray *coords = g_array_new (true, true, sizeof (coord_t *));
  guint64 loop_length = loop_build(results->grid, start, &coords);
  if (loop_length > 0 && loop_length > results->loop_length) {
    results->loop_length = loop_length;

    // now that we've found our loop, let's mark as our pipes as part of the
    // loop
    for (gsize i = 0; i < coords->len; i++) {
      coord_t *coord = g_array_index(coords, coord_t *, i);
      coord->loops = true;
    }
    GArray *new_grid = g_array_copy(results->grid);
    grid_set(new_grid, start->x, start->y, start);

    results->actual_grid = new_grid;
  }
}

bool coords_equal(coord_t *left, coord_t *right) {
  return left->x == right->x && left->y == right->y;
}

#define CACHE_HITS_ALLOWED 1000
bool coord_can_escape_loop(GArray *grid, coord_t *coord) {
  if (coord->loops) {
    // this means we are part of the main loop, which
    // is not counted in our result, returning true
    // skips incrementing
    return true;
  }

  grid_visit_reset(grid);

  // arbitrary number so that if we happen to be passing through
  // a previous number we won't immediately exit
  guint64 cache_hits_allowed = CACHE_HITS_ALLOWED;

  guint64 y_mx = grid->len - 1;  // garray adds a 0 at the end
  guint64 x_mx = (g_array_index(grid, GArray *, 0))->len;

  coord_t *tile = coord_copy(coord);
  g_assert(tile != NULL);

  bool disabled_directions[DIRS] = {false, false, false, false, false};

  while (cache_hits_allowed > 0) {
    if (tile->x <= 0 || tile->x >= (x_mx - 1)) {
      // we are not part of the loop and we're on the edge
      return true;
    }
    if (tile->y <= 0 || tile->y >= (y_mx - 1)) {
      return true;
    }

    coord_t *goto_coord = NULL;

    for (gint i = 1; i <= 4; i++) {
      if (disabled_directions[i]) {
        // always reset the disable, we shoudl be required to set to
        // false every time we know we can't move a certain direction
        disabled_directions[i] = false;
        // this direction is disabled
        continue;
      }

      guint64 nxt_x = tile->x;
      guint64 nxt_y = tile->y;
      g_print("Start\n");
      g_print("%ld, %ld\n", nxt_x, nxt_y);
      direction_t movement = (direction_t)i;
      switch (movement) {
        case south:
          nxt_y++;
          break;
        case north:
          nxt_y--;
          break;
        case east:
          nxt_x++;
          break;
        case west:
          nxt_x--;
          break;
        case none:
          g_print("ERROR: don't use this movement\n");
          break;
      }
      g_print("%ld, %ld\n", nxt_x, nxt_y);
      coord_t *nxt = grid_lookup(grid, nxt_x, nxt_y);
      g_assert(nxt != NULL);

      if (nxt->loops) {
        bool north_south = false;
        coord_t *adjc_a = NULL;
        coord_t *adjc_b = NULL;

        switch (movement) {
          case south:
          case north:
            adjc_a = grid_lookup(grid, nxt_x - 1, nxt_y);
            adjc_b = grid_lookup(grid, nxt_x + 1, nxt_y);
            north_south = true;
            break;
          case east:
          case west:
            adjc_a = grid_lookup(grid, nxt_x, nxt_y - 1);
            adjc_b = grid_lookup(grid, nxt_x, nxt_y + 1);
            break;
          case none:
            g_print("ERROR: can't move into none\n");
            break;
        }
        bool is_an = coords_equal(adjc_a, nxt->next);
        bool is_ap = coords_equal(adjc_a, nxt->prev);
        bool is_bn = coords_equal(adjc_b, nxt->next);
        bool is_bp = coords_equal(adjc_b, nxt->prev);

        if ((is_an && is_bp) || (is_ap && is_bn)) {
          // this means we are blocked from moving down through
          // here as this pipe is connected to both adjacents
          // for example: `---` where `nxt` is the middle `-`
          continue;
        }

        if (is_an || is_bn) {
          // this means the `left` ($a) is connected to `nxt`
          // for example -7
          //              |
          // where the `7` is `nxt`. If `-` is `$a` then we must
          // block from doing and west movements. For east-west
          // it will be to block north movements.
          if (north_south) {
            disabled_directions[west] = true;
          } else {
            disabled_directions[north] = true;
          }
        } else if (is_ap || is_bp) {
          // inverse of above
          if (north_south) {
            disabled_directions[east] = true;
          } else {
            disabled_directions[south] = true;
          }
        }

        // OLD: this means this direction isn't even available to us
        // continue;
      }

      if (nxt->visits > cache_hits_allowed) {
        // we've visited this exact position `cache_hits_allowed`
        // times which means we are likely stuck in the middle of
        // the loop
        return false;
      }

      if (goto_coord == NULL || nxt->visits < goto_coord->visits) {
        // set this position as where we want to go as it's the least
        // visited
        goto_coord = nxt;
      }
    }

    if (goto_coord == NULL) {
      // this means all 4 sides are surrounded by loops
      // and we cannot escape - return false to increment the result
      return false;
    }

    // move into the least visited position, attempting
    // to find our way to the edges of the map
    goto_coord->visits++;
    // g_print ("%ld:%ld -> %ld:%ld || ", c->x, c->y, goto_x, goto_y);
    tile->x = goto_coord->x;
    tile->y = goto_coord->y;
  }

  return false;
}

results_t *results_new(GArray *grid, coord_t *start) {
  results_t *results = g_new0(results_t, 1);
  results->actual_grid = NULL;
  results->grid = grid;
  results->start = start;

  g_assert(results->start != NULL);
  g_assert(results->start->pipe != NULL);
  return results;
}

int main() {
  // some bookkeeping
  const gchar start_c = 'S';

  gchar *contents = NULL;
  GError *err = NULL;
  g_file_get_contents(FILENAME, &contents, NULL, &err);
  if (err != NULL) {
    g_printerr("could not open %s : but got %s\n", FILENAME, err->message);
    g_assert(contents == NULL);
  }

  // gchar **lines = g_strsplit (contents, "\n", 0);
  GStrv lines = g_strsplit(contents, "\n", 0);
  guint gsz = g_strv_length(lines);

  g_autoptr(GArray) grid = g_array_new(true, true, sizeof(GArray *));

  guint64 result1 = 0;  // result2 = 0;

  coord_t *start = NULL;

  // convert character grid into pipe grid
  for (guint64 y_index = 0; y_index < gsz; y_index++) {
    gchar *x_axis = lines[y_index];
    gsize xsz = strlen(x_axis);

    GArray *coords = g_array_new(true, true, sizeof(coord_t *));

    for (gint64 x_index = 0; x_index < xsz; x_index++) {
      gchar sym = x_axis[x_index];
      pipe_t *pipe = pipe_from(sym, NULL);
      coord_t *coord = coord_new(pipe, x_index, y_index);
      g_array_append_val(coords, coord);
      if (x_axis[x_index] == start_c) {
        start = coord;
      }
    }

    g_array_append_val(grid, coords);
  }
  GHashTable *lookup_table = pipe_lookup_table();
  results_t *results = results_new(grid, start);

  g_hash_table_foreach(lookup_table, (GHFunc)loop_build_from_start, results);
  grid_print(results->grid);
  grid_print(results->actual_grid);

  if (results->loop_length <= 0) {
    g_printerr("ERROR: could not find loop, programmer error\n");
  }
  g_assert(results->loop_length > 0);

  // at this point we have a rebuilt grid with pipes in the loop marked as in
  // the loop. We need to loop over every pipe and see if there is a path out
  // of the loop
  //
  // Our strategy is going to be really stupid:
  //
  // 1. Move left, and down 1 at a time
  // 2. If blocked left, switch to right and down
  // 3. If blocked right, switch to right and up
  // 4. if blocked right, switch to left and up
  // 5. Repeat at 1 until we notice our indexes are dupes
  guint64 result2 = 0;
  for (gsize y_index = 0; y_index < gsz; y_index++) {
    GArray *coords = g_array_index(grid, GArray *, y_index);
    for (gsize x_index = 0; x_index < coords->len; x_index++) {
      coord_t *coord = g_array_index(coords, coord_t *, x_index);
      if (!coord_can_escape_loop(grid, coord)) {
        result2 += 1;
      }
    }
  }

  result1 = results->loop_length / 2;

  g_print("Part 1 : %ld\nPart 2 : %ld\n", result1, result2);

  return EXIT_SUCCESS;
}
