#include <omp.h>

#include <atomic>
#include <fstream>
#include <iostream>
#include <memory>
#include <optional>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <tuple>
#include <vector>

// #define FILENAME "bin/2023/tests/day23-example1.txt"
#define FILENAME "bin/2023/tests/day23-part1.txt"

namespace aoc {

using pair = std::tuple<int64_t, int64_t>;

class path {
  pair pos_;
  uint64_t steps_ = 0;
  std::set<pair> visited_;
  // char prev_ = '.';

  path(const path &old, pair pos, std::set<pair> &visited)  //, char prev)
      : pos_(std::move(pos)), steps_(old.steps_ + 1), visited_(visited) {}
  // prev_(prev) {}

 public:
  explicit path(pair pos) : pos_(std::move(pos)) { visited_.insert(pos); }

  [[nodiscard]] auto steps() const -> uint64_t { return steps_; }
  /*
  [[nodiscard]] auto was_prev(char prev) const -> bool {
    // return prev_.has_value() && prev_.value() == prev;
    return prev_ == prev;
  };
  */

  [[nodiscard]] auto been(pair tile) const -> bool {
    return visited_.find(tile) != visited_.end();
  }
  [[nodiscard]] auto pos() const -> pair { return pos_; };
  [[nodiscard]] auto move(int64_t x_move, int64_t y_move) const
      -> std::optional<path> {
    auto [cur_x, cur_y] = pos_;
    pair nxt = {cur_x + x_move, cur_y + y_move};
    auto visited = visited_;
    auto [unused_, inserted] = visited.insert(nxt);
    if (!inserted) {
      return std::nullopt;
    }
    return path(*this, nxt, visited);  //, prev);
  }
};

auto pair_to_string(pair pair) -> std::string {
  auto [x, y] = pair;
  std::string ret;
  return std::to_string(x) + ", " + std::to_string(y);
}

auto pair_equal(pair cur, pair nxt) -> bool {
  auto [x_cur, y_cur] = cur;
  auto [x_nxt, y_nxt] = nxt;
  return x_cur == x_nxt && y_cur == y_nxt;
}

class grid {
  std::vector<std::string> data_;
  uint64_t y_mx_ = 0;
  uint64_t x_mx_ = 0;

  pair start_;
  pair end_;

 public:
  // grid(auto data, auto y_max, auto x_max) : data_(data), y_mx_(y_max),
  // x_mx_(x_max) {}

  void add_line(std::string line) {
    data_.push_back(line);
    for (auto x_index = 0; x_index < line.size(); x_index++) {
      auto tile = line[x_index];
      if (y_mx_ == 0 && tile == '.') {
        start_ = {x_index, y_mx_};
        x_mx_ = line.length();
        break;
      }
      if (tile == '.') {
        end_ = {x_index, y_mx_};
      }
    }
    y_mx_++;
  }

  void print(const path &path) const {
    std::cout << "s : " << aoc::pair_to_string(start_) << '\n';
    std::cout << "e : " << aoc::pair_to_string(end_) << '\n';
    std::cout << "========================================\n";
    for (auto y = 0; y < data_.size(); y++) {
      const auto &row = data_[y];
      for (auto x = 0; x < row.size(); x++) {
        if (path.been({x, y})) {
          std::cout << 'S';
        } else {
          std::cout << data_[y][x];
        }
      }
      std::cout << '\n';
    }
    std::cout << "========================================\n";
  }

  [[nodiscard]] auto start() const -> path {
    path path(start_);
    return path;
  }

  [[nodiscard]] auto at_end(const path &cur) const -> bool {
    auto [enx, eny] = end_;
    return pair_equal(cur.pos(), {enx, eny});
  }

  [[nodiscard]] auto get(size_t cur_x, size_t cur_y) const -> char {
    return data_[cur_y][cur_x];
  }

  [[nodiscard]] auto check(const path &path, int64_t nxt_x, int64_t nxt_y) const
      -> bool {
    auto nxt = get(nxt_x, nxt_y);
    if (nxt_y >= y_mx_ || nxt_x >= x_mx_) {
      return false;
    }
    if (nxt == '#') {
      return false;
    }
    // if (nxt != '.' && path.was_prev(nxt)) {
    //   return false;
    // }
    return true;
  }
};

/*
[[nodiscard]] auto done(const std::vector<bool> &dones) -> bool {
  for (const auto done : dones) {
    if (!done) {
      return false;
    }
  }
  return true;
}

auto next(
    std::vector<std::tuple<omp_lock_t *, std::shared_ptr<std::queue<path>>>>
        &queues,
    path &path, bool is_leader) {
  if (is_leader) {
    for (auto [mutex, queue] : queues) {
      if (omp_test_lock(mutex) != 0) {
        queue->push(path);
        omp_unset_lock(mutex);
        return;
      }
    }
  }
  // even as leader we can always fallback to our own queue
  auto [unused_, queue] = queues[omp_get_thread_num()];
  queue.push(path);
}
*/

}  // namespace aoc

auto main() -> int {
  aoc::grid grid_builder;

  std::string line;
  std::fstream infile(FILENAME);
  while (std::getline(infile, line)) {
    grid_builder.add_line(line);
  }

  const aoc::grid &grid = grid_builder;

  uint64_t result1 = 0;

  // omp_lock_t *leader;
  // omp_init_lock (leader);
  // std::vector<bool> dones(omp_get_num_threads());
  // std::vector<std::tuple<omp_lock_t*,
  // std::shared_ptr<std::queue<aoc::path>>>> queues(omp_get_num_threads()); for
  // (auto [lock, unused_] : queues) {
  //     omp_init_lock (lock);
  // }

  auto paths =
      std::make_shared<std::stack<aoc::path>>();  // std::queue<aoc::path>
                                                  // paths;
  paths->emplace(grid.start());
  omp_lock_t lock;
  omp_init_lock(&lock);

  int steps = 0;
  bool waited = false;
#pragma omp parallel default(none) \
    shared(paths, std::cout, steps, grid, result1, lock) private(waited)
  while (!paths->empty()) {
    // bool is_leader = false;
    // auto [mutex, queue] = queues[omp_get_thread_num()];
    // omp_set_lock (mutex);

    // 0 means not leader, non-zero is leader
    // if (omp_test_lock (leader) != 0) {
    //  is_leader = true;
    //  queue = paths;
    //  queues[omp_get_thread_num()] = {mutex, paths};
    //} else if (queue == nullptr) {
    //  auto new_queue = std::make_shared<std::queue<aoc::path>>();
    //  queues[omp_get_thread_num()] = {mutex, new_queue};
    //}

    // std::atomic_int thread = 0;

    bool locked = false;
    if (!waited) {
      omp_set_lock(&lock);
      locked = true;
    }

    std::optional<aoc::path> path_opt;
#pragma omp critical(queue)
    if (!paths->empty()) {
      // std::cout << steps << " | Paths " << paths->size() << '\n';
      path_opt = paths->top();
      paths->pop();
      // steps++;
      //  std::cout << "===start===\n";
      //  grid.print(path_opt.value());
      //  std::cout << "===end===\n";
    }

    if (!path_opt.has_value()) {
      continue;
    }

    std::vector<aoc::path> next_paths;
    auto path = path_opt.value();

    while (!grid.at_end(path)) {
      std::optional<aoc::path> next_path;
      auto [cur_x, cur_y] = path.pos();
      // auto cur = grid.get (cur_x, cur_y);
      //  std::cout << "at : " << aoc::pair_to_string(path.pos()) << '\n';

      // if ((cur == '.' || cur == '<') &&
      if (cur_x != 0 && grid.check(path, cur_x - 1, cur_y)) {
        auto nxt = path.move(-1, 0);
        if (nxt.has_value()) {
          // std::cout << "moving west\n";
          if (next_path.has_value()) {
            next_paths.push_back(nxt.value());
          } else {
            next_path = nxt;
          }
          // aoc::next (queues, nxt.value(), is_leader);
        }
      }

      // if ((cur == '.' || cur == '>') &&
      if (grid.check(path, cur_x + 1, cur_y)) {
        auto nxt = path.move(1, 0);
        if (nxt.has_value()) {
          // std::cout << "moving east\n";
          if (next_path.has_value()) {
            next_paths.push_back(nxt.value());
          } else {
            next_path = nxt;
          }
        }
      }

      // if ((cur == '.' || cur == '^') &&
      if (cur_y != 0 && grid.check(path, cur_x, cur_y - 1)) {
        auto nxt = path.move(0, -1);
        if (nxt.has_value()) {
          // std::cout << "moving north\n";
          if (next_path.has_value()) {
            next_paths.push_back(nxt.value());
          } else {
            next_path = nxt;
          }
        }
      }

      // if ((cur == '.' || cur == 'v') &&
      if (grid.check(path, cur_x, cur_y + 1)) {
        auto nxt = path.move(0, 1);
        if (nxt.has_value()) {
          // std::cout << "moving south\n";
          if (next_path.has_value()) {
            next_paths.push_back(nxt.value());
          } else {
            next_path = nxt;
          }
        }
      }

      if (!next_path.has_value()) {
        // std::cout << next_paths.size() << " next\n";
        // grid.print(path);
        break;
      }
      path = next_path.value();
    }
    if (grid.at_end(path))
#pragma omp critical(result)
    {
      if (path.steps() > result1) {
        std::cout << "Reached end at " << path.steps() << '\n';
        result1 = path.steps();
      }
    }
    for (auto &next : next_paths)
#pragma omp critical(queue)
    {
      paths->push(next);
    }
    if (locked) {
      omp_unset_lock(&lock);
      waited = true;
    }
  }

  std::cout << steps << " Part 1 : " << result1 << '\n';
}
