#include <errno.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

// #define FILENAME "bin/2023/tests/day6-example1.txt"
#define FILENAME "bin/2023/tests/day6-part1.txt"

#define BUFFER_DEFAULT 100

typedef struct stat stat_t;

typedef struct ns_result
{
  uint64_t num;
  char *err;
} ns_result_t;

const ns_result_t no_digit = { 0, "ERROR: Expected a digit" };

ns_result_t
number_from_str (const char *linebuffer, const char end, size_t *digits)
{
  ns_result_t ret = { 0, NULL };
  for (size_t i = 0; linebuffer[i]; i++)
    {
      char c = linebuffer[i];
      if (c == end || c == '\n')
        {
          // done
          break;
        }
      int digit = c - '0';
      if (digit < 0 || digit > 9)
        {
          fprintf (stderr, "%s  but got %c (%d)\n", no_digit.err, c, c);
          ret.err = no_digit.err;
          return ret;
        }

      ret.num *= 10;
      ret.num += (uint64_t)digit;
      if (digits != NULL)
        {
          digits[0]++;
        }
    }

  return ret;
}

uint64_t
num_bigger (uint64_t time, uint64_t goal)
{
  uint64_t bigger = 0;

  // Method: brute force
  for (size_t j = 1; j < time; j++)
    {
      uint64_t distance = (time - j) * j;
      if (distance > goal)
        {
          if (bigger >= INT64_MAX)
            {
              fprintf (stderr, "ERROR: we're going to overflow, exiting...\n");
            }
          bigger += 1;
        }
    }
  return bigger;
}

uint64_t
i64_pow (uint64_t num, size_t of)
{
  uint64_t ret = 1;
  for (size_t i = 0; i < of; i++)
    {
      ret *= num;
    }
  return ret;
}

int
main ()
{
  FILE *file = fopen (FILENAME, "r");
  if (file == NULL)
    {
      fprintf (stderr, "ERROR: could not open %s : got %d\n", FILENAME, errno);
      perror ("fopen");
      return EXIT_FAILURE;
    }

  int fd = fileno (file);

  stat_t statb;
  int statr = fstat (fd, &statb);
  if (statr != 0)
    {
      fprintf (stderr, "ERROR: could not read %s informations : got %d\n",
               FILENAME, errno);
      perror ("fstat");
      return EXIT_FAILURE;
    }

  size_t bufsize = (size_t)statb.st_size;
  char *linebuffer = (char *)malloc (bufsize);
  if (linebuffer == NULL)
    {
      fprintf (stderr, "ERROR: could not alloc mem to read %s : got %d\n",
               FILENAME, errno);
      perror ("malloc");
      return EXIT_FAILURE;
    }

  const char *TIMES = "Time: ";
  const char *DISTANCE = "Distance: ";

  uint64_t line = 0;
  long linesz_u = 0;
  size_t linesz = 0;

  uint64_t *digits_d = (uint64_t *)calloc (sizeof (uint64_t), BUFFER_DEFAULT);
  uint64_t *distances = (uint64_t *)calloc (sizeof (uint64_t), BUFFER_DEFAULT);
  size_t dist_sz = 0;

  uint64_t *digits_t = (uint64_t *)calloc (sizeof (uint64_t), BUFFER_DEFAULT);
  uint64_t *times = (uint64_t *)calloc (sizeof (uint64_t), BUFFER_DEFAULT);
  uint64_t largest_time = 0;
  size_t times_sz = 0;
  while ((linesz_u = getline (&linebuffer, &bufsize, file)) != -1)
    {
      line++;
      if (errno != 0)
        {
          fprintf (
              stderr,
              "ERROR: (%ld) attempted to read a line from %s but got : %d\n",
              line, FILENAME, errno);
          perror ("getline");
          continue;
        }
      linesz = (size_t)linesz_u;
      linebuffer[linesz] = '\0';
      fprintf (stderr, "INFO: Starting to process line %ld\n", line);

      size_t tsz = strlen (TIMES);
      if (strncmp (TIMES, linebuffer, tsz) == 0)
        {
          for (size_t i = tsz; linebuffer[i]; i++)
            {
              char c = linebuffer[i];
              if (c == ' ')
                {
                  continue;
                }
              size_t digits = 0;
              char *str = &linebuffer[i];
              ns_result_t result = number_from_str (str, ' ', &digits);
              if (result.err != NULL)
                {
                  fprintf (
                      stderr,
                      "ERROR: (time:%ld@%ld) expected to be able to extract a "
                      "num from [%s] of [%s]\n",
                      tsz, i, str, linebuffer);
                  return EXIT_FAILURE;
                }
              i += digits;
              times[times_sz] = result.num;
              digits_t[times_sz] = digits;
              times_sz++;
              if (result.num > largest_time)
                {
                  largest_time = result.num;
                }
            }
          continue;
        }

      size_t dsz = strlen (DISTANCE);
      if (strncmp (DISTANCE, linebuffer, dsz) == 0)
        {
          for (size_t i = dsz; linebuffer[i]; i++)
            {
              char c = linebuffer[i];
              if (c == ' ')
                {
                  continue;
                }

              size_t digits = 0;
              char *str = &linebuffer[i];
              ns_result_t result = number_from_str (str, ' ', &digits);
              if (result.err != NULL)
                {
                  fprintf (stderr,
                           "ERROR: (distance) expected to be able to extract "
                           "a num from "
                           ": %s\n",
                           str);
                  return EXIT_FAILURE;
                }
              i += digits;
              distances[dist_sz] = result.num;
              digits_d[dist_sz] = digits;
              dist_sz++;
            }
        }
    }

  if (dist_sz != times_sz)
    {
      fprintf (stderr,
               "ERROR: expected an equal number of distances and times d:%ld "
               "t:%ld\n",
               dist_sz, times_sz);
      return EXIT_FAILURE;
    }

  uint64_t result1 = 1;

  // MATHS: ($time - $i) * $i == distance traveled
  uint64_t total_time = 0;
  uint64_t total_distance = 0;
  for (size_t i = 0; i < times_sz; i++)
    {
      uint64_t time = times[i];
      uint64_t goal = distances[i];

      uint64_t overflow_time_check = total_time;
      uint64_t overflow_dist_check = total_distance;

      total_time *= i64_pow (10, digits_t[i]);
      total_time += time;
      if (total_time < overflow_time_check)
        {
          fprintf (stderr, "ERROR: detected integer overflow %ld > %ld\n",
                   overflow_time_check, total_time);
        }
      total_distance *= i64_pow (10, digits_d[i]);
      total_distance += goal;
      if (total_distance < overflow_dist_check)
        {
          fprintf (stderr, "ERROR: detected integer overflow %ld > %ld\n",
                   overflow_dist_check, total_distance);
        }

      result1 *= num_bigger (time, goal);
    }

  printf ("INFO: gathering result2 : t:%ld d:%ld\n", total_time,
          total_distance);
  uint64_t result2 = num_bigger (total_time, total_distance);

  printf ("Part 1 : %ld\nPart 2 : %ld\n", result1, result2);
  return EXIT_SUCCESS;
}
