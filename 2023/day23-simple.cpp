//
// Created by dgilmore on 12/30/23.
//

#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <map>
#include <memory>
#include <stack>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

namespace aoc {

using Pair = std::pair<size_t, size_t>;
using Vertex = std::tuple<int64_t, size_t, size_t>;

auto next(Pair pair) -> std::vector<Pair> {
  auto [row, col] = pair;
  return {
      {row + 1, col},
      {row - 1, col},
      {row, col + 1},
      {row, col - 1},
  };
}

using Graph = std::map<aoc::Pair, std::map<aoc::Pair, int64_t>>;
using GraphPtr = std::shared_ptr<const Graph>;
class DFSRecursive {
  std::set<Pair> seen_;
  Pair end_;
  GraphPtr graph_;

  [[nodiscard]] auto len(const std::vector<Vertex> &path) const -> int64_t {
    int64_t ret = 0;
    for (auto [num, row, col] : path) {
      ret += num;
    }
    return ret;
  }

 public:
  explicit DFSRecursive(Pair &end, GraphPtr &graph)
      : end_(end), graph_(graph) {}

  [[nodiscard]] auto path(Pair point) -> std::vector<Vertex> {
    if (point == end_) {
      return {{0, end_.first, end_.second}};
    }

    // m = -float("inf")
    int64_t max = 0;
    std::vector<Vertex> ret;

    seen_.insert(point);
    // seen.add(pt)
    // nx in graph[pt] :
    for (auto [nxt, num] : graph_->at(point)) {
      // if nx not in seen : m = max(m, dfs(nx) + graph[pt][nx])
      if (seen_.find(nxt) == seen_.end()) {
        auto res = path(nxt);
        auto length = len(res) + num;
        if (length > max) {
          max = length;
          ret.clear();
          ret = {
              {num, nxt.first, nxt.second},
          };
          ret.insert(ret.end(), res.begin(), res.end());
        }
      }
    }
    // seen.remove(pt)
    seen_.erase(point);
    return ret;
  }

  [[nodiscard]] auto size(Pair point) -> int64_t {
    int64_t ret = 0;
    for (auto [num, row, col] : path(point)) {
      std::cout << row << ", " << col << " -> " << num << '\n';
      ret += num;
    }
    return ret;
  }
};

}  // namespace aoc

auto main() -> int {
  // Specify the file path
  std::string file_path = "bin/2023/tests/day23-part1.txt";

  // Open the file using std::ifstream
  std::ifstream file_stream(file_path);

  // Check if the file is open
  if (!file_stream.is_open()) {
    std::cerr << "ERROR: opening file: " << file_path << '\n';
    return EXIT_FAILURE;
  }

  // Read the entire file into a string using iterators
  std::string file_content((std::istreambuf_iterator<char>(file_stream)),
                           std::istreambuf_iterator<char>());
  std::vector<std::string> grid;
  boost::split(grid, file_content, boost::is_any_of("\n"));
  // grid = open(0).read().splitlines()

  // start =  (0, grid[0].index("."))
  auto start = aoc::Pair(0, grid[0].find('.'));

  // end = (len(grid) - 1, grid[-1].index("."))
  auto len = grid.size() - 1;  // ending newline means there's an empty newline
                               // at the end of this vector
  auto end = aoc::Pair(len - 1, grid[len - 1].find('.'));

  std::vector<aoc::Pair> points{start, end};
  aoc::Graph graph = {
      {start, {}},
      {end, {}},
  };

  for (auto r_idx = 0; r_idx < len; r_idx++) {
    auto row = grid[r_idx];
    for (auto c_idx = 0; c_idx < row.size(); c_idx++) {
      auto pos = row[c_idx];
      if (pos == '#') {
        continue;
      }

      auto neighbors = 0;
      aoc::Pair nxt = {r_idx, c_idx};
      for (auto [nxt_r, nxt_c] : aoc::next(nxt)) {
        if (0 <= nxt_r && nxt_r < len && 0 <= nxt_c && nxt_c < row.size() &&
            grid[nxt_r][nxt_c] != '#') {
          neighbors++;
        }
      }
      if (neighbors >= 3) {
        points.push_back(nxt);
        graph[nxt] = {};
      }
    }
  }

  /*
    for r, row in enumerate(grid):
    for c, ch in enumerate(row):
    if ch == "#":
    continue
    neighbors = 0
    for nr, nc in [(r - 1, c), (r + 1, c), (r, c - 1), (r, c + 1)]:
    if 0 <= nr < len(grid) and 0 <= nc < len(grid[0]) and grid[nr][nc] != "#":
    neighbors += 1
    if neighbors >= 3:
    points.append((r, c))
    graph = {pt: {} for pt in points}
  */

  for (auto point : points) {
    auto [r_start, c_start] = point;
    std::stack<aoc::Vertex> stack;
    stack.emplace(0, r_start, c_start);
    std::set<aoc::Pair> seen;
    seen.insert(point);

    while (!stack.empty()) {
      auto [num, row, col] = stack.top();
      stack.pop();

      aoc::Pair pair = {row, col};

      if (num != 0 &&
          std::find(points.begin(), points.end(), pair) != points.end()) {
        graph[{r_start, c_start}][pair] = num;
        continue;
      }

      for (auto [r_nxt, c_nxt] : aoc::next(pair)) {
        aoc::Pair nxt = {r_nxt, c_nxt};
        auto [unused_, inserted] = seen.insert(nxt);
        if (!inserted) {
          continue;
        }
        if (0 <= r_nxt && r_nxt < len && 0 <= c_nxt && c_nxt < grid[0].size() &&
            grid[r_nxt][c_nxt] != '#') {
          stack.emplace(num + 1, r_nxt, c_nxt);
          // stack.append((n + 1, nr, nc));
        }
      }
    }
  }

  for (auto [key, junctions] : graph) {
    std::cout << "(" << key.first << ", " << key.second << ") : ";
    auto res = 0;
    for (auto [junction_, num] : junctions) {
      // std::cout << "(" << junction.first << "," << junction.second << ":" <<
      // num
      //           << ") ";
      // std::cout << num << " ";
      res += num;
    }
    std::cout << res << '\n';
  }

  aoc::GraphPtr graph_ptr = std::make_shared<aoc::Graph>(graph);
  aoc::DFSRecursive dfs(end, graph_ptr);
  auto part2 = dfs.size(start);
  std::cout << "Part 2 " << part2 << '\n';

  /*
for sr, sc in points:
stack = [(0, sr, sc)]
seen = {(sr, sc)}

while stack:
n, r, c = stack.pop()

if n != 0 and (r, c) in points:
graph[(sr, sc)][(r, c)] = n
continue

for dr, dc in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
nr = r + dr
nc = c + dc
if 0 <= nr < len(grid) and 0 <= nc < len(grid[0]) and grid[nr][nc] != "#" and
(nr, nc) not in seen: stack.append((n + 1, nr, nc)) seen.add((nr, nc))
  */
  // print(dfs(start))
}
