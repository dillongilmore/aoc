#include <cstdlib>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

#define FILENAME "bin/2023/tests/day19-part1.txt"
// #define FILENAME "bin/2023/tests/day19-example1.txt"

#define BEGIN "in"

using std::cout;
constexpr char endl = '\n';

namespace aoc {

using bounds_map = std::map<char, std::tuple<uint32_t, uint32_t>>;
using part = std::unordered_map<char, uint64_t>;

enum class op : u_char {
  less_than = '<',
  greater_than = '>',
};

enum class threshold_phase { tid, left, op, right, ret };

enum class input_phase { left, right };

auto part_from(const std::string &line) -> aoc::part {
  aoc::input_phase phase = aoc::input_phase::left;
  aoc::part part;

  char prop{};
  std::string int_tmp;
  for (const auto &nxt : line) {
    if (nxt == '{') {
      continue;
    }

    switch (phase) {
    case aoc::input_phase::left:
      if (nxt == '=') {
        phase = aoc::input_phase::right;
        continue;
      }
      prop = nxt;
      break;
    case aoc::input_phase::right:
      if (nxt == ',' || nxt == '}') {
        part.insert({prop, std::stoul(int_tmp)});
        phase = aoc::input_phase::left;
        int_tmp.clear();
      } else {
        int_tmp.push_back(nxt);
      }
      break;
    }
    if (nxt == '}') {
      break;
    }
  }

  return part;
}

class cond {
public:
  char letter;
  aoc::op op;
  uint64_t threshold;
  std::string ret;

  cond(auto letter, auto opp, auto threshold, std::string ret)
      : letter(letter), op(opp), threshold(threshold), ret(std::move(ret)) {}
};

class threshold {
  std::vector<aoc::cond> conditions;
  std::string fail;

public:
  static auto from(const std::string &line)
      -> std::tuple<std::string, aoc::threshold> {
    std::string tid;
    std::vector<aoc::cond> conditions;
    std::string fail;

    size_t end_index{};
    for (auto index = line.length() - 1; index > 0; index--) {
      const auto &nxt = line[index];
      if (nxt == '}') {
        continue;
      }
      if (nxt == ',') {
        end_index = index;
        break;
      }
      fail.insert(0, 1, nxt);
    }

    aoc::threshold_phase phase = aoc::threshold_phase::tid;
    char letter{};
    aoc::op opp{};
    std::string threshold;
    std::string ret;
    for (auto index = 0; index < end_index + 1; index++) {
      const auto &nxt = line[index];
      switch (phase) {
      case aoc::threshold_phase::tid:
        if (nxt == '{') {
          phase = aoc::threshold_phase::left;
          continue;
        }
        tid.push_back(nxt);
        break;
      case aoc::threshold_phase::left:
        letter = nxt;
        phase = aoc::threshold_phase::op;
        break;
      case aoc::threshold_phase::op:
        opp = static_cast<aoc::op>(nxt);
        phase = aoc::threshold_phase::right;
        break;
      case aoc::threshold_phase::right:
        if (nxt == ':') {
          phase = threshold_phase::ret;
          continue;
        }
        threshold.push_back(nxt);
        break;
      case aoc::threshold_phase::ret:
        if (nxt == ',') {
          conditions.emplace_back(letter, opp, std::stoul(threshold), ret);
          threshold.clear();
          ret.clear();
          phase = aoc::threshold_phase::left;
          continue;
        }
        ret.push_back(nxt);
        break;
      }
    }

    return {tid, {conditions, fail}};
  }

  threshold(std::vector<aoc::cond> &conditions, std::string &fail)
      : conditions(conditions), fail(fail) {}

  auto next(aoc::part part) -> std::string {
    for (const auto &cond : conditions) {
      if (part.contains(cond.letter)) {
        auto threshold = part[cond.letter];

        switch (cond.op) {
        case aoc::op::less_than:
          if (threshold < cond.threshold) {
            return cond.ret;
          }
          break;
        case aoc::op::greater_than:
          if (threshold > cond.threshold) {
            return cond.ret;
          }
          break;
        }
      }
    }
    return fail;
  }

  static void
  append_map(std::map<std::string, std::vector<aoc::bounds_map>> &ret,
             auto &cond, auto &updated_map) {
    if (ret.contains(cond)) {
      ret[cond].push_back(updated_map);
    } else {
      std::vector<aoc::bounds_map> new_maps{updated_map};
      ret[cond] = new_maps;
    }
  }

  auto bound_paths(aoc::bounds_map bounds_map)
      -> std::map<std::string, std::vector<aoc::bounds_map>> {
    std::map<std::string, std::vector<aoc::bounds_map>> ret;
    for (const auto &cond : conditions) {
      aoc::bounds_map updated_map(bounds_map);
      auto [lower_bounds, upper_bounds] = updated_map.at(cond.letter);
      auto [lower_oppo, upper_oppo] = bounds_map.at(cond.letter);

      switch (cond.op) {
      case aoc::op::greater_than:
        lower_bounds = cond.threshold + 1;
        upper_oppo = cond.threshold;
        break;
      case aoc::op::less_than:
        upper_bounds = cond.threshold - 1;
        lower_oppo = cond.threshold;
        break;
      }
      updated_map[cond.letter] = {lower_bounds, upper_bounds};
      bounds_map[cond.letter] = {lower_oppo, upper_oppo};
      append_map(ret, cond.ret, updated_map);
    }
    append_map(ret, fail, bounds_map);
    return ret;
  }
};

} // namespace aoc

auto count_bounds(const auto &thresholds, const auto &bounds, std::string nxt)
    -> uint64_t {
  std::vector<aoc::bounds_map> all_maps;
  uint64_t additional = 0;
  if (thresholds.contains(nxt)) {
    auto thresh = thresholds.at(nxt);
    auto paths = thresh.bound_paths(bounds);
    for (auto [nxt_in, updated_bounds] : paths) {
      for (const auto &updated_bound : updated_bounds) {
        if (nxt_in == "R") {
          continue;
        }
        if (nxt_in == "A") {
          all_maps.push_back(updated_bound);
          continue;
        }
        additional += count_bounds(thresholds, updated_bound, nxt_in);
      }
    }
  } else {
    cout << "ERROR: threshold not found, " << nxt << endl;
    return 0;
  }
  uint64_t result = 0;
  for (const auto &map : all_maps) {
    uint64_t res = 1;
    for (const auto &[unused, bounds] : map) {
      const auto &[lower, upper] = bounds;
      // cout << unused << "(" << lower << "," << upper << ") " << endl;
      res *= upper - lower + 1;
    }
    result += res;
    // cout << endl;
  }
  uint64_t ret = result + additional;
  // cout << "RETURN   " << ret << endl;
  return result + additional;
}

enum class phase { thresholds, inputs };

constexpr int32_t MAX_BOUNDS = 4000;
auto main() -> int {

  std::map<std::string, aoc::threshold> thresholds;
  std::vector<aoc::part> parts;

  std::string line;
  std::fstream infile(FILENAME);

  phase doc_phase = phase::thresholds;
  while (std::getline(infile, line)) {
    if (line.empty()) {
      doc_phase = phase::inputs;
      continue;
    }

    switch (doc_phase) {
    case phase::inputs:
      parts.push_back(aoc::part_from(line));
      break;
    case phase::thresholds:
      auto [tid, thresh] = aoc::threshold::from(line);
      thresholds.insert({tid, thresh});
      break;
    }
  }

  uint64_t result1 = 0;
  for (const auto &part : parts) {
    std::string nxt = "in";
    while (nxt != "A" && nxt != "R") {
      if (thresholds.contains(nxt)) {
        aoc::threshold thresh = thresholds.at(nxt);
        std::string res = thresh.next(part);
        nxt = res;
      } else {
        cout << "ERROR: threshold not found, " << nxt << endl;
      }
    }

    if (nxt == "A") {
      for (auto [unused_, val] : part) {
        result1 += val;
      }
    }
  }

  const aoc::bounds_map bounds = {
      {'x', {1, MAX_BOUNDS}},
      {'m', {1, MAX_BOUNDS}},
      {'a', {1, MAX_BOUNDS}},
      {'s', {1, MAX_BOUNDS}},
  };
  uint64_t result2 = count_bounds(thresholds, bounds, "in");

  cout << "Part 1 : " << result1 << endl
       << "Part 2 : " << result2 << endl
       << "         167409079868000" << endl;

  return EXIT_SUCCESS;
}
