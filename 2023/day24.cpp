
#include <mkl.h>

#include <algorithm>
#include <array>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

// #define FILENAME "bin/2023/tests/day24-example1.txt"
#define FILENAME "bin/2023/tests/day24-part1.txt"

namespace aoc {

constexpr int DECIMAL = 10;

auto trim_strtol(std::string str) -> int64_t {
  boost::trim(str);

  return std::strtol(str.c_str(), nullptr, DECIMAL);
}

constexpr uint64_t DIM = 3;
constexpr size_t X = 0;
constexpr size_t Y = 1;
template <size_t N>
class ice {
  std::array<double_t, N> pos_{};
  std::array<double_t, N> velocity_{};

  double_t a;
  double_t b;
  double_t c;

 public:
  ice(ice &&) = default;
  auto operator=(const ice &) -> ice & = default;
  auto operator=(ice &&) -> ice & = default;
  ice(const ice &old) = default;
  ~ice() = default;

  explicit ice(std::string &line) {
    std::vector<std::string> parts;
    boost::split(parts, line, boost::is_any_of("@"));
    assert(parts.size() == 2);

    auto coords = parts[0];
    auto velocity = parts[1];

    std::vector<std::string> coord_v;
    std::vector<std::string> velocity_v;

    boost::split(coord_v, coords, boost::is_any_of(","));
    boost::split(velocity_v, velocity, boost::is_any_of(","));

    assert(coord_v.size() == 3);
    assert(velocity_v.size() == 3);

    std::transform(coord_v.begin(), coord_v.end(), pos_.begin(),
                   aoc::trim_strtol);
    std::transform(velocity_v.begin(), velocity_v.end(), velocity_.begin(),
                   aoc::trim_strtol);

    a = velocity_[Y];
    b = velocity_[X] * -1;
    c = velocity_[Y] * pos_[X] - velocity_[X] * pos_[Y];
  }

  [[nodiscard]] auto move() const -> ice {
    ice obj(*this);
    for (auto i = 0; i < N; i++) {
      obj.pos_[i] = pos_[i] + velocity_[i];
    }
    return obj;
  }

  void print() const {
    for (auto pos : pos_) {
      std::cout << pos << ", ";
    }
    std::cout << " @ ";
    for (auto vel : velocity_) {
      std::cout << vel << ", ";
    }

    std::cout << " (" << a << "," << b << "," << c << ")\n";
  }

  [[nodiscard]] auto is_parallel(const ice &right) const -> bool {
    const auto &left = *this;
    return left.a * right.b == right.a * left.b;
  }

  [[nodiscard]] auto is_future(double_t x, double_t y) const -> bool {
    return (x - pos_[X]) * velocity_[X] >= 0 &&
           (y - pos_[Y]) * velocity_[Y] >= 0;
  }

  [[nodiscard]] constexpr auto dvel(size_t index) const -> double {
    auto ret = static_cast<double>(velocity_[index]);
    return ret;
  }

  [[nodiscard]] constexpr auto dpos(size_t index) const -> double {
    auto ret = static_cast<double>(pos_[index]);
    return ret;
  }

  auto crossed(const ice &right, double_t lower, double_t upper) -> bool {
    const auto &left = *this;
    if (left.is_parallel(right)) {
      return false;
    }

    auto denom = (left.a * right.b - right.a * left.b);
    auto x = (left.c * right.b - right.c * left.b) / denom;
    auto y = (right.c * left.a - left.c * right.a) / denom;

    if (lower <= x && x <= upper && lower <= y && y <= upper) {
      return left.is_future(x, y) && right.is_future(x, y);
    }
    return false;
  }
};

using ice3 = ice<DIM>;

void grid_print(const std::vector<aoc::ice3> &grid) {
  for (const auto &ice : grid) {
    ice.print();
  }
}

void rock_throw(const std::vector<aoc::ice3> &grid) {
  auto len = grid.size();

  // Thrower's initial position (you can set this to an initial guess)
  constexpr int32_t VARS = 6;
  std::array<double, VARS> variables = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};

  MKL_INT num = 3 * len;

  constexpr int32_t unknowns = 3 * 3;
  std::vector<double> coefficient_matrix(unknowns * len);
  std::vector<double> results(3 * len);  // Right-hand side vector
  std::vector<MKL_INT> ipiv(3 * len);

  MKL_INT info{};

  // Set up the coefficient matrix and right-hand side vector
  for (int i = 0; i < len; i++) {
    for (int j = 0; j < 3; j++) {
      coefficient_matrix[i * unknowns + j] = 1.0;
      // coefficient_matrix[i * unknowns + 3 + j] = -objectVelocities[i][j];
      coefficient_matrix[i * unknowns + 3 + j] =
          -grid[i].dvel(j);  // objectVelocities[i][j];
      coefficient_matrix[i * unknowns + VARS + j] = 0.0;

      results[i * 3 + j] =
          grid[i].dpos(j);  // objectPositions[i][j];  // - throwerPosition[j];
    }
  }

  // Perform LU decomposition
  dgetrf(&num, &num, coefficient_matrix.data(), &num, ipiv.data(), &info);

  if (info == 0) {
    // Solve the system using LU decomposition
    dgetrs("N", &num, &num, coefficient_matrix.data(), &num, ipiv.data(),
           results.data(), &num, &info);

    if (info == 0) {
      // The solution is in the B vector
      for (int i = 0; i < VARS; i++) {
        // throwerVelocity[i] = B[i];
        variables[i] = results[i];
      }

      // Print the results
      std::cout << "Thrower Position: " << variables[0] << ", " << variables[1]
                << ", " << variables[2];
      std::cout << "\nThrower Velocity: " << variables[3] << ", "
                << variables[4] << ", " << variables[5];
      std::cout << "\n";
    } else {
      std::cerr << "Error solving the system.\n";
    }
  } else {
    std::cerr << "Error in LU decomposition.\n";
  }
}

}  // namespace aoc

// Part 1 - Example 1
// constexpr double_t LOWER = 7;
// constexpr double_t UPPER = 27;
// Part 1
constexpr double_t LOWER = 200000000000000;
constexpr double_t UPPER = 400000000000000;

auto main() -> int {
  std::vector<aoc::ice3> grid;

  std::string line;
  std::fstream infile(FILENAME);

  while (std::getline(infile, line)) {
    aoc::ice3 ice(line);
    grid.push_back(ice);
  }

  aoc::grid_print(grid);

  uint64_t result1 = 0;
  for (auto i = 0; i < grid.size(); i++) {
    auto cur = grid[i];
    for (auto j = i + 1; j < grid.size(); j++) {
      auto nxt = grid[j];
      if (cur.crossed(nxt, LOWER, UPPER)) {
        result1++;
      }
    }
  }

  std::cout << "Part 1 : " << result1 << '\n';

  rock_throw(grid);

  return EXIT_SUCCESS;
}
