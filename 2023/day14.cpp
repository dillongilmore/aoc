
#include <cstdint>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <optional>
#include <string>
#include <unordered_set>
#include <vector>

// #define FILENAME "bin/2023/tests/day14-example1.txt"
#define FILENAME "bin/2023/tests/day14-part1.txt"
using std::cout;
constexpr char endl = '\n';

enum class Object { Nothing, Round, Cube };

inline char object_to_char(Object obj) {
  switch (obj) {
  case Object::Nothing:
    return '.';
  case Object::Round:
    return 'O';
  case Object::Cube:
    return '#';
  }
}

inline std::optional<Object> char_to_object(char obj) {
  switch (obj) {
  case '.':
    return Object::Nothing;
  case 'O':
    return Object::Round;
  case '#':
    return Object::Cube;
  default:
    cout << "ERROR: unhandled object" << endl;
  }
  return std::nullopt;
}

class Point {
public:
  const uint64_t x;
  const uint64_t y;
  Point(uint64_t xxx, uint64_t yyy) : x(xxx), y(yyy) {}
};

class Grid {
  std::vector<std::vector<Object>> data;
  uint64_t row_sz = 0;

  void move_object(Point src, Point dest, Object object) {
    // if line_number == yindex then the object doesn't even move
    // move this object dest the highest it can go
    auto old_object = data[dest.y][dest.x];
    data[dest.y][dest.x] = object;
    data[src.y][src.x] = old_object;
  }

  // LATER: likely some opportunity to share code
  void spin_north() {
    std::vector<std::optional<uint64_t>> col_max(row_sz);
    for (auto yindex = 0; yindex < data.size(); yindex++) {
      auto &row = data[yindex];

      // #pragma omp parallel for
      for (auto xindex = 0; xindex < row_sz; xindex++) {
        auto object = row[xindex];
        switch (object) {
        case Object::Nothing:
          break;
        case Object::Cube:
          col_max[xindex] = yindex + 1;
          break;
        case Object::Round:
          auto line = col_max.at(xindex).value_or(0);
          move_object(Point(xindex, yindex), Point(xindex, line), object);
          col_max[xindex] = line + 1;
          break;
        }
      }
      // cout << endl;
    }
  }

  void spin_south() {
    std::vector<std::optional<uint64_t>> col_min(row_sz);
    auto line_number_mx = data.size() - 1;
    for (auto yyindex = data.size(); yyindex > 0; yyindex--) {
      auto yindex = yyindex - 1;
      auto &row = data[yindex];

      // #pragma omp parallel for
      for (auto xindex = 0; xindex < row_sz; xindex++) {
        auto object = row[xindex];
        switch (object) {
        case Object::Nothing:
          break;
        case Object::Cube:
          col_min[xindex] = yindex - 1;
          break;
        case Object::Round:
          auto line = col_min.at(xindex).value_or(line_number_mx);
          // if line_number == yindex then the object doesn't even move
          move_object(Point(xindex, yindex), Point(xindex, line), object);
          col_min[xindex] = line - 1;
          break;
        }
      }
    }
  }

  void spin_east() {
    std::vector<std::optional<uint64_t>> row_min(row_sz);
    auto col_mx = row_sz - 1;
    for (auto xxindex = row_sz; xxindex > 0; xxindex--) {
      auto xindex = xxindex - 1;

      // #pragma omp parallel for
      for (auto yindex = 0; yindex < data.size(); yindex++) {
        auto object = data[yindex][xindex];
        switch (object) {
        case Object::Nothing:
          break;
        case Object::Cube:
          row_min[yindex] = xindex - 1;
          break;
        case Object::Round:
          auto vline = row_min.at(yindex).value_or(col_mx);
          // if line_number == yindex then the object doesn't even move
          move_object(Point(xindex, yindex), Point(vline, yindex), object);
          row_min[yindex] = vline - 1;
          break;
        }
      }
    }
  }

  void spin_west() {
    std::vector<std::optional<uint64_t>> row_max(row_sz);
    for (auto xindex = 0; xindex < row_sz; xindex++) {
      // #pragma omp parallel for
      for (auto yindex = 0; yindex < data.size(); yindex++) {
        auto object = data[yindex][xindex];
        switch (object) {
        case Object::Nothing:
          break;
        case Object::Cube:
          row_max[yindex] = xindex + 1;
          break;
        case Object::Round:
          auto vline = row_max.at(yindex).value_or(0);
          // if line_number == yindex then the object doesn't even move
          move_object(Point(xindex, yindex), Point(vline, yindex), object);
          row_max[yindex] = vline + 1;
          break;
        }
      }
    }
  }

public:
  std::string to_string() {
    std::string ret;
    for (auto &row : data) {
      for (auto object : row) {
        ret = ret.append(1, object_to_char(object));
      }
      ret = ret.append(1, '\n');
    }
    return ret;
  }

  void print() { cout << to_string(); }

  void set_row_sz(uint64_t size) { row_sz = size; }

  bool insert_line(std::string line) {
    std::vector<Object> row;
    for (auto index = 0; index < row_sz; index++) {
      char obj = line[index];
      std::optional<Object> object = char_to_object(obj);
      if (!object.has_value()) {
        cout << "ERROR: can't continue" << endl;
        return false;
      }

      switch (object.value()) {
      case Object::Nothing:
        row.push_back(Object::Nothing);
        break;
      case Object::Cube:
        row.push_back(Object::Cube);
        break;
      case Object::Round:
        row.push_back(Object::Round);
        break;
      }
    }
    data.push_back(row);
    return true;
  }

  uint64_t north_load() {
    uint64_t ret = 0;
    for (auto index = 0; index < data.size(); index++) {
      for (auto object : data[index]) {
        if (object == Object::Round) {
          ret += data.size() - index;
        }
      }
    }
    return ret;
  }

  void spin() {
    spin_north();
    spin_west();
    spin_south();
    spin_east();
  }
};

// arbitrary number for how many times until we're effectively done
constexpr uint64_t CACHE_TIMES = 10000;
// constexpr uint64_t SPINS = 1000000000;
constexpr uint64_t SPINS = 10000;
// constexpr uint64_t SPINS = 1;

int main() {
  uint64_t result1 = 0;
  std::string line;
  std::fstream infile(FILENAME);
  std::optional<uint64_t> row_sz = std::nullopt;

  Grid grid;

  while (std::getline(infile, line)) {
    if (!row_sz.has_value()) {
      row_sz = line.length();
      grid.set_row_sz(line.length());
    }
    grid.insert_line(line);
  }

  grid.print();
  cout << endl;

  std::unordered_set<std::string> cache;
  uint64_t cache_count = 0;
  for (auto spins = 0; spins < SPINS; spins++) {
    grid.spin();
    auto [i, inserted] = cache.insert(grid.to_string());
    if (!inserted) {
      cache_count++;
    }
    if (cache_count >= CACHE_TIMES) {
      cout << "Same pattern for " << CACHE_TIMES << endl;
      break;
    }
    cout << "Done " << spins << endl;
  }
  grid.print();

  result1 = grid.north_load();
  cout << "Part 1 : " << result1 << endl;
  return EXIT_SUCCESS;
}
