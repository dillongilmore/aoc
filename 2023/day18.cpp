#include <cstdlib>
#include <fstream>
#include <glib.h>
#include <iostream>
#include <string>
#include <tuple>
#include <vector>

// #define FILENAME "bin/2023/tests/day18-example1.txt"
#define FILENAME "bin/2023/tests/day18-part1.txt"

using std::cout;

constexpr char endl = '\n';

namespace aoc {

enum class direction : u_char { right = 'R', down = 'D', left = 'L', up = 'U' };

class tile {
  std::string color{};
  bool included_ = false;

public:
  tile() = default;
  explicit tile(std::string &color) : color(color) {}

  bool included() const { return included_; }
};

using pair = std::tuple<int64_t, int64_t>;
aoc::pair pair_add(const aoc::pair &one, const aoc::pair &two) {
  auto [first_x, first_y] = one;
  auto [sec_x, sec_y] = two;
  return {first_x + sec_x, first_y + sec_y};
}

aoc::pair pair_diff(const aoc::pair &one, const aoc::pair &two) {
  auto [first_x, first_y] = one;
  auto [sec_x, sec_y] = two;
  return {first_x - sec_x, first_y - sec_y};
}

aoc::pair pair_with_bounds(aoc::pair bounds, int64_t x_pos) {
  auto [left, right] = bounds;
  if (left > x_pos) {
    left = x_pos;
  } else if (right < x_pos) {
    right = x_pos;
  }
  return {left, right};
}

class grid {
  int64_t negative_offset = 0;
  std::vector<aoc::pair> data = {{0, 0}};
  aoc::pair pos;
  uint64_t total_len = 0;

  // LATER, research what these mean
  int64_t sum1 = 0;
  int64_t sum2 = 0;

  void update_pair(aoc::pair pair) {
    auto [pos_x, pos_y] = pos;
    data[pos_y + negative_offset] = pair;
  }

public:
  grid() = default;

  void process_hex_instruction(const std::string &line) {
    GStrv strings = g_strsplit(line.c_str(), " ", 0);
    std::string hex(strings[2]);
    char last_digit = hex[hex.length() - 2];
    std::string s_steps =
        hex.substr(2, 5); // starts on index 2 and is 5 digits long
    auto steps = std::stol(s_steps, nullptr, 16);
    aoc::direction dir{};
    switch (last_digit) {
    case '0':
      dir = aoc::direction::right;
      break;
    case '1':
      dir = aoc::direction::down;
      break;
    case '2':
      dir = aoc::direction::left;
      break;
    case '3':
      dir = aoc::direction::up;
      break;
    default:
      cout << "ERROR: unsupported direction\n";
    }
    cout << last_digit << " - " << s_steps << " - " << steps << endl;
    return process_instruct_(dir, steps);
  }

  void process_instruction(const std::string &line) {
    GStrv strings = g_strsplit(line.c_str(), " ", 0);
    std::string sdir(strings[0]);
    auto dir = static_cast<aoc::direction>(sdir[0]);
    std::string s_steps(strings[1]);
    auto steps = std::stoi(s_steps);
    return process_instruct_(dir, steps);
  }

  void process_instruct_(aoc::direction dir, uint64_t steps) {
    this->total_len += steps;

    auto modifier = 1;

    auto cur_pos = this->pos;
    auto [cur_x, cur_y] = cur_pos;

    switch (dir) {
    case aoc::direction::up:
      modifier = -1;
    case aoc::direction::down:
      this->pos = pair_add(this->pos, {0, steps * modifier});
      break;
    case aoc::direction::left:
      modifier = -1;
    case aoc::direction::right:
      this->pos = pair_add(this->pos, {steps * modifier, 0});
      break;
    }

    auto [next_x, next_y] = this->pos;
    auto [x_diff, y_diff] = pair_diff(cur_pos, this->pos);
    // cout << "(" << next_x << ", " << next_y << ")\n";

    /* OLD
    if (x_diff != 0) {
      auto new_pair =
          aoc::pair_with_bounds(data[next_y + negative_offset], next_x);
      data[next_y + negative_offset] = new_pair;
    } else if (y_diff != 0) {
      for (auto step = 1; step <= steps; step++) {
        auto index = step * modifier;
        auto data_y = cur_y + negative_offset + index;
        aoc::pair new_pair = {cur_x, cur_x};
        if (data_y == -1) {
          negative_offset++;
          data.insert(data.begin(), new_pair);
        } else if (data.size() <= data_y) {
          data.insert(data.begin() + data_y, new_pair);
        } else {
          // Y exists, we need to re-check X
          new_pair = aoc::pair_with_bounds(data[data_y], next_x);
          data[data_y] = new_pair;
        }
      }
    }
    */

    sum1 += cur_x * next_y;
    sum2 += cur_y * next_x;
  }

  uint64_t area() const {
    auto area = std::abs(sum1 - sum2) / 2;
    return area + total_len / 2 + 1;
  }

  void print() const {
    cout << "=====================" << endl;
    cout << "size: " << data.size() << endl;
    int64_t row_start = 0;
    int64_t row_end = 0;
    for (const auto &row : data) {
      auto [x_min, x_max] = row;
      if (x_min < row_start) {
        row_start = x_min;
      }
      if (x_max > row_end) {
        row_end = x_max;
      }
    }
    for (const auto &row : data) {
      auto [x_min, x_max] = row;
      for (auto index = row_start; index <= row_end; index++) {
        if (index == x_min || index == x_max) {
          cout << '|';
        } else if (index >= x_min && index <= x_max) {
          cout << '#';
        } else {
          cout << '.';
        }
      }
      cout << "   >> " << (x_max - x_min + 1) << endl;
    }
    cout << "=====================" << endl;
  }
};

} // namespace aoc

int main() {
  aoc::grid grid;
  aoc::grid hex_grid;
  std::string line;
  std::fstream infile(FILENAME);

  // int limit = 10;
  while (std::getline(infile, line)) { // && limit >= 0) {
    grid.process_instruction(line);
    hex_grid.process_hex_instruction(line);
    // grid.print();
    //  limit--;
  }

  // grid.print();

  cout << "Part 1 : " << grid.area() << endl
       << "Part 2 : " << hex_grid.area() << endl;

  return EXIT_SUCCESS;
}
