#include <algorithm>
#include <cassert>
#include <chrono>
#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <fstream>
#include <glib.h>
#include <iostream>
#include <map>
#include <numeric>
#include <omp.h>
#include <ostream>
#include <sstream>
#include <string>
#include <sys/types.h>
#include <tuple>
#include <vector>

#define DEBUG

namespace chrono = std::chrono;

// #define FILENAME "bin/2023/tests/day12-example1.txt"
#define FILENAME "bin/2023/tests/day12-part1.txt"

#define out std::cout
constexpr const char *endl = "\n";

namespace aoc {

enum class State : u_char {
  Working = '.',
  Broken = '#',
  Unknown = '?',
};

void state_append(std::vector<std::vector<State>> *vecs, State state) {
  std::for_each(
      vecs->begin(), vecs->end(),
      [state](std::vector<State> &vec) mutable { vec.push_back(state); });
}

void state_print(uint64_t num, const std::vector<State> &vec) {
  out << num << ": ";
  for (auto state : vec) {
    out << " " << static_cast<u_char>(state);
  }
}

void state_assert(uint64_t num, std::vector<std::vector<State>> &tests,
                  const std::string &ctx) {
  size_t size = 0;
  for (auto &test : tests) {
    if (test.size() > size) {
      size = test.size();
    }
    if (test.size() != size) {
      for (const auto &test : tests) {
        state_print(num, test);
      }
      out << ctx << endl;
      assert(test.size() == size);
    }
  }
}

template <typename T>
std::string join(const std::vector<T> &vec, const std::string &delim) {
  std::vector<std::string> nxt(vec.size());
  std::transform(vec.begin(), vec.end(), nxt.begin(),
                 [&](const auto &str) mutable { return std::to_string(str); });
  std::string ret =
      std::accumulate(nxt.begin(), nxt.end(), std::string(""),
                      [&](const auto &acc, const auto &str) mutable {
                        return acc + str + delim;
                      });
  return ret;
}

class Line {
  uint64_t num;
  std::vector<State> springs;
  std::vector<uint64_t> broken_hints;

  void parse_line(const std::string &line) {
    GStrv results = g_strsplit(line.c_str(), " ", 0);
    if (g_strv_length(results) != 2) {
      out << "ERRROR: expected to find a line with 2 parts separated by a "
             "space\n";
      return;
    }
    std::string spring_map(results[0]);
    for (auto nxt_c : spring_map) {
      auto state = static_cast<State>(nxt_c);
      springs.push_back(state);
    }

    GStrv broken_map = g_strsplit(results[1], ",", 0);
    std::string broken_str(results[1]);
    for (size_t index = 0; index < g_strv_length(broken_map); index++) {
      std::stringstream str;
      str << broken_map[index];
      int64_t broken = 0;
      str >> broken;
      broken_hints.push_back(broken);
    }
  }

  enum class ValidationPhases {
    None,
    MustBeBroken,
    MustBeWorking,
  };

  uint64_t needed_positions(int64_t index) {
    auto working_needed = broken_hints.size() - index - 1;
    auto broken_needed =
        std::reduce(broken_hints.begin() + index, broken_hints.end());
    return broken_needed + working_needed;
  }

  std::map<std::tuple<size_t, size_t>, uint64_t> memoize;
  uint64_t count_combination_recursive(std::vector<State> test, size_t start,
                                       size_t broken_index,
                                       uint64_t broken_current,
                                       ValidationPhases phase) {
    std::tuple<size_t, size_t> m_key(start, broken_index);
    if (memoize.find(m_key) != memoize.end()) {
      return memoize[m_key];
    }

    uint64_t result = 0;

    // auto phase = ValidationPhases::None;

    for (auto index = start; index < springs.size(); index++) {
      auto spring = springs[index];
      switch (spring) {
      case State::Working:
        test.push_back(spring);

        switch (phase) {
        case ValidationPhases::None:
          continue;
        case ValidationPhases::MustBeWorking:
          phase = ValidationPhases::None;
          continue;
        case ValidationPhases::MustBeBroken:
          return result;
        }
      case State::Broken:
        test.push_back(spring);
        switch (phase) {
        case ValidationPhases::None:
          phase = ValidationPhases::MustBeBroken;
          break;
        case ValidationPhases::MustBeWorking:
          return result;
        case ValidationPhases::MustBeBroken:
          break;
        }
        break;
      case State::Unknown:
        // let's make a copy now for use later
        std::vector<State> copy(test);

        // copy will go down the path of State::Working
        copy.push_back(State::Working);

        // let's continue using State::Broken
        test.push_back(State::Broken);

        auto new_phase = phase;
        switch (phase) {
        case ValidationPhases::MustBeBroken:
          break;
        case ValidationPhases::MustBeWorking:
          new_phase = ValidationPhases::None;
          [[fallthrough]];
        case ValidationPhases::None:
          uint64_t positions_left = springs.size() - index;
          if (needed_positions(static_cast<int64_t>(broken_index)) <
              positions_left) {
            result += count_combination_recursive(copy, index + 1, broken_index,
                                                  broken_current, new_phase);
          }
          if (phase != ValidationPhases::None) {
            return result;
          }
          break;
        }
        phase = ValidationPhases::MustBeBroken;
        break;
      }

      broken_current--;
      if (broken_current == 0) {
        broken_index++;
        if (broken_index >= broken_hints.size()) {
          if (std::find(springs.begin() + index + 1, springs.end(),
                        State::Broken) != springs.end()) {
            return result;
          }
          break;
        }
        broken_current = broken_hints[broken_index];
        phase = ValidationPhases::MustBeWorking;
      }
    }
    if (broken_current > 0 || broken_index < broken_hints.size()) {
      return result;
    }

    result++;
    memoize[m_key] = result;

    // out << "RETURNED " << result;
    return result;
  }

public:
  explicit Line(const uint64_t num, const std::string &line) : num(num) {
    parse_line(line);
  }

  void unfold(uint64_t repeats) {
    std::vector<uint64_t> b_copy(broken_hints);
    std::vector<State> s_copy(springs);
    for (uint64_t index = 1; index < repeats; index++) {
      broken_hints.insert(broken_hints.end(), b_copy.begin(), b_copy.end());
      springs.push_back(State::Unknown);
      springs.insert(springs.end(), s_copy.begin(), s_copy.end());
    }
  }

  void print_input(uint64_t combos) {
    out << combos << " ";
    state_print(num, springs);
    for (auto broken : broken_hints) {
      out << ' ' << broken;
    }
    out << endl;
  }

  uint64_t count_combinations() {
    // worked for part1, using recursive for part2 to reduce the
    // memory burden
    // return count_combination_serial()

    auto broken_index = 0;
    auto broken_current = broken_hints[broken_index];
    std::vector<State> test(springs.size());
    return count_combination_recursive(test, 0, broken_index, broken_current,
                                       ValidationPhases::None);
  }
};

} // namespace aoc

constexpr uint64_t REPEATS = 4;

int main() {

  uint64_t result1 = 0;
  uint64_t result2 = 0;
  uint64_t number = 0;

  auto num_devices = omp_get_num_devices();
  out << "Found " << num_devices << " devices to run on!\n";

#pragma omp parallel
  {
#pragma omp master
    {
      std::string line;
      std::fstream infile(FILENAME);
      while (std::getline(infile, line))
#pragma omp task
      {
#ifdef DEBUG
        const auto start = std::chrono::system_clock::now();
#endif
        uint64_t num = 0;
#pragma omp critical
        {
          number++;
          num = number;
        }
        assert(num != 0);
        // out << number << ")) " << line << endl;

        aoc::Line part1(num, line);
        auto part1_result = part1.count_combinations();
        // part1.print_input(part1_result);
#pragma omp critical
        { result1 += part1_result; }

        aoc::Line part2(num, line);
        part2.unfold(REPEATS);
        auto part2_result = part2.count_combinations();
        // part2.print_input (part2_result);
#pragma omp critical
        {
          result2 += part2_result;
#ifdef DEBUG
          const auto end = std::chrono::system_clock::now();
          const auto time = end - start;
          out << num << ": "
              << " : took : " << chrono::duration_cast<chrono::seconds>(time);
          part2.print_input(0);
#endif
        }
      }
#pragma omp taskwait
    }
  }

  out << "Part 1 : 7622=" << result1 << endl
      << "Part 2 : 63958600=" << result2 << endl;

  return EXIT_SUCCESS;
}
