/*
 * Day 22: WIP could not finish part 2
 */

#include <glib.h>

#include <boost/multi_array.hpp>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <memory>
#include <optional>
#include <queue>
#include <string>
#include <tuple>
#include <unordered_set>
#include <vector>

#define FILENAME "bin/2023/tests/day22-example1.txt"
// #define FILENAME "bin/2023/tests/day22-part1.txt"

namespace aoc {

using coord = std::tuple<int64_t, int64_t, int64_t>;

class obj {
  uint64_t id_;
  std::vector<coord> coords_;
  std::vector<std::shared_ptr<obj>> children_;

 public:
  explicit obj(uint64_t id) : id_(id) {}
  void add_coord(coord new_coord) { coords_.push_back(new_coord); }
  void add_child(const std::shared_ptr<obj> &child) {
    children_.push_back(child);
  }
  [[nodiscard]] auto children() const -> std::vector<std::shared_ptr<obj>> {
    return children_;
  }
  [[nodiscard]] auto coords() const -> std::vector<coord> { return coords_; }
  [[nodiscard]] auto id() const -> uint64_t { return id_; }
};

[[nodiscard]] auto coord_from(const char *coord_str) -> coord {
  GStrv int_str = g_strsplit(coord_str, ",", 0);
  auto x = std::stol(int_str[0]);
  auto y = std::stol(int_str[1]);
  auto z = std::stol(int_str[2]);
  return {x, y, z};
}

using grid = boost::multi_array<std::shared_ptr<aoc::obj>, 3>;
void grid_print(grid grid) {
  std::cout << "=====================================================\n";
  for (auto z = grid.size() - 1; z > 0; z--) {
    auto plane = grid[z];
    std::cout << z << ": ";
    for (auto y = 0; y < plane.size(); y++) {
      auto column = plane[y];
      std::cout << y << "- ";
      for (auto x = 0; x < column.size(); x++) {
        auto row = column[x];
        if (!row) {
          std::cout << "...";
        } else {
          std::cout << '[' << row->id() << ']';
        }
      }
      std::cout << ' ';
    }
    std::cout << '\n';
  }
  std::cout << "=====================================================\n";
}

auto grid_lower_obj(const grid &grid, const std::shared_ptr<obj> &obj)
    -> std::optional<aoc::obj> {
  size_t coords = 0;
  std::optional<int64_t> z_cur_opt;
  for (const auto &coord : obj->coords()) {
    coords++;
    auto [x, y, z] = coord;
    if (!z_cur_opt.has_value() || z < z_cur_opt.value()) {
      z_cur_opt = z;
    }
  }

  if (!z_cur_opt.has_value()) {
    return std::nullopt;
  }

  auto z_cur = z_cur_opt.value();

  int64_t z_lower = 0;
  for (auto z = z_cur - 1; z > 0; z--) {
    auto can_lower = true;
    for (const auto &coord : obj->coords()) {
      auto [x, y, unused_] = coord;
      auto nxt_coord = grid[z][y][x];
      if (!nxt_coord) {
        continue;
      }

      // this means we're attempting to lower where we already
      // exist. this happens on vertical bricks
      if (nxt_coord->id() == obj->id()) {
        continue;
      }

      can_lower = false;
    }

    if (can_lower) {
      z_lower++;
    } else {
      break;
    }
  }

  aoc::obj new_obj(obj->id());
  std::vector<coord> new_coords(coords);
  for (const auto &coord : obj->coords()) {
    auto [x, y, z_this] = coord;
    new_obj.add_coord({x, y, z_this - z_lower});
  }
  return new_obj;
}

}  // namespace aoc

auto main() -> int {
  std::vector<std::shared_ptr<aoc::obj>> objects;

  int64_t x_max = 0;
  int64_t y_max = 0;
  int64_t z_max = 0;

  uint64_t obj_id = 0;

  std::string line;
  std::fstream infile(FILENAME);
  while (std::getline(infile, line)) {
    GStrv coords = g_strsplit(line.c_str(), "~", 0);
    if (g_strv_length(coords) != 2) {
      std::cout << "ERROR: expected a string separated by a ~ but got : "
                << line << "\n";
      return EXIT_FAILURE;
    }

    auto obj_p = std::make_shared<aoc::obj>(obj_id);

    auto left = aoc::coord_from(coords[0]);
    auto right = aoc::coord_from(coords[1]);
    auto [left_x, left_y, left_z] = left;
    auto [right_x, right_y, right_z] = right;

    for (auto z = left_z; z <= right_z; z++) {
      if (z > z_max) {
        z_max = z;
      }

      for (auto y = left_y; y <= right_y; y++) {
        if (y > y_max) {
          y_max = y;
        }

        for (auto x = left_x; x <= right_x; x++) {
          if (x > x_max) {
            x_max = x;
          }

          obj_p->add_coord({x, y, z});
        }
      }
    }
    objects.push_back(std::move(obj_p));

    obj_id++;
  }

  aoc::grid grid(boost::extents[z_max + 1][y_max + 1][x_max + 1]);

  for (auto &obj : objects) {
    for (const auto &coord : obj->coords()) {
      auto [x, y, z] = coord;
      grid[z][y][x] = obj;
    }
  }

  grid_print(grid);

  std::unordered_set<uint64_t> lowered;
  // aoc::grid grid_lowered(boost::extents[z_max + 1][y_max + 1][x_max + 1]);
  aoc::grid grid_lowered(grid);

  std::vector<std::shared_ptr<aoc::obj>> new_objs(objects.size());
  for (auto &&plane : grid) {
    for (auto &&column : plane) {
      for (const auto &brick_ptr : column) {
        if (!brick_ptr) {
          continue;
        }

        auto [unused_, inserted] = lowered.insert(brick_ptr->id());
        if (!inserted) {
          continue;
        }

        auto new_brick = grid_lower_obj(grid_lowered, brick_ptr);
        if (!new_brick.has_value()) {
          std::cout << "ERROR: failed to create a new brick\n";
          return EXIT_FAILURE;
        }

        auto new_brick_ptr = std::make_shared<aoc::obj>(new_brick.value());
        new_objs.push_back(new_brick_ptr);
        for (const auto &coord : brick_ptr->coords()) {
          auto [x, y, z] = coord;
          grid_lowered[z][y][x] = nullptr;
        }
        for (const auto &coord : new_brick_ptr->coords()) {
          auto [x, y, z] = coord;
          grid_lowered[z][y][x] = new_brick_ptr;
        }
      }
    }
  }

  grid_print(grid_lowered);

  std::unordered_map<uint64_t, std::unordered_set<uint64_t>> supported_by(
      grid_lowered.size());
  for (auto &&plane : grid_lowered) {
    for (auto &&column : plane) {
      for (const auto &brick_ptr : column) {
        if (brick_ptr == nullptr) {
          continue;
        }
        if (!supported_by.contains(brick_ptr->id())) {
          std::unordered_set<uint64_t> supports;
          supported_by[brick_ptr->id()] = supports;
        }
        for (const auto &coord : brick_ptr->coords()) {
          auto [x, y, z] = coord;
          auto support = grid_lowered[z - 1][y][x];
          // std::cout << brick_ptr->id() << " " << x << ", " << y << ", "
          //           << (z - 1) << '\n';
          if (!support) {
            continue;
          }
          if (support->id() == brick_ptr->id()) {
            continue;
          }
          auto [unused_, inserted] =
              supported_by[brick_ptr->id()].insert(support->id());
          if (inserted) {
            support->add_child(brick_ptr);
          }
          // std::cout << support->id() << " supports " << brick_ptr->id() <<
          // '\n';
        }
      }
    }
  }

  uint64_t result1 = 0;
  for (auto [id, unused1_] : supported_by) {
    std::vector<uint64_t> dedicated_support_for;
    auto dedicated_support = 0;

    for (auto [id2, supports] : supported_by) {
      if (supports.find(id) != supports.end()) {
        if (supports.size() == 1) {
          dedicated_support += 1;
          dedicated_support_for.push_back(id2);
        }
      }
    }

    // part 1
    if (dedicated_support == 0) {
      // std::cout << "Able to disintegrate " << id << '\n';
      result1++;
    }
  }

  uint64_t result2 = 0;
  for (const auto &brick_ptr : new_objs) {
    if (brick_ptr == nullptr) {
      // std::cout << "got null\n";
      continue;
    }
    std::queue<std::shared_ptr<aoc::obj>>
        queue;  //, std::vector<aoc::obj>> queue;
    auto tree = brick_ptr->children();
    tree.push_back(brick_ptr);
    for (const auto &child : brick_ptr->children()) {
      queue.push(child);
    }

    uint64_t tree_size = 0;
    while (!queue.empty()) {
      auto child = queue.front();
      queue.pop();

      auto supports = supported_by[child->id()];
      for (auto support : supports) {
        bool found = false;
        for (const auto &leaf : tree) {
          if (leaf->id() == support) {
            found = true;
            break;
          }
        }
        if (!found) {
          break;
        }
        std::cout << brick_ptr->id() << " only parent of " << child->id()
                  << "\n";
        tree.push_back(child);
        for (const auto &next_child : child->children()) {
          queue.push(next_child);
        }
        tree_size++;
      }
    }
    std::cout << brick_ptr->id() << " tree dropped " << tree_size << '\n';
    result2 += tree_size;
    break;
  }

  std::cout << "Part 1 : " << result1 << "\nPart 2 : " << result2 << '\n';

  return EXIT_SUCCESS;
}
