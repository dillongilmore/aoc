
#include <cassert>
#include <fstream>
#include <iostream>
#include <map>
#include <optional>
#include <string>
#include <sys/types.h>
#include <tuple>
#include <unordered_set>
#include <vector>

// #define FILENAME "bin/2023/tests/day16-example1.txt"
#define FILENAME "bin/2023/tests/day16-part1.txt"
// #define DEBUG

using std::cout;
constexpr const char endl = '\n';

namespace aoc {

// note: so we can safely loop over, make sure `endd` is the last
// element in the enum
enum class direction { east, south, west, north };
constexpr auto endd = static_cast<int>(direction::north);

char to_char(aoc::direction dir) {
  switch (dir) {
  case direction::east:
    return '>';
  case direction::south:
    return 'v';
  case direction::west:
    return '<';
  case direction::north:
    return '^';
  }
}

enum class part : u_char {
  none = '.',
  fmirror = '/',
  bmirror = '\\',
  hsplit = '-',
  vsplit = '|',
};

using coord = std::tuple<uint64_t, uint64_t>;

class tile {
  bool energized = false;
  aoc::part object = aoc::part::none;

public:
  tile() = default;

  void set_object(auto obj) { object = static_cast<aoc::part>(obj); }

  void energize() { energized = true; }

  [[nodiscard]] aoc::part get_object() const { return object; }
  [[nodiscard]] bool is_energized() const { return energized; }
};

class beam {
  uint64_t x{};
  uint64_t y{};
  direction dir = direction::east;
  uint64_t x_max;
  uint64_t y_max;

public:
  beam(auto x_max, auto y_max) : x_max(x_max), y_max(y_max) {}
  beam(auto xxx, auto yyy, auto dir, auto x_max, auto y_max)
      : x(xxx), y(yyy), dir(dir), x_max(x_max), y_max(y_max) {}

  [[nodiscard]] aoc::beam clone(direction new_dir) const {
    return {x, y, new_dir, x_max, y_max};
  }

  [[nodiscard]] aoc::coord position() const { return {x, y}; }

  [[nodiscard]] direction direction() const { return dir; }

  [[nodiscard]] aoc::coord next() {
    switch (direction()) {
    case direction::east:
      if (x + 1 != x_max) {
        x += 1;
      }
      break;
    case direction::south:
      if (y + 1 != y_max) {
        y += 1;
      }
      break;
    case direction::west:
      if (x != 0) {
        x -= 1;
      }
      break;
    case direction::north:
      if (y != 0) {
        y -= 1;
      }
      break;
    }
    return position();
  }

  std::tuple<bool, std::optional<aoc::beam>> move(aoc::tile tile) {
    bool done = false;
    std::optional<aoc::direction> next_dir;
    switch (tile.get_object()) {
    case part::hsplit:
      if (dir == direction::north || dir == direction::south) {
        dir = direction::east;
        next_dir = direction::west;
      }
      break;
    case part::vsplit:
      if (dir == direction::east || dir == direction::west) {
        dir = direction::north;
        next_dir = direction::south;
      }
      break;
    case part::fmirror:
      switch (direction()) {
      case direction::north:
        dir = direction::east;
        break;
      case direction::east:
        dir = direction::north;
        break;
      case direction::west:
        dir = direction::south;
        break;
      case direction::south:
        dir = direction::west;
        break;
      }
      break;
    case part::bmirror:
      switch (direction()) {
      case direction::north:
        dir = direction::west;
        break;
      case direction::east:
        dir = direction::south;
        break;
      case direction::west:
        dir = direction::north;
        break;
      case direction::south:
        dir = direction::east;
        break;
      }
      break;
    case part::none:
      break;
    }

    std::optional<aoc::beam> new_beam;
    // if the first thing our new beam does is move off the map
    // then there's no point in returning it
    if (next_dir.has_value()) {
      new_beam = clone(next_dir.value());
      auto [xxx, yyy] = new_beam->position();
#ifdef DEBUG
      cout << "nextpos " << xxx << ":" << yyy << " "
           << to_char(new_beam->direction()) << endl;
#endif
    }

    // if our next move is off the map then let's flag to
    // the caller that we're done
    auto current = position();
#ifdef DEBUG
    cout << to_char(dir) << " ) " << x << " : " << y << endl;
#endif
    if (next() == current) {
      done = true;
    }

#ifdef DEBUG
    cout << "newpos " << x << ":" << y << endl;
#endif
    return {done, new_beam};
  }
};

class grid {
  std::vector<std::vector<aoc::tile>> tile_grid;
  uint64_t x_max = 0;
  uint64_t y_max = 0;

public:
  uint64_t y_size() const { return y_max; }
  uint64_t x_size() const { return x_max; }

  void add_line(std::string &line) {
    y_max += 1;
    size_t len = line.length();
    if (x_max == 0) {
      x_max = len;
    }
    assert(x_max == len);

    std::vector<aoc::tile> tiles(len);

    for (auto index = 0; index < len; index++) {
      tiles[index].set_object(line[index]);
    }
    tile_grid.push_back(tiles);
  }

  void print() const { print(false); }
  void print(bool energized) const {
    cout << "==================================" << endl;
    for (const auto &tiles : tile_grid) {
      for (const auto &tile : tiles) {
        auto obj = static_cast<char>(tile.get_object());
        if (energized && tile.is_energized()) {
          obj = '#';
        }
        cout << obj;
      }
      cout << endl;
    }
    cout << "==================================" << endl;
  }

  void
  trace_beam(aoc::beam beam,
             std::map<aoc::coord, std::unordered_set<aoc::direction>> &visits) {

    for (;;) {
      auto xxyy = beam.position();

      auto [xxx, yyy] = xxyy;
      auto &tile = tile_grid[yyy][xxx];
      tile.energize();

#ifdef DEBUG
      cout << "==================================" << endl;
      print();
      cout << "----------------------------------" << endl;
      print(true);
      cout << "==================================" << endl;
#endif

      auto [done, new_beam] = beam.move(tile);

      if (new_beam.has_value()) {
        trace_beam(new_beam.value(), visits);
      }

      if (done) {
        return;
      }

      // let's start by checking if we've visited this tile before
      if (tile.get_object() != part::none) {
        if (visits.contains(xxyy)) {
          // we've been here before, but let's see if last time we ended
          // up going in the same direction as last time, if we are then
          // we're going in a loop
          auto [unused_, inserted] = visits[xxyy].insert(beam.direction());
          if (!inserted) {
            // this means we're going in a loop, we can exit, all tiles
            // have been energized
            return;
          }
        } else {
          // let's put an entry in the cache that reads as:
          // tile xxyy, sent us in the direction of `direction`. Next time
          // we're back here if we end up going in the same direction then
          // we're in a loop
          std::unordered_set<aoc::direction> cache;
          cache.insert(beam.direction());
          visits[xxyy] = cache;
        }
      }
    }
  }

  uint64_t energized_tiles(auto xxx, auto yyy, auto dir) {
    aoc::beam beam(xxx, yyy, dir, x_max, y_max);

    // any mirror can redirect in any direction so we want to cache on
    // places we've visited by the direction we were redirected to go
    std::map<aoc::coord, std::unordered_set<aoc::direction>> visits;
    trace_beam(beam, visits);

    uint64_t ret = 0;
    for (const auto &tiles : tile_grid) {
      for (auto tile : tiles) {
        if (tile.is_energized()) {
          ret += 1;
        }
      }
    }
    return ret;
  }
};

} // namespace aoc

int main() {
  // mutable grid so we can build it line by line
  aoc::grid grid;
  std::string line;
  std::fstream infile(FILENAME);
  while (std::getline(infile, line)) {
    grid.add_line(line);
  }

  // convert the original grid to a const grid so we always have an original
  // to work from
  const aoc::grid og_grid = grid;

  // LATER: Easy OpenMP usage here (atomic for result2)
  // need to do 4 loops, once for each direction
  uint64_t result2 = 0;
  for (auto dir_i = 0; dir_i != aoc::endd; dir_i++) {
    uint64_t result{};

    // 1 loop for each direction
    auto dir = static_cast<aoc::direction>(dir_i);
    uint64_t y_index = 0;
    uint64_t x_index = 0;
    switch (dir) {
    case aoc::direction::north:
      // if we are moving from the north, we are moving _up_ from the
      // _bottom_ of the grid
      y_index = og_grid.y_size() - 1;
      [[fallthrough]];
    case aoc::direction::south:
      // if we are moving from the south, we are moving _down_ from the
      // _top_ of the grid

      // for both north and south, we loop through every X but have static Y
      // LATER: Easy OpenMP usage (atomic for result2)
      for (auto index = 0; index < og_grid.x_size(); index++) {
        // copy the grid
        aoc::grid wk_grid = og_grid;
        // energize the grid
        result = wk_grid.energized_tiles(index, y_index, dir);
        if (result > result2) {
          result2 = result;
        }
      }
      break;
    case aoc::direction::west:
      // if we are moving from the west, we are moving _left_ from the
      // _right_ of the grid
      x_index = og_grid.x_size() - 1;
      [[fallthrough]];
    case aoc::direction::east:
      // if we are moving from the east, we are moving _right_ from the
      // _left_ of the grid

      // for both east and west we loop through every Y but have static X
      // LATER: Easy OpenMP usage (atomic for result2)
      for (auto index = 0; index < og_grid.y_size(); index++) {
        // copy the grid
        aoc::grid wk_grid = og_grid;
        // energize the grid
        result = wk_grid.energized_tiles(x_index, index, dir);
        if (result > result2) {
          result2 = result;
        }
      }
      break;
    }
  }

  // part 1, copy the original grid and do 0,0 ->
  aoc::grid wk_grid = og_grid;
  auto result1 = wk_grid.energized_tiles(0, 0, aoc::direction::east);

  cout << "Part 1 : " << result1 << endl << "Part 2 : " << result2 << endl;
}
