
#include <fstream>
#include <glib.h>
#include <iostream>
#include <optional>
#include <string>
#include <vector>

constexpr int DEBUG_BOX = 251;
// constexpr const char *FILENAME = "bin/2023/tests/day15-example1.txt";
constexpr const char *FILENAME = "bin/2023/tests/day15-part1.txt";

using std::cout;

constexpr const char endl = '\n';

constexpr const int i17 = 17;
constexpr const int i256 = 256;
int hash(const char *sequence, const size_t label_len) {
  signed int result = 0;
  // for (auto sindex = 0; sindex < strlen(sequence); sindex++) {
  for (auto sindex = 0; sindex < label_len; sindex++) {
    signed int ascii = (signed int)sequence[sindex];
    result += ascii;
    result *= i17;
    result = result % i256;
  }
  return result;
}

class Seq {
  std::string raw_;
  std::string label_;
  char operation_ = '\0';
  signed int length_ = 0;

public:
  Seq() = default;

  Seq(std::string &raw, char operation, std::string &label, signed int len)
      : raw_(raw), label_(label), operation_(operation), length_(len) {}

  Seq(std::string &raw, char operation, std::string &label)
      : Seq(raw, operation, label, 0) {}

  static Seq from_cstr(const char *raw) {
    std::string str(raw);
    std::string label;

    char operation = '\0';
    signed int length = 0;
    for (auto part : str) {
      if (part == '-' || part == '=') {
        operation = part;
        continue;
      }
      if (operation != '\0') {
        length = part - '0';
      } else {
        label.push_back(part);
      }
    }

    return {str, operation, label, length};
  }

  std::string to_string() const {
    return "b" + std::to_string(box_index()) + " l" + label_;
  }

  signed int raw_hash() const { return hash(raw_.c_str(), raw_.length()); }

  size_t box_index() const { return hash(label_.c_str(), label_.length()); }

  std::string label() const { return label_; }
  char op() const { return operation_; }
  signed int length() const { return length_; }
};

class Box {
  std::vector<std::optional<Seq>> lenses;

  void add_lens(const Seq &lens) {
    if (!lenses.empty()) {
      // cout << lenses.size();
      for (auto index = 0; index < lenses.size(); index++) {
        auto &cur_lens = lenses[index];
        if (!cur_lens.has_value()) {
          continue;
        }
        if (cur_lens.value().label() == lens.label()) {
          if (lens.box_index() == DEBUG_BOX) {
            cout << "replaced " << lens.to_string() << endl;
          }
          lenses[index] = lens;
          return;
        }
      }
    }
    if (lens.box_index() == DEBUG_BOX) {
      cout << "added " << lens.label() << endl;
    }

    lenses.push_back(lens);
  }

  void remove_lens(const Seq &lens) {
    for (auto index = 0; index < lenses.size(); index++) {
      auto cur_lens = lenses[index];
      if (cur_lens.has_value() && cur_lens.value().label() == lens.label()) {
        // remove lens and begin shifting elements
        if (lens.box_index() == DEBUG_BOX) {
          cout << "removed " << lens.to_string() << endl;
        }
        lenses[index] = std::nullopt;
      }
    }
  }

public:
  Box() : lenses(0) {}

  bool do_sequence(const Seq &seq) {
    switch (seq.op()) {
    case '=':
      add_lens(seq);
      return true;
    case '-':
      remove_lens(seq);
      return true;
    default:
      return false;
    }
  }

  uint64_t focul_power(signed int box) {
    uint64_t ret = 0;
    auto counter = 1;
    for (auto index = 0; index < lenses.size(); index++) {
      auto lens_opt = lenses[index];
      if (!lens_opt.has_value()) {
        continue;
      }
      auto lens = lens_opt.value();
      signed int lens_power = box * counter * lens.length();
      // cout << "i" << counter << " b" << box << " l(" << lens.length()
      //      << "):" << lens.label() << " " << lens_power << endl;
      ret += lens_power;
      if (box - 1 == DEBUG_BOX) {
        cout << "R: " << ret << endl;
      }
      counter += 1;
    }
    return ret;
  }
};

constexpr size_t BOXES = 256;
int main() {
  signed int result1 = 0;

  std::vector<Box> boxes(BOXES);

  std::string line;
  std::fstream infile(FILENAME);
  while (std::getline(infile, line)) {
    GStrv sequences = g_strsplit(line.c_str(), ",", 0);
    auto total_sequences = g_strv_length(sequences);

    for (auto index = 0; index < total_sequences; index++) {
      const char *sequence = sequences[index];
      const Seq seq = Seq::from_cstr(sequence);

      auto hsh1 = seq.raw_hash();
      auto box = seq.box_index();
      boxes[box].do_sequence(seq);

      // cout << sequence << " : " << box << endl;
      result1 += hsh1;
    }
  }

  cout << endl;

  uint64_t result2 = 0;
  for (auto index = 0; index < boxes.size(); index++) {
    auto box = boxes[index];
    auto power = box.focul_power(index + 1);
    if (index == DEBUG_BOX) {
      cout << "b" << index << " " << power << endl;
    }
    result2 += power;
  }

  cout << endl
       << "Part 1 : " << result1 << endl
       << "Part 2 : " << result2 << endl;
}
