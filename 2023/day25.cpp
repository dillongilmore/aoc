
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/detail/adjacency_list.hpp>
#include <boost/graph/graph_selectors.hpp>
#include <boost/graph/named_function_params.hpp>
#include <boost/graph/properties.hpp>
#include <boost/graph/property_maps/constant_property_map.hpp>
#include <boost/graph/stoer_wagner_min_cut.hpp>
#include <boost/pending/property.hpp>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <tuple>
#include <utility>

#define FILENAME "bin/2023/tests/day25-part1.txt"

namespace aoc {

struct name_tag {
  using kind = boost::vertex_property_tag;
};

using name_prop = boost::property<name_tag, std::string>;
using weight_prop = boost::property<boost::edge_weight_t, int32_t>;

using edge = std::pair<const std::basic_string<char>, int64_t>;
using edge_map = std::map<std::string, int64_t>;
using edge_list =
    boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS,
                          name_prop, weight_prop>;

class edge_builder {
  edge_map table;
  edge_list list;

 public:
  void add_line(std::string &line) {
    auto len = static_cast<int64_t>(table.size());

    std::vector<std::string> parts;
    boost::split(parts, line, boost::is_any_of(":"));

    auto left = parts[0];
    boost::trim(left);
    if (table.find(left) == table.end()) {
      table[left] = len;
      len++;
    }

    std::vector<std::string> connected_to;
    boost::split(connected_to, parts[1], boost::is_any_of(" "));

    for (auto &right : connected_to) {
      boost::trim(right);

      if (right.empty()) {
        continue;
      }

      if (table.find(right) == table.end()) {
        table[right] = len;
        len++;
      }

      boost::add_edge(table[left], table[right], weight_prop(1), list);
      // boost::add_edge(table[right], table[left], weight_prop(1), list);
      // boost::put(boost::edge_weight, list, table[right], 1);
      //  boost::property_map<edge_list, name_tag>::type name_map =
      //      boost::get(name_tag(), list);
      //  name_map[id] = name;
    }
  }

  [[nodiscard]] auto cut() const -> std::tuple<int64_t, int64_t, int64_t> {
    auto len = num_vertices(list);
    BOOST_AUTO(parities, boost::make_one_bit_color_map(
                             len, get(boost::vertex_index, list)));
    auto ret =
        boost::stoer_wagner_min_cut(list, boost::get(boost::edge_weight, list),
                                    boost::parity_map(parities));

    auto left = 0;
    auto right = 0;
    for (auto i = 0; i < len; i++) {
      if (boost::get(parities, i) != 0U) {
        left++;
      } else {
        right++;
      }
    }
    return {ret, left, right};
  }

  void print() const {
    std::cout << "Graph has " << boost::num_vertices(list) << " nodes";
    std::cout << " and " << boost::num_edges(list) << " edges\n";
    std::cout << "Map has " << table.size() << " entries\n";

    auto [cuts, left, right] = cut();
    std::cout << "Cut " << cuts << "\n";
    std::cout << "Left " << left << "\n";
    std::cout << "Right " << right << "\n";
    std::cout << "Part 1 : " << (left * right) << "\n";
  }
};

}  // namespace aoc

// constexpr int32_t CUTS = 3;
auto main() -> int {
  aoc::edge_builder builder;

  std::string line;
  std::fstream infile(FILENAME);
  while (std::getline(infile, line)) {
    builder.add_line(line);
  }

  builder.print();
}
