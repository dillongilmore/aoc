/*
 * Day 9 - ran into serious issues so refactoring onto `glib`
 */
#include <glib.h>
#include <glib/gstdio.h>
#include <stdbool.h>

#define FILENAME "bin/2023/tests/day9-part1.txt"
// #define FILENAME "bin/2023/tests/day9-example1.txt"

gint64
predict_next_sum (gchar **nums, guint nsz)
{
  gint64 result = 0;
  if (!nsz)
    {
      return 0;
    }

  // initial array of integers for this line
  g_autoptr (GArray) ints
      = g_array_sized_new (false, true, sizeof (gint64), nsz);
  for (guint j = 0; j < nsz; j++)
    {
      gchar *num = nums[j];
      gint64 result = g_ascii_strtoll (num, NULL, 10);
      g_array_insert_val (ints, j, result);
    }

  // keep subtracting until we get all 0's
  for (gint j = nsz; j >= 0; j--)
    {
      gboolean zeroes = true;
      gint64 tail;
      for (guint k = 1; k < j; k++)
        {
          gint64 a = g_array_index (ints, gint64, k - 1);
          gint64 b = g_array_index (ints, gint64, k);
          tail = b;
          gint64 result = b - a;
          g_array_remove_index (ints, k - 1);
          g_array_insert_val (ints, k - 1, result);
          if (result != 0)
            {
              zeroes = false;
            }
        }
      // because this is just addition, I think we can just keep adding?
      result += tail;
      if (zeroes)
        {
          break;
        }
    }

  return result;
}

int
main ()
{
  gchar *contents = NULL;
  GError *err = NULL;
  g_file_get_contents (FILENAME, &contents, NULL, &err);
  if (err != NULL)
    {
      g_printerr ("could not open %s : but got %s\n", FILENAME, err->message);
      g_assert (contents == NULL);
    }

  gchar **lines = g_strsplit (contents, "\n", 0);
  guint lsz = g_strv_length (lines);
  gint64 result1 = 0, result2 = 0;

  for (guint i = 0; i < lsz; i++)
    {
      gchar *line = lines[i];
      gchar **nums = g_strsplit (line, " ", 0);
      g_autoptr (GStrvBuilder) builder = g_strv_builder_new ();
      for (int i = g_strv_length (nums); i > 0; i--)
        {
          g_strv_builder_add (builder, nums[i - 1]);
        }

      gchar **rev = g_strv_builder_end (builder);
      guint nsz = g_strv_length (nums);

      result1 += predict_next_sum (nums, nsz);
      result2 += predict_next_sum (rev, nsz);

      g_strfreev (nums);
    }
  g_print ("Part 1 : %ld\nPart 2 : %ld\n", result1, result2);

  g_strfreev (lines);
}
