
//
// Created by dgilmore on 12/3/23.
//
// TODO: this is a really really great candidate to run on the GPU
//

#include <errno.h>
#include <limits.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

// #define FILENAME "bin/2023/tests/day5-example1.txt"
#define FILENAME "bin/2023/tests/day5-part1.txt"

#define BUFFER_DEFAULT 100

typedef struct stat stat_t;

const size_t PTR_SZ = sizeof (void *);

size_t
next_realloc (size_t i)
{
  double index = (double)i;
  return (size_t)(ceil ((double)(index / BUFFER_DEFAULT)) * BUFFER_DEFAULT);
}

typedef struct ulong_array
{
  size_t len;
  ulong start;
} ulong_array_t;

ulong_array_t *
ulong_array_new (ulong start, size_t range)
{
  ulong_array_t *array = (ulong_array_t *)malloc (sizeof (ulong_array_t));
  array->len = range;
  array->start = start;
  return array;
}

void
ulong_array_free (ulong_array_t *array)
{
  // OLD: from brute force method
  // free (array->values);
  free (array);
}

bool
ulong_array_in (ulong_array_t *haystack, ulong needle, size_t *pos)
{
  ulong clamp = haystack->start + haystack->len;
  if (needle >= haystack->start && needle <= clamp)
    {
      if (pos != NULL)
        {
          *pos = needle - haystack->start;
        }
      return true;
    }
  return false;
}

typedef struct map_entry
{
  ulong_array_t *src;
  ulong_array_t *dst;
} map_entry_t;

map_entry_t *
map_entry_new (ulong dst, ulong src, ulong range)
{
  map_entry_t *entry = (map_entry_t *)malloc (sizeof (map_entry_t));
  entry->dst = ulong_array_new (dst, range);
  entry->src = ulong_array_new (src, range);
  return entry;
}

void
map_entry_free (map_entry_t *entry)
{
  ulong_array_free (entry->src);
  ulong_array_free (entry->dst);
}

typedef struct map
{
  char *from;
  char *to;
  size_t entries;
  map_entry_t **map_entries;
} map_t;

const char *TO = "-to-";
const size_t TO_SZ = 4; // strlen (TO);

map_t *
map_new (const char *linebuffer)
{
  char *from = (char *)calloc (sizeof (char), BUFFER_DEFAULT);
  char *to = (char *)calloc (sizeof (char), BUFFER_DEFAULT);

  // Even though this could use serious cleanup, it worked
  // AS expected on my very first run so props to me...
  // I guess??
  size_t linesz = strlen (linebuffer);
  char *substring = strstr (linebuffer, TO);
  char *end = strchr (linebuffer, ' ');
  size_t suffix_sz = (size_t)((&linebuffer[linesz - 1]) - end);

  size_t from_sz = (size_t)(substring - linebuffer);
  size_t to_sz = linesz - suffix_sz - from_sz - TO_SZ - 1;
  strncpy (from, linebuffer, from_sz);
  from[from_sz] = '\0';
  strncpy (to, &linebuffer[from_sz + TO_SZ], to_sz);
  to[to_sz] = '\0';

  map_t *map = (map_t *)malloc (sizeof (map_t));
  map->from = from;
  map->to = to;
  map->entries = 0;
  map->map_entries
      = (map_entry_t **)calloc (sizeof (map_entry_t *), BUFFER_DEFAULT);
  return map;
}

bool
map_insert_entry (map_t *map, ulong dst, ulong src, size_t range)
{
  if (map == NULL)
    {
      fprintf (stderr, "ERROR: did not expect map to be NULL\n");
      return false;
    }

  map->map_entries[map->entries] = map_entry_new (dst, src, range);
  map->entries += 1;

  return true;
}

ulong
map_src_to_dst (map_t *map, ulong target)
{
  ulong result = target;
  for (size_t k = 0; k < map->entries; k++)
    {
      map_entry_t *entry = map->map_entries[k];
      size_t pos = 0;
      if (ulong_array_in (entry->src, target, &pos))
        {
          // HERE: refactored to math
          result = entry->dst->start + pos;
          break;
        }
    }
  return result;
}

void
map_free (map_t *map)
{
  if (map == NULL)
    {
      return;
    }
  for (size_t i = 0; i < map->entries; i++)
    {
      map_entry_free (map->map_entries[i]);
    }
  free (map->from);
  free (map->to);
  free (map);
}

typedef struct ns_result
{
  ulong num;
  char *err;
} ns_result_t;

const ns_result_t no_digit = { 0, "ERROR: Expected a digit" };

ns_result_t
number_from_str (const char *linebuffer, const char end, size_t *digits)
{
  ns_result_t ret = { 0, NULL };
  for (size_t i = 0; linebuffer[i]; i++)
    {
      char c = linebuffer[i];
      if (c == end || c == '\n')
        {
          // done
          break;
        }
      int digit = c - '0';
      if (digit < 0 || digit > 9)
        {
          fprintf (stderr, "%s  but got %c (%d)\n", no_digit.err, c, c);
          ret.err = no_digit.err;
          return ret;
        }

      ret.num *= 10;
      ret.num += (ulong)digit;
      if (digits != NULL)
        {
          digits[0]++;
        }
    }

  return ret;
}

typedef struct seeds
{
  ulong *ranges;
  size_t sz;
} seeds_t;

const char *BEGIN = "seeds: ";
const size_t BEGIN_SZ = 7; // strlen (BEGIN);

seeds_t *
seeds_from (char *linebuffer, size_t len)
{
  if (strncmp (BEGIN, linebuffer, BEGIN_SZ) != 0)
    {
      fprintf (stderr,
               "WARN: expected the string to start with %s but got : %s\n",
               BEGIN, linebuffer);
      return NULL;
    }

  ulong *ranges = (ulong *)calloc (sizeof (ulong), BUFFER_DEFAULT);
  size_t range_sz = 0;
  size_t *digits = (size_t *)malloc (sizeof (size_t));
  for (size_t i = 0; i < len - BEGIN_SZ; i += digits[0])
    {
      digits[0] = 1;
      // TODO: I know we don't need to realloc but should for safety anyway
      ns_result_t res
          = number_from_str (&linebuffer[BEGIN_SZ + i], ' ', digits);
      if (res.err != NULL)
        {
          fprintf (stderr,
                   "ERROR: attempted to convert ulong starting on index %ld "
                   "of %s\n",
                   BEGIN_SZ + 1, linebuffer);
          return 0;
        }
      ranges[range_sz] = res.num;
      range_sz++;
    }

  if (range_sz % 2 != 0)
    {
      fprintf (stderr,
               "ERROR: expected seed ranges line to have 2 values per range "
               "got %ld "
               "of %s\n",
               range_sz, linebuffer);
      return 0;
    }

  seeds_t *seeds = (seeds_t *)malloc (sizeof (seeds_t));
  seeds->ranges = ranges;
  seeds->sz = range_sz;
  printf ("SEEDS of %ld\n", seeds->sz);
  return seeds;
}

__global__ void
seed_location(map_t *maps)
{

}

int
main ()
{
  FILE *file = fopen (FILENAME, "r");
  if (file == NULL)
    {
      fprintf (stderr, "ERROR: could not open %s : got %d\n", FILENAME, errno);
      perror ("fopen");
      return EXIT_FAILURE;
    }

  int fd = fileno (file);

  stat_t statb;
  int statr = fstat (fd, &statb);
  if (statr != 0)
    {
      fprintf (stderr, "ERROR: could not read %s informations : got %d\n",
               FILENAME, errno);
      perror ("fstat");
      return EXIT_FAILURE;
    }

  size_t bufsize = (size_t)statb.st_size;
  char *linebuffer = (char *)malloc (bufsize);
  if (linebuffer == NULL)
    {
      fprintf (stderr, "ERROR: could not alloc mem to read %s : got %d\n",
               FILENAME, errno);
      perror ("malloc");
      return EXIT_FAILURE;
    }

  // loop here
  ulong result1 = ULONG_MAX, line = 0;

  // start by getting the seeds
  // ulong *seeds = (ulong *)calloc (sizeof (ulong), BUFFER_DEFAULT);
  // size_t seeds_sz = 0;
  seeds_t *seeds = NULL;

  // next start building the maps
  map_t **maps = (map_t **)calloc (sizeof (map_t *), BUFFER_DEFAULT);
  size_t maps_sz = 0;

  // this is the part where we add really weird state management
  // variables
  bool nxt_line_header = false;
  map_t *map = NULL;

  long linesz_u = 0;
  size_t linesz = 0;
  while ((linesz_u = getline (&linebuffer, &bufsize, file)) != -1)
    {
      line++;
      if (errno != 0)
        {
          fprintf (
              stderr,
              "ERROR: (%ld) attempted to read a line from %s but got : %d\n",
              line, FILENAME, errno);
          perror ("getline");
          continue;
        }
      linesz = (size_t)linesz_u;
      linebuffer[linesz] = '\0';
      fprintf (stderr, "INFO: Starting to process line %ld\n", line);

      if (strlen (linebuffer) <= 0 || linebuffer[0] == ' '
          || linebuffer[0] == '\n')
        {
          // signals the previous map is over
          nxt_line_header = true;
          continue;
        }

      if (seeds == NULL)
        {
          // signals we haven't collected seeds yet
          seeds = seeds_from (linebuffer, linesz);
          if (seeds == NULL)
            {
              fprintf (stderr, "ERROR: (%ld) could not seem to count seeds\n",
                       line);
              return EXIT_FAILURE;
            }
          continue;
        }

      if (nxt_line_header)
        {
          if (strlen (linebuffer) > BUFFER_DEFAULT)
            {
              fprintf (stderr, "ERROR: (%ld) didn't code for long strings",
                       line);
              return EXIT_FAILURE;
            }
          nxt_line_header = false;
          map = map_new (linebuffer);
          // TODO: don't need to resize since we know it's big enough
          // but should anyways if we need to
          maps[maps_sz] = map;
          maps_sz++;
          continue;
        }

      // at this point we should have 3 numbers in a row representing a map
      // entry
      size_t digit_v = 0;
      size_t *digits = &digit_v;
      ulong dst = 0, src = 0, range = 0;
      size_t loops = 0;
      for (size_t i = 0; i < linesz; i += digits[0])
        {
          loops += 1;
          digits[0] = 1;
          ns_result_t res = number_from_str (&linebuffer[i], ' ', digits);
          if (res.err != NULL)
            {
              fprintf (stderr,
                       "ERROR: (%ld) could not parse DSR at index '%ld' from "
                       "'%s'\n",
                       line, i, linebuffer);
              return EXIT_FAILURE;
            }
          ulong num = res.num;

          if (loops == 1)
            {
              dst = num;
            }
          else if (loops == 2)
            {
              src = num;
            }
          else if (loops == 3)
            {
              range = num;
            }
          else
            {
              // probably safe to just break
              // break;
              // ERROR: other option is to throw an error
              fprintf (stderr,
                       "ERROR: (l:%ld i:%ld d:%ld) Expected 3 numbers, got "
                       "more than 3\n",
                       line, i, digits[0]);
              return EXIT_FAILURE;
            }
        }

      if (loops < 3)
        {
          fprintf (stderr,
                   "ERROR: (%ld:%ld) expected 3 ulongs on the line, got d:%ld "
                   "s:%ld r:%ld : [%s]\n",
                   line, loops, dst, src, range, linebuffer);
        }

      // printf ("MAP ENTRY d:%ld s:%ld r:%ld from [%s]\n", dst, src, range,
      //         linebuffer);
      bool inserted = map_insert_entry (map, dst, src, range);
      if (!inserted)
        {
          fprintf (stderr,
                   "ERROR: (%ld) expected to be able to insert an entry but "
                   "could not\n",
                   line);
          return EXIT_FAILURE;
        }
    }

  if (seeds == NULL)
    {
      fprintf (stderr, "ERROR: did not expect seeds to be null\n");
      return EXIT_FAILURE;
    }

  // TODO: could make this an arg of some sort
  const char *TARGET = "location";
  for (size_t range = 0; range < seeds->sz; range += 2)
    {
      ulong begin = seeds->ranges[range];
      ulong seeds_sz = seeds->ranges[range + 1];
      printf ("%ld of %ld batch1 %ld at %ld\n", range, seeds->sz, seeds_sz,
              begin);
      for (size_t i = 0; i < seeds_sz; i++)
        {
          ulong index = (ulong)i;
          ulong seed = begin + index;
          char *from = "seed"; // TODO: constants here?
          for (;;)
            {
              for (size_t j = 0; j < maps_sz; j++)
                {
                  map_t *map_cur = maps[j];
                  // prsize_tf ("Map from %s to %s\n", map->from, map->to);
                  if (strcmp (map_cur->from, from) == 0)
                    {
                      // we found the next map
                      // start by setting the next from
                      from = map_cur->to;
                      seed = map_src_to_dst (map_cur, seed);
                    }
                  // skip the failure condition and look for the next map
                  if (strcmp (from, TARGET) == 0)
                    {
                      // set the result!
                      if (seed < result1)
                        {
                          result1 = seed;
                        }
                      goto done;
                    }
                }

              fprintf (stderr,
                       "ERROR: if we got here then it means we didn't find "
                       "the next map. We wanted %s. Here's what we have:\n",
                       from);
              return EXIT_FAILURE;
            }
        done:;
        }
      printf ("INFO: current lowest %ld\n", result1);
    }

  printf ("Part 1 : %ld\n", result1);

  for (int i = 0; i < maps_sz; i++)
    {
      map_free (map);
    }
  free (maps);
  free (seeds);

  return EXIT_SUCCESS;
}
