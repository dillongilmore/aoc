#include <errno.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#define PARTS 2
#define GEAR '*'

// How much to allocate our buffers by to avoid smashing our
// heap
#define DEFAULT_MX 100

typedef struct stat stat_t;

#define FILENAME "bin/2023/tests/day3-example1.txt"
// #define FILENAME "bin/2023/tests/day3-part1.txt"

const size_t INT_SZ = sizeof (int);
const size_t SZT_SZ = sizeof (size_t);
const size_t PTR_SZ = sizeof (void *);

// 48
const int ZERO = (int)'0';

// 33-47
const int SYMB1_BEGIN = (int)'!';
const int SYMB1_END = (int)'/';
const int SYMB2_BEGIN = (int)':';
const int SYMB2_END = (int)'@';

typedef struct symbol
{
  char sym;
  int ratio;
  size_t parts;
  size_t pos;
} symbol_t;

const size_t SYMBOL_SZ = sizeof (symbol_t);

symbol_t *
symbol_new (char c, size_t pos)
{
  symbol_t *sym = (symbol_t *)malloc (SYMBOL_SZ);
  sym->sym = c;
  sym->pos = pos;
  sym->ratio = 1; // doing multiplication via *=
  sym->parts = 0;
  return sym;
}

int
symbol_has_parts (symbol_t **syms, size_t syms_sz)
{
  int result = 0;

  for (int i = 0; i < syms_sz; i++)
    {
      symbol_t *sym = syms[i];
      if (sym == NULL)
        {
          continue;
        }

      if (sym->parts == PARTS)
        {
          result += sym->ratio;
        }
    }
  return result;
}

typedef struct number
{
  int value;
  size_t start;
  size_t end;
} number_t;

const size_t NUMBER_SZ = sizeof (number_t);

size_t
next_realloc (size_t i)
{
  size_t result = (size_t)(ceil (i / DEFAULT_MX) * DEFAULT_MX);
  return result;
}

int
numbers_symbol_adjacent (number_t **nums, size_t nums_sz, symbol_t **syms,
                         size_t syms_sz)
{
  int result = 0;
  for (size_t i = 0; i < nums_sz; i++)
    {
      number_t *num = nums[i];
      if (num == NULL)
        {
          continue;
        }

      // How much to allocate our buffers by to avoid smashing our
      // heap
      for (size_t j = 0; j < syms_sz; j++)
        {
          symbol_t *sym = syms[j];
          size_t pos = sym->pos;

          // we are adjacent if we are start-1 to end+1
          size_t begin = num->start;
          // size_t is unsigned
          if (begin > 0)
            {
              begin -= 1;
            }

          // NOTE: likely need to check for MAX_INT to prevent overflow
          size_t end = num->end;

          if (pos >= begin && pos <= end)
            {
              sym->parts += 1;
              sym->ratio *= num->value;

              // we are adjacent
              result += num->value;

              // remove this num from our state so we don't double count
              if (num != NULL)
                {
                  free (num);
                }
              nums[i] = NULL;
              break;
            }
        }
    }
  return result;
}

int
main ()
{
  FILE *file = fopen (FILENAME, "r");
  if (file == NULL)
    {
      fprintf (stderr, "ERROR: could not open %s : got %d\n", FILENAME, errno);
      perror ("fopen");
      return EXIT_FAILURE;
    }

  int fd = fileno (file);

  stat_t statb;
  int statr = fstat (fd, &statb);
  if (statr != 0)
    {
      fprintf (stderr, "ERROR: could not read %s informations : got %d\n",
               FILENAME, errno);
      perror ("fstat");
      return EXIT_FAILURE;
    }

  size_t bufsize = (size_t)statb.st_size;
  char *linebuffer = (char *)malloc (bufsize);
  if (linebuffer == NULL)
    {
      fprintf (stderr, "ERROR: could not alloc mem to read %s : got %d\n",
               FILENAME, errno);
      perror ("malloc");
      return EXIT_FAILURE;
    }

  // these are used towards the end, accumulate our results
  int line = 0, result1 = 0, result2 = 0;

  //  prev_nums keeps track of the previous line size and max size
  //  for numbers (to avoid excess reallocs)
  size_t prev_nums_sz = 0, prev_nums_mx = DEFAULT_MX,
         // nums and sym mx variables so we can reuse the same memory buffer
         // between loops and simply increase the size as needed
      nums_sz = 0, nums_mx = DEFAULT_MX, syms_sz = 0, syms_mx = DEFAULT_MX,
         // prev_sym keeps track of previous line size and max size for symbol
         // positions (to avoid excess reallocs)
      prev_syms_sz = 0, prev_syms_mx = DEFAULT_MX;

  symbol_t **prev_syms = calloc (PTR_SZ, DEFAULT_MX),
           **syms = calloc (PTR_SZ, DEFAULT_MX);

  number_t **prev_nums = calloc (PTR_SZ, DEFAULT_MX),
           **nums = calloc (PTR_SZ, DEFAULT_MX);

  while (getline (&linebuffer, &bufsize, file) != -1)
    {
      line++;
      if (errno != 0)
        {
          fprintf (
              stderr,
              "ERROR: (%d) attempted to read a line from %s but got : %d\n",
              line, FILENAME, errno);
          perror ("getline");
          continue;
        }

      // PHASE 1 : convert the raw line into managable objects
      syms_sz = 0;
      nums_sz = 0;
      number_t *num = NULL;

      for (size_t i = 0; linebuffer[i]; i++)
        {
          char c = linebuffer[i];

          if (c == '.' || c == ' ' || c == '\n')
            {
              goto next;
            }

          int ascii = (int)c;
          if ((ascii >= SYMB1_BEGIN && ascii <= SYMB1_END)
              || (ascii >= SYMB2_BEGIN && ascii <= SYMB2_END))
            {
              syms[syms_sz] = symbol_new (c, i);
              syms_sz++;

              // only doing a realloc as needed
              if (syms_sz >= syms_mx)
                {
                  size_t nxt = next_realloc (syms_sz);
                  syms = reallocarray (syms, PTR_SZ, nxt);
                  syms_mx = nxt;
                }
              goto next;
            }

          int digit = ascii - ZERO;
          if (digit >= 0 && digit <= 9)
            {
              if (num == NULL)
                {
                  num = (number_t *)malloc (NUMBER_SZ);
                  num->start = i;
                  num->value = 0;
                }
              num->value *= 10;
              num->value += digit;
              continue;
            }

          printf ("ERROR: unknown character %c\n", c);

        next:
          if (num != NULL)
            {
              num->end = i;
              nums[nums_sz] = num;
              nums_sz++;
              if (nums_sz >= nums_mx)
                {
                  size_t nxt = next_realloc (nums_sz);
                  nums = reallocarray (nums, PTR_SZ, nxt);
                  nums_mx = nxt;
                }
              num = NULL;
            }
        }
      // End PHASE 1

      // PHASE 2 check the previous line if any numbers are adjacent to our
      // current symbols
      result1
          += numbers_symbol_adjacent (prev_nums, prev_nums_sz, syms, syms_sz);
      // End PHASE 2
      // PHASE 3 check if our current numbers are adjacent to the previous
      // line's symbols
      result1
          += numbers_symbol_adjacent (nums, nums_sz, prev_syms, prev_syms_sz);
      // End PHASE 3
      // PHASE 4 check if the current numbers are adjacent to the current
      // symbols
      result1 += numbers_symbol_adjacent (nums, nums_sz, syms, syms_sz);
      //  End Phase 4

      // PHASE 6 : take the previous symbols, which at this point have gone
      // through 2 iterations of matching to see if they match exactly PARTS
      // times and to add it's ratio to result2
      result2 += symbol_has_parts (prev_syms, prev_syms_sz);

      // PHASE 5 discard previous lines data and set them to our current line
      for (size_t i = 0; i < prev_nums_sz; i++)
        {
          number_t *num = prev_nums[i];
          if (num != NULL)
            {
              free (num);
            }
          prev_nums[i] = NULL;
        }

      if (nums_mx >= prev_nums_mx)
        {
          prev_nums = reallocarray (prev_nums, PTR_SZ, nums_mx);
          prev_nums_mx = nums_mx;
        }

      if (syms_mx >= prev_syms_mx)
        {
          prev_syms = reallocarray (prev_syms, PTR_SZ, syms_mx);
          prev_syms_mx = syms_mx;
        }
      memcpy (prev_syms, syms, syms_sz * PTR_SZ);
      memcpy (prev_nums, nums, nums_sz * PTR_SZ);
      prev_nums_sz = nums_sz;
      prev_syms_sz = syms_sz;
    }

  // PHASE 6 part 2 : the last row isn't in yet...
  result2 += symbol_has_parts (syms, syms_sz);

  printf ("Result 1: %d\nResult 2: %d\n", result1, result2);

  // TODO: each of these arrays will leak data :(
  if (nums != NULL)
    {
      free (nums);
    }
  if (prev_nums != NULL)
    {
      free (prev_nums);
    }
  if (syms != NULL)
    {
      free (syms);
    }
  if (prev_syms != NULL)
    {
      free (prev_syms);
    }

  return EXIT_SUCCESS;
}
