#include <array>
#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <glib.h>
#include <iostream>
#include <map>
#include <memory>
#include <optional>
#include <string>
#include <vector>

#define FILENAME "bin/2023/tests/day20-example1.txt"

namespace aoc {

enum class pulse { low, high };

enum class type : u_char {
  broadcaster = 'b',
  flip_flop = '%',
  conjunction = '&'
};

/*
enum class error_code { module_not_found };
using error_map =
    std::map<const error_code, const std::unique_ptr<const std::string>>;
inline auto error_msg() -> const error_map & {
  static error_map errors{
      {error_code::module_not_found,
       std::make_unique<const std::string>("couldn't find module %s")}};
  return errors;
};
*/
constexpr size_t default_error_sz = 100;
template <typename... Ts>
auto make_error(const std::string &msg, Ts... fmt) -> const char * {
  assert(msg.length() < default_error_sz);
  std::array<char, default_error_sz> buffer{};
  if (std::sprintf(buffer.begin(), msg.c_str(), &fmt...) <= 0) {
    return "fatality - attempted to generate an error but could not";
  }
  const char *ret = buffer.cbegin();
  return ret;
}

using signals = std::tuple<uint64_t, uint64_t>;

class module {
public:
  module(module &) = default;
  module(const module &) = default;
  module(module &&) = delete;
  module() = default;

  auto operator=(const module &) -> module & = default;
  auto operator=(module &&) -> module & = delete;
  virtual ~module() = default;

  using builder = std::map<std::string, std::shared_ptr<module>>;
  using error = std::optional<const char *>;

  /**
   * Once we loop through our input we'll call this once per module to translate
   * the string pointers to module to actual modules.
   */
  [[nodiscard]] virtual auto realize(const builder &) -> error = 0;

  /**
   * This is for upstreams to call to downstreams so that
   * the downstreams know about all of it's parents. This is
   * only called once per upstream.
   */
  virtual auto notify(const std::string &) -> void = 0;

  /**
   * Process a signal, called `TIMES`
   */
  [[nodiscard]] virtual auto signal(pulse pulse) -> signals = 0;
};

class component {
  std::string name_;
  std::vector<std::string> pointers_;
  std::vector<std::shared_ptr<module>> downstreams_;

public:
  component(std::string &name, std::vector<std::string> &downstreams)
      : name_(name), pointers_(downstreams) {}

  auto get() -> std::vector<std::shared_ptr<module>> { return downstreams_; }

  auto realize(const module::builder &builder) -> module::error {
    for (const auto &pointer : pointers_) {
      if (builder.contains(pointer)) {
        const auto &mod = builder.at(pointer);
        // notify all of our downstreams that we are a parent
        mod->notify(name_);
        downstreams_.push_back(builder.at(pointer));
      } else {
        return make_error("expected to find %s in builder map", pointer);
      }
    }
    if (pointers_.size() != downstreams_.size()) {
      return make_error("expected pointers and downstreams to be same size but "
                        "got p%ld != d%ld",
                        pointers_.size(), downstreams_.size());
    }
    return std::nullopt;
  }
};

enum class ff_state { off, on };
class flip_flop : public module {
  ff_state state = ff_state::off;
  component comp_;

public:
  explicit flip_flop(component &comp) : comp_(comp) {}

  auto realize(const module::builder &builder) -> module::error final {
    return comp_.realize(builder);
  }
  auto notify(const std::string &name) -> void final {
    // nothing to do here
  }
  auto signal(pulse pulse) -> signals final { return {1, 1}; }
};

enum class conj_type { single, multi };
class conjunction : public module {
  std::optional<conj_type> type_;
  component comp_;

public:
  explicit conjunction(component &comp) : comp_(comp) {}
  auto realize(const module::builder &builder) -> module::error final {
    return comp_.realize(builder);
  }
  auto notify(const std::string &name) -> void final {
    // TODO: implement
  }
  auto signal(pulse pulse) -> signals final { return {1, 1}; }
};

const char *const broadcast = "roadcaster";
class broadcaster : public module {
  component comp_;

public:
  explicit broadcaster(component &comp) : comp_(comp) {}

  auto realize(const module::builder &builder) -> module::error final {
    return comp_.realize(builder);
  }
  auto notify(const std::string &name) -> void final {
    // nothing to do here
  }
  auto signal(pulse pulse) -> signals final { return {1, 1}; }
};

auto make_module(std::string name, type type,
                 std::vector<std::string> downstreams)
    -> std::shared_ptr<module> {
  component comp(name, downstreams);
  switch (type) {
  case type::broadcaster:
    // broadcaster mod(comp);
    // auto *mod = new broadcaster(comp);
    return std::make_shared<broadcaster>(broadcaster(comp));
  case type::flip_flop:
    // return std::make_shared<module>(flip_flip(comp));
    return std::make_shared<flip_flop>(flip_flop(comp));
  case type::conjunction:
    // return std::make_shared<module>(conjunction(comp));
    return std::make_shared<conjunction>(conjunction(comp));
  }
}

} // namespace aoc

constexpr uint64_t TIMES = 1000;
auto main() -> int {
  std::string line;
  std::fstream infile(FILENAME);
  aoc::module::builder modules;
  while (std::getline(infile, line)) {
    auto *splits = g_strsplit(line.c_str(), " -> ", 0);
    if (g_strv_length(splits) != 2) {
      std::cout << "ERROR: expected 2 strings, got " << line << '\n';
      return EXIT_FAILURE;
    }

    std::string module(splits[0]);
    auto *destinations = g_strsplit(splits[1], ",", 0);
    auto dest_length = g_strv_length(destinations);
    std::vector<std::string> mods(dest_length);

    for (auto index = 0; index < dest_length; index++) {
      std::string mod(destinations[index]);
      mods[index] = mod;
    }

    auto name = module.substr(1);
    auto type = static_cast<aoc::type>(module[0]);
    modules[name] = aoc::make_module(name, type, mods);
  }

  // now that all modules have been intialized, let's realize our mods
  for (auto [name, mod] : modules) {
    // realize should call `notify`
    if (!mod->realize(modules)) {
      std::cout << "ERROR: failed to realize module : " << name << '\n';
      // return EXIT_FAILURE;
    }
    std::cout << name << '\n';
  }
  return EXIT_FAILURE;

  uint64_t low_result = 0;
  uint64_t high_result = 0;
  for (auto time = 0; time < TIMES; time++) {
    if (modules.contains(aoc::broadcast)) {
      auto [low, high] = modules.at(aoc::broadcast)->signal(aoc::pulse::low);
      low_result += low;
      high_result += high;

    } else {
      std::cout << "ERROR: could not find the `broadcast` module\n";
      return EXIT_FAILURE;
    }
  }

  uint64_t result1 = low_result * high_result;
  std::cout << "Part 1 : " << result1 << "\n";

  return EXIT_SUCCESS;
}
