
#include <algorithm>
#include <cstdint>
#include <cstdlib>
#include <errno.h>
#include <functional>
#include <iostream>
#include <ostream>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <sys/stat.h>
#include <tuple>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#define FILENAME "bin/2023/tests/day7-part1.txt"
// #define FILENAME "bin/2023/tests/day7-example1.txt"

#define BUFFER_DEFAULT 100

typedef struct stat stat_t;

typedef struct ns_result
{
  uint64_t num;
  std::string err;
  size_t *digits;
} ns_result_t;

const ns_result_t no_digit = { 0, "ERROR: Expected a digit", 0 };

ns_result_t
number_from_str (const char *linebuffer, const char end)
{
  ns_result_t ret = { 0, "", 0 };
  for (size_t i = 0; linebuffer[i]; i++)
    {
      char c = linebuffer[i];
      if (c == end || c == '\n' || c == '\0')
        {
          // done
          break;
        }
      int digit = c - '0';
      if (digit < 0 || digit > 9)
        {
          fprintf (stderr, "%s  but got %c (%d)\n", no_digit.err.c_str (), c,
                   c);
          ret.err = no_digit.err;
          return ret;
        }

      ret.num *= 10;
      ret.num += (uint64_t)digit;
      ret.digits += 1;
    }

  return ret;
}

uint64_t
num_bigger (uint64_t time, uint64_t goal)
{
  uint64_t bigger = 0;

  // Method: brute force
  for (size_t j = 1; j < time; j++)
    {
      uint64_t distance = (time - j) * j;
      if (distance > goal)
        {
          if (bigger >= INT64_MAX)
            {
              fprintf (stderr, "ERROR: we're going to overflow, exiting...\n");
            }
          bigger += 1;
        }
    }
  return bigger;
}

uint64_t
i64_pow (uint64_t num, size_t of)
{
  uint64_t ret = 1;
  for (size_t i = 0; i < of; i++)
    {
      ret *= num;
    }
  return ret;
}

#define CARD_HIGHEST 14

uint8_t
card_to_int (char c, bool wildcards)
{
  switch (c)
    {
    case 'A':
      {
        return CARD_HIGHEST;
      }
    case 'K':
      {
        return CARD_HIGHEST - 1;
      }
    case 'Q':
      {
        return CARD_HIGHEST - 2;
      }
    case 'J':
      {
        if (wildcards)
          {
            return 0;
          }
        else
          {
            return CARD_HIGHEST - 3;
          }
      }
    case 'T':
      {
        return CARD_HIGHEST - 4;
      }
    default:
      {
        int64_t ret = c - '0';
        if (ret <= 0 || ret > 9)
          {
            fprintf (stderr, "ERROR: unexpected hand input of %c\n", c);
          }
        return (uint8_t)ret;
      }
    }
}

enum class Type
{
  high,
  one,
  two,
  three,
  full,
  four,
  five,
};

constexpr size_t HAND_SZ = 5;

typedef struct Hand
{
  Type type;
  uint64_t bid;
  std::vector<uint8_t> card_order;
  std::unordered_map<uint8_t, uint8_t> cards;
  std::string raw;
  bool wildcards;
  uint64_t wilds;

  // turns out this isn't actually poker
  std::vector<uint8_t> power;

  Hand (bool wildcards) : wildcards (wildcards), wilds (0) {}

  std::string
  to_string ()
  {
    std::string ret = "";
    for (uint8_t p : card_order)
      {
        printf ("%d", p);
        // ret.append (1, p);
      }
    return ret;
  }

  friend bool
  operator< (const Hand &a, const Hand &b)
  {
    if (a.type < b.type)
      {
        return true;
      }
    if (a.type > b.type)
      {
        return false;
      }

    // equal
    for (size_t i = 0; i < a.card_order.size (); i++)
      {
        /* Old logic that is correct in poker terms but
         * the author doesn't know how poker works
          if (a.power[i] < b.power[i])
            {
              return true;
            }
          if (a.power[i] > b.power[i])
            {
              return false;
            }
            *
            */
        if (a.card_order[i] < b.card_order[i])
          {
            return true;
          }
        if (a.card_order[i] > b.card_order[i])
          {
            return false;
          }
      }

    // both hands are exactly equal so return false
    return false;
  }

  void
  add_card (char card)
  {
    raw.append (1, card);
    auto i = card_to_int (card, wildcards);
    cards[i] += 1;
    card_order.push_back (i);
    if (wildcards && card == 'J')
      {
        wilds++;
      }
  }

  void
  set_type ()
  {
    printf ("got wild %ld\n", wilds);
    // get the power of our cards
    for (auto card : cards)
      {
        power.push_back (card.first);
      }
    std::sort (power.begin (), power.end (), std::greater<uint64_t> ());

    // take care of the easy parts
    size_t sz = cards.size ();

    if (sz == 1)
      {
        // this is the only option
        type = Type::five;

        // if we're 5 wilds then same as high
        if (power[0] == 'J')
          {
            type = Type::high;
          }
      }
    else if (sz == 2)
      {
        // either a 4 or full stack
        for (auto [card, count] : cards)
          {
            switch (count)
              {
              case 4:
                {
                  power[0] = card;
                  type = Type::four;

                  // regardless of it we have 4 wilds
                  // or 1 wild, we're upgraded to a 5 stack
                  if (wilds >= 1)
                    {
                      type = Type::five;
                    }
                  break;
                }
              case 3:
                {
                  power[0] = card;
                  type = Type::full;

                  // if we have 3 wilds or 2 wilds,
                  // we're still a 5 stack
                  if (wilds >= 1)
                    {
                      type = Type::five;
                    }
                  break;
                }
              case 2:
                {
                  power[1] = card;
                  break;
                }
              case 1:
                {
                  power[1] = card;
                  break;
                }
              default:
                {
                  fprintf (stderr, "ERROR: expected either 4, 3, 2, 1\n");
                  return;
                }
              }
          }
      }
    else if (sz == 3)
      {
        // could be 1 3 pair, or 2 2 pairs
        uint8_t previous = 0; // our smallest card is 1
        for (auto [card, count] : cards)
          {
            switch (count)
              {
              case 3:
                {
                  type = Type::three;

                  if (wilds >= 1)
                    {
                      // this means our wilds are not the 3 pair,
                      // it should only be possible to have a single wild
                      // when here.
                      // Even if the 3 pair is the wilds, most we can add is 1.
                      // In either case we're a 4 pair
                      type = Type::four;
                    }

                  // OLD POWER SORTING
                  auto it = find (power.begin (), power.end (), card);
                  power.erase (it);
                  power.insert (power.begin (), card);
                  goto done;
                }
              case 2:
                {
                  type = Type::two;

                  // If we have 1 wild, we get a full house
                  // If we have 2 wilds then we can make a
                  // 4 pair
                  switch (wilds)
                    {
                    case 1:
                      {
                        type = Type::full;
                        break;
                      }
                    case 2:
                      {
                        type = Type::four;
                        break;
                      }
                    default:
                      {
                        if (wildcards)
                          {
                            fprintf (stderr,
                                     "ERROR: got an unexpected number of "
                                     "wilds %ld\n",
                                     wilds);
                          }
                      }
                    }

                  // OLD POWER SORTING
                  uint8_t offset = 0;
                  if (card < previous)
                    {
                      offset += 1;
                    }

                  auto it = find (power.begin (), power.end (), card);
                  // auto index = it - power.begin ();
                  power.erase (it);
                  power.insert (power.begin () + offset, card);
                  previous = card;
                  break;
                }
              case 1:
                {
                  break;
                }
              default:
                {
                  fprintf (stderr, "ERROR: expected either 3 or 2 pair\n");
                  return;
                }
              }
          }
      done:;
      }
    else if (sz == 4)
      {
        // only option is 1 2 pair
        type = Type::one;

        // if we have 1 pair, and 1 wild, then that's a
        // 3 pair. If our 1, 1 pair is 2 wilds, also a 3 pair
        if (wilds >= 1)
          {
            type = Type::three;
          }

        // OLD POWER SORTER
        for (auto [card, count] : cards)
          {
            switch (count)
              {
              case 2:
                {
                  auto it = find (power.begin (), power.end (), card);
                  // auto index = it - power.begin ();
                  power.erase (it);
                  power.insert (power.begin (), card);
                  break;
                }
              }
          }
      }
    else
      {
        // only option, already sorted
        type = Type::high;

        // it's only possible to have 1 wildcard, so most we go is up to `one`
        if (wilds == 1)
          {
            type = Type::one;
          }
        else if (wilds != 0)
          {
            fprintf (stderr, "ERROR: expected 1 or 0 wilds, got %ld\n", wilds);
          }
      }
    printf ("got type %d\n", static_cast<int> (type));
  }
} hand_t;

int
main ()
{
  FILE *file = fopen (FILENAME, "r");
  if (file == NULL)
    {
      fprintf (stderr, "ERROR: could not open %s : got %d\n", FILENAME, errno);
      perror ("fopen");
      return EXIT_FAILURE;
    }

  int fd = fileno (file);

  stat_t statb;
  int statr = fstat (fd, &statb);
  if (statr != 0)
    {
      fprintf (stderr, "ERROR: could not read %s informations : got %d\n",
               FILENAME, errno);
      perror ("fstat");
      return EXIT_FAILURE;
    }

  size_t bufsize = (size_t)statb.st_size;
  char *linebuffer = (char *)malloc (bufsize);
  if (linebuffer == NULL)
    {
      fprintf (stderr, "ERROR: could not alloc mem to read %s : got %d\n",
               FILENAME, errno);
      perror ("malloc");
      return EXIT_FAILURE;
    }

  uint64_t line = 0;
  long linesz_u = 0;
  size_t linesz = 0;

  // paranoid about dupe hands
  std::unordered_set<std::string> dupes;
  std::vector<hand_t> hands;
  std::vector<hand_t> hands_w;

  while ((linesz_u = getline (&linebuffer, &bufsize, file)) != -1)
    {
      line++;
      if (errno != 0)
        {
          fprintf (
              stderr,
              "ERROR: (%ld) attempted to read a line from %s but got : %d\n",
              line, FILENAME, errno);
          perror ("getline");
          continue;
        }
      linesz = (size_t)linesz_u;
      linebuffer[linesz - 1] = '\0';
      fprintf (stderr, "INFO: Starting to process line %ld :: ", line);

      std::string line_str = std::string (linebuffer);
      std::cout << '[' << line_str << ']' << std::endl;

      if (line_str.size () < 7)
        {
          // 5 cards, plus 1 space, plus at least 1 digit for a bid
          continue;
        }

      hand_t hand (false);
      hand_t hand_w (true);

      for (size_t i = 0; i < line_str.size (); i++)
        {
          char c = linebuffer[i];
          if (i <= 4)
            {
              hand.add_card (c);
              hand_w.add_card (c);
            }
          else if (c == ' ')
            {
              bool inserted;
              std::tie (std::ignore, inserted) = dupes.insert (hand.raw);
              if (!inserted)
                {
                  fprintf (stderr, "ERROR: (%ld) did not code for dupes %s\n",
                           line, hand.raw.c_str ());
                  return EXIT_FAILURE;
                }
              hand.set_type ();
              hand_w.set_type ();
              printf ("handw has %ld wilds\n", hand_w.wilds);
            }
          else
            {
              auto substr = line_str.substr (i, i + 1);
              auto results = number_from_str (substr.c_str (), ' ');
              if (!results.err.empty ())
                {
                  fprintf (stderr,
                           "ERROR: (l:%ld) could not parse bid from %s\n",
                           line, results.err.c_str ());
                  return EXIT_FAILURE;
                }
              hand.bid = results.num;
              hand_w.bid = results.num;
              break;
            }
        }
      hands.push_back (hand);
      hands_w.push_back (hand_w);
    }

  std::sort (hands.begin (), hands.end (),
             [] (const auto &a, const auto &b) { return a < b; });
  std::sort (hands_w.begin (), hands_w.end (),
             [] (const auto &a, const auto &b) { return a < b; });

  uint64_t result1 = 0, result2 = 0;
  for (size_t i = 0; i < hands.size (); i++)
    {
      auto hand1 = hands[i], hand2 = hands_w[i];
      auto index = i + 1;
      result1 += index * hand1.bid, result2 += index * hand2.bid;
      printf ("%s (%d) got %ld unique cards and result "
              "of [%ld * %ld = %ld + %ld]\n",
              hand2.raw.c_str (), static_cast<int> (hand2.type),
              hand2.power.size (), hand2.bid, index, index * hand2.bid,
              result2);
      printf ("Hand(cards='%s', bid=%ld)\n", hand2.raw.c_str (), hand2.bid);

      // std::cout << hand1.to_string () << std::endl;
    }

  printf ("Part 1 : %ld\nPart 2 : %ld\n", result1, result2);

  return EXIT_SUCCESS;
}
