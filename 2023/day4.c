//
// Created by dgilmore on 12/3/23.
//
// TODO: this is a really really great candidate to run on the GPU
//

#include <errno.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

// #define FILENAME "bin/2023/tests/day4-example1.txt"
#define FILENAME "bin/2023/tests/day4-part1.txt"

#define BUFFER_DEFAULT 100

typedef struct stat stat_t;

const size_t PTR_SZ = sizeof (void *);

enum Position
{
  Winning,
  Current
};

typedef struct card
{
  int repeats;
  int line;
} card_t;

card_t *
card_new (int repeats, int line)
{
  card_t *card = (card_t *)malloc (sizeof (card_t));
  // TODO: IF NULL
  card->repeats = repeats;
  card->line = line;
  return card;
}

size_t
next_realloc (size_t i)
{
  double index = (double)i;
  return (size_t)(ceil ((double)(index / BUFFER_DEFAULT)) * BUFFER_DEFAULT);
}

int
number_from_str (const char *linebuffer, const char end, size_t *digits)
{
  int ret = 0;
  for (size_t i = 0; linebuffer[i]; i++)
    {
      char c = linebuffer[i];
      if (c == end || c == '\n')
        {
          // done
          break;
        }
      int digit = c - '0';
      if (digit < 0 || digit > 9)
        {
          fprintf (stderr, "WARN: Expected a digit but got %c (%d)\n", c,
                   (int)c);
          break;
        }

      ret *= 10;
      ret += digit;
      if (digits != NULL)
        {
          digits[0]++;
        }
    }
  return ret;
}

int
main ()
{
  const char *BEGIN = "Card";
  const size_t BEGIN_SZ = strlen (BEGIN);

  FILE *file = fopen (FILENAME, "r");
  if (file == NULL)
    {
      fprintf (stderr, "ERROR: could not open %s : got %d\n", FILENAME, errno);
      perror ("fopen");
      return EXIT_FAILURE;
    }

  int fd = fileno (file);

  stat_t statb;
  int statr = fstat (fd, &statb);
  if (statr != 0)
    {
      fprintf (stderr, "ERROR: could not read %s informations : got %d\n",
               FILENAME, errno);
      perror ("fstat");
      return EXIT_FAILURE;
    }

  size_t bufsize = (size_t)statb.st_size;
  char *linebuffer = (char *)malloc (bufsize);
  if (linebuffer == NULL)
    {
      fprintf (stderr, "ERROR: could not alloc mem to read %s : got %d\n",
               FILENAME, errno);
      perror ("malloc");
      return EXIT_FAILURE;
    }

  // loop here
  int result1 = 0, result2 = 0, line = 0;
  size_t winning_mx = BUFFER_DEFAULT, current_mx = BUFFER_DEFAULT,
         repeat_mx = BUFFER_DEFAULT;
  int *winning_nums = (int *)calloc (sizeof (int), winning_mx);
  int *current_nums = (int *)calloc (sizeof (int), current_mx);
  card_t **repeats = (card_t **)calloc (sizeof (card_t *), repeat_mx);
  size_t winning_sz = 0, current_sz = 0, repeat_sz = 0;
  while (getline (&linebuffer, &bufsize, file) != -1)
    {
      line++;
      if (errno != 0)
        {
          fprintf (
              stderr,
              "ERROR: (%d) attempted to read a line from %s but got : %d\n",
              line, FILENAME, errno);
          perror ("getline");
          continue;
        }

      if (strncmp (BEGIN, linebuffer, BEGIN_SZ) != 0)
        {
          fprintf (
              stderr,
              "WARN: (%d) expected the string to start with %s but got : %s\n",
              line, BEGIN, linebuffer);
          continue;
        }

      size_t *digits = (size_t *)malloc (sizeof (size_t));
      digits[0] = 0;
      for (size_t i = BEGIN_SZ; linebuffer[i]; i++)
        {
          int c = linebuffer[i];
          if (c != ' ')
            {
              break;
            }
          digits[0]++;
        }
      number_from_str (&linebuffer[BEGIN_SZ + digits[0]], ':', digits);

      // <begin_sz> Card <space> <number> <colon>: <space> ... begin
      size_t pos = BEGIN_SZ + digits[0] + 2;

      free (digits);

      size_t *num_digits = (size_t *)malloc (sizeof (size_t));
      enum Position ctx = Winning;
      for (size_t i = pos; linebuffer[i]; i += num_digits[0])
        {
          char c = linebuffer[i];
          if (c == '|')
            {
              // one for the pipe and one for the space afterward
              num_digits[0] = 2;
              ctx = Current;
              continue;
            }

          if (c == ' ')
            {
              // comes up if we have xx and x in the same lists
              num_digits[0] = 1;
              continue;
            }

          num_digits[0] = 1;
          int num = number_from_str (&linebuffer[i], ' ', num_digits);
          switch (ctx)
            {
            case Winning:
              {
                winning_nums[winning_sz] = num;
                winning_sz++;
                // only doing a realloc as needed
                if (winning_sz >= winning_mx)
                  {
                    size_t nxt = next_realloc (winning_sz);
                    winning_nums = reallocarray (winning_nums, PTR_SZ, nxt);
                    winning_mx = nxt;
                  }
                break;
              }
            case Current:
              {
                current_nums[current_sz] = num;
                current_sz++;
                // only doing a realloc as needed
                if (current_sz >= current_mx)
                  {
                    size_t nxt = next_realloc (current_sz);
                    current_nums = reallocarray (current_nums, PTR_SZ, nxt);
                    current_mx = nxt;
                  }
                break;
              }
            }
        }

      free (num_digits);

      // find ourselves in repeats, append that value + 1 to results, then
      // remove ourselves and shift the rest of the items by 1
      size_t repeat_sz_copy = repeat_sz;
      bool removed_self = false;
      card_t *self = NULL;
      for (size_t i = 0; i < repeat_sz_copy; i++)
        {
          card_t *card = repeats[i];
          if (card->line == line)
            {
              result2 += card->repeats;
              repeat_sz--;
              removed_self = true;
              self = card;
            }
          else if (i > 0 && removed_self)
            {
              repeats[i - 1] = card;
            }
        }
      result2++;

      if (self == NULL)
        {
          self = card_new (1, line);
        }
      else
        {
          // for the current card!
          self->repeats += 1;
        }

      int winnings = 0;
      // TODO: way too many indents
      for (int k = 0; k < self->repeats; k++)
        {
          int nxt_line = line;
          printf ("\n%s\n", linebuffer);
          for (size_t i = 0; i < current_sz; i++)
            {
              int cur = current_nums[i];

              for (size_t j = 0; j < winning_mx; j++)
                {
                  int win = winning_nums[j];

                  if (cur == win)
                    {
                      // Phase 1
                      if (k == 0)
                        {
                          if (winnings == 0)
                            {
                              winnings += 1;
                            }
                          else
                            {
                              winnings *= 2;
                            }
                        }

                      printf ("%d == %d @@ %d\n", cur, win, winnings);
                      // End Phase 1
                      // Phase 2
                      nxt_line++;
                      for (int r = 0; r < repeat_sz; r++)
                        {
                          card_t *repeat_line = repeats[r];
                          if (repeat_line->line == nxt_line)
                            {
                              repeat_line->repeats++;
                              printf ("%d inc'd line %d to %d\n", line,
                                      nxt_line, repeat_line->repeats);
                              goto skip;
                            }
                        }
                      printf ("%d added new line %d\n", line, nxt_line);
                      card_t *new_repeat_line = card_new (1, nxt_line);
                      repeats[repeat_sz] = new_repeat_line;
                      repeat_sz++;
                    skip:;
                      // End Phase 2
                      break;
                    }
                }
            }
        }

      result1 += winnings;

      current_sz = 0;
      winning_sz = 0;
    }
  printf ("Part 1 : %d\nPart 2 : %d\n", result1, result2);

  free (winning_nums);
  free (current_nums);
  return EXIT_SUCCESS;
}
