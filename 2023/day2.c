
#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

// #define FILENAME "bin/2023/tests/day2-part1.txt"
#define FILENAME "bin/2023/tests/day2-part1.txt"
typedef struct stat stat_t;

typedef struct color
{
  int max;
  char *name;
  size_t namel;
  int largest_possible;
} color_t;

#define COLORS 3

color_t *
color_new (int max, char *name)
{
  color_t *color = (color_t *)malloc (sizeof (color_t));
  color->name = name;
  color->namel = strlen (name);
  color->largest_possible = 0;
  color->max = max;
  return color;
}

int
main ()
{
  // TODO: these can be CLI args
  const int MAX_RED = 12;
  const int MAX_GREEN = 13;
  const int MAX_BLUE = 14;
  const char *GAME = "Game";
  const size_t game_len = strlen (GAME);

  color_t *colors[COLORS]
      = { color_new (MAX_RED, "red"), color_new (MAX_GREEN, "green"),
          color_new (MAX_BLUE, "blue") };

  FILE *file = fopen (FILENAME, "r");
  if (file == NULL)
    {
      fprintf (stderr, "ERROR: could not open %s : got %d\n", FILENAME, errno);
      perror ("fopen");
      return EXIT_FAILURE;
    }

  int fd = fileno (file);

  stat_t statb;
  int statr = fstat (fd, &statb);
  if (statr != 0)
    {
      fprintf (stderr, "ERROR: could not read %s informations : got %d\n",
               FILENAME, errno);
      perror ("fstat");
      return EXIT_FAILURE;
    }

  size_t bufsize = (size_t)statb.st_size;
  char *linebuffer = (char *)malloc (bufsize);
  if (linebuffer == NULL)
    {
      fprintf (stderr, "ERROR: could not alloc mem to read %s : got %d\n",
               FILENAME, errno);
      perror ("malloc");
      return EXIT_FAILURE;
    }

  // loop here
  int result1 = 0, result2 = 0, line = 0;
  while (getline (&linebuffer, &bufsize, file) != -1)
    {
      line++;
      if (errno != 0)
        {
          fprintf (
              stderr,
              "ERROR: (%d) attempted to read a line from %s but got : %d\n",
              line, FILENAME, errno);
          perror ("getline");
          continue;
        }

      // Game
      if (strncmp (GAME, linebuffer, game_len) != 0)
        {
          fprintf (
              stderr,
              "WARN: (%d) expected the string to start with %s but got : %s\n",
              line, GAME, linebuffer);
          continue;
        }

      // TODO: (%d:) throw into a function
      int game = 0;
      int digits = 1;
      for (size_t i = game_len + 1; linebuffer[i]; i++)
        {
          char c = linebuffer[i];
          if (c == ':')
            {
              // done
              break;
            }
          int digit = c - '0';
          if (digit < 0 || digit > 9)
            {
              fprintf (stderr, "WARN: (%d) Expected a digit but got %c\n",
                       line, c);
              goto cont;
            }

          game *= 10;
          game += digit;
          digits++;
        }

      // %d <str>[, %d str]*[;]...
      size_t offset = game_len + ((size_t)digits) +
                      // for the colon
                      1 +
                      // for the space after
                      1;
      // printf("%ld\n", offset);
      int num = 0;
      size_t loops = strlen (linebuffer);
      for (size_t i = offset; linebuffer[i] && i < loops; i++)
        {
          char c = linebuffer[i];
          // printf("%ld %ld %c\n", strlen(linebuffer), i, c);
          if (c == ' ')
            {
              // next part should be a color
              color_t *current_color = NULL;
              for (size_t j = 0; j < COLORS; j++)
                {
                  color_t *color = colors[j];
                  if (strncmp (color->name, &linebuffer[i + 1], color->namel)
                      == 0)
                    {
                      // matched a color!
                      current_color = color;
                      break;
                    }
                }
              if (current_color == NULL)
                {
                  fprintf (stderr,
                           "ERROR: (%d) expected to match on a color but got "
                           "no color : "
                           "offset %ld : pos %ld : %s\n",
                           line, offset, i, linebuffer);
                  return EXIT_FAILURE;
                }
              // printf("skipping from %ld + %ld\n", i, current_color.namel);
              i +=
                  // for the current color
                  current_color->namel +
                  // for the comma/semicolon
                  1 +
                  // for the space after the comma
                  1;

              // TODO: Whole IF here is part 1 so add a condition I guess
              /*if (num > current_color->max) {
                // NOPE
                fprintf(stderr,
                        "INFO: Line %s @ %ld has more %s than we have %d >
              %d\n", linebuffer, i, current_color->name, num,
              current_color->max);
                // part 1 :)
                // goto cont;
              } else */
              if (num > current_color->largest_possible)
                {
                  // fprintf(stderr, "INFO: setting %s largest to %d max is
                  // %d\n",
                  //         current_color->name, num, current_color->max);
                  current_color->largest_possible = num;
                }

              num = 0;

              continue;
            }

          // TODO doing digit
          int digit = c - '0';
          if (digit < 0 || digit > 9)
            {
              fprintf (
                  stderr,
                  "WARN: (%d) Expected a digit for colors but got %c on %s at "
                  "pos %ld\n",
                  line, c, linebuffer, i);
              goto cont;
            }
          // %d
          num *= 10;
          num += digit;
        }

      result1 += game;
      int tmp_result = 1;
      for (int i = 0; i < COLORS; i++)
        {
          color_t *color = colors[i];
          fprintf (stderr, "%s(%d) ", color->name, color->largest_possible);
          tmp_result *= color->largest_possible;
          color->largest_possible = 0;
        }
      result2 += tmp_result;
    cont:
      fprintf (stderr, "INFO: Done with line! %s", linebuffer);
    }

  printf ("Part 1 : %d\nPart 2 : %d\n", result1, result2);

  return EXIT_SUCCESS;
}
